<?php
 
class I18n {
	
	private $val = array();
	
	function I18n() {
		Global $db;
		$this->val = $db->getAssoc("SELECT s.string, v.value FROM cms_lang_values AS v
				INNER JOIN cms_lang_strings AS s ON (s.id=v.string_id)
				WHERE v.lang_id=1");	
	}
 
    public function get($str) {
    	if (empty($this->val[$str])) return $str; 
        return $this->val[$str];
    }
    
    public function getLang() {
    	return "lt";
    }
}
 
?>