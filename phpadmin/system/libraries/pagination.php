<?php
function paginate($url = "?", $offset, $items, $ipp = 30, $nextprev = true, $step = false, $window = false) {
		$current = floor($offset / $ipp) + 1;
		$total = floor(($items - 1) / $ipp) + 1;
		// $ipp - items per page
		// $step - kas kiek praleidinėti
		// $window - kiek iš pradžių iš eilės
		
		global $report_db_id;
		
		if (!$items || ($total == 1)) {
			return '';
		}
	
		// step & window adjust
		if (!$step) {
			$step = 3;
	
			if ($total > 20)  {	$step = 5;		$window = 2; }
			if ($total > 50)  {	$step = 10;		$window = 2; }
			if ($total > 100) {	$step = 20;		$window = 4; }
			if ($total > 300) {	$step = 30; 	$window = 5; }
			if ($total > 400) {	$step = 40; 	$window = 6; }
			if ($total > 500) {	$step = 50;		$window = 6; }
			if ($total > 900) {	$step = 100;	$window = 8; }
		} // if
	
		if (!$window) {
			$window = 2;
		} // if
	
		// page pick
		$pages = array(1 => true);
	
		if ($total != 1) {
			$pages[$total] = true;
		} // if
	
		for ($i = 1; $i < ceil($total / $step); $i++) {
			$pages[$i * $step] = true;
		} // for
	
		$cf = max(1, $current - $window);
		$ct = min($total, $current + $window);
	
		for ($i = $cf; $i <= $ct; $i++) {
			$pages[$i] = true;
		} // for
	
		ksort($pages);
	
		// output
		$out = '';
		$lst = 0;
		if ($nextprev) {
			if ($current > 1) {
				$out .= '<li><a href="' . $url . "/" . ($current - 2) * $ipp . '">&laquo;</a></li>';
			} // if
		}
		while (list($page, ) = each($pages)) {
			if ($lst + 1 != $page) {
				//$out .= '&hellip; ';
				$out .= '<li class="disabled"><a href="#">&hellip;</a></li>';
			} // if

			
			if ($current == $page) {
				$out .= '<li class="active"><a href="' . $url . "/" . ($page - 1) * $ipp . '">'.$page.'</a></li>';
			} else {
				$out .= '<li><a href="' . $url . "/" . ($page - 1) * $ipp . '">'.$page.'</a></li>';
			} // if
	
			$lst = $page;
		} // while
	
		if ($nextprev) {
			if ($current < $total) {
				$out .= '<li><a href="' . $url . "/" . $current * $ipp . '">&raquo;</a></li>';
			} // if
		} // if
	
		return "<ul>$out</ul>";
	}
?>