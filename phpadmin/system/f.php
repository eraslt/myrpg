<?php

function __autoload($class_name) {
	$tmp = explode("_",$class_name);
	$dir = strtolower(array_shift($tmp));
	$class = strtolower(implode("_",$tmp));
	$dirs = array(
		"model"=>"models",
		"lib"=>"libraries",
	);
	if (file_exists(BASEPATH.$dirs[$dir].'/'.$class.EXT))
		require_once (BASEPATH.$dirs[$dir].'/'.$class.EXT);
	else 
		throw new Exception("Unable to load $class class.");
}

abstract class Controller {
	
	protected $auth_required = false;
	protected $authorized = false;
	
	public $output = "";
	
	public function checkAuth() {
		if (!$this->auth_required || $this->authorized) return true;
		return false;
	}
	
	function Controller() {
		GLOBAL $db;
		$this->db = $db;
		$this->load = new Loader($this);
	}
	
	function output($overide = false) {
		if ($overide)
			return $this->output;
		echo $this->output;
	}
}

abstract class Model {
	
	function Model() {
		GLOBAL $db;
		$this->db = $db;
	}
	
}

class Loader {
	
	function Loader(&$parent) {
		$this->parent = $parent;
	}
	
	public function get() {
		return $this->parent;
	}
	
	function controller($class, $name = false, $params = array()) {
		if (!$name)
			$name = $class;
			
		if (!empty($this->parent->$name)) {
			show_error("controller name already taken");
			return;
		}
		
		if (file_exists(BASEPATH.'controllers/'.$class.EXT)) {
			require_once (BASEPATH.'controllers/'.$class.EXT);
			$this->parent->$name = new $class($params);
			$this->parent->$name->output =& $this->parent->output;
			return $this->parent->$name;
		} else {
			show_error("controller not found");
		}
	}
	
	function model($class, $name = false, $params = array()) {
		if (is_array($name)) {
			$params = $name;
			$name = $class;
		}
		
		if (!$name)
			$name = $class;
			
		if (!empty($this->parent->$name)) {
			show_error("model name already taken");
			return;
		}
		
		if (file_exists(BASEPATH.'models/'.$class.EXT)) {
			require_once (BASEPATH.'models/'.$class.EXT);
			$this->parent->$name = new $class(implode(",",array_values($params)));
			return $this->parent->$name;
		} else {
			show_error("model not found");
		}
	}
	
	function view($view, $params = array()) {
		$_ci_path = BASEPATH.'views/'.$view.EXT;
		
		if (!file_exists($_ci_path)) {
			show_error("view not found");
			return;
		}
		GLOBAL $i18n;
		extract($params);
		
		ob_start();
		
		include($_ci_path);
		
		$buffer = ob_get_contents();
		@ob_end_clean();
		
		
		$this->parent->output .= $buffer;
					
	}
}