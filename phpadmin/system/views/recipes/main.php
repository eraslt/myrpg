<div>
	<div class="row-fluid">
		<a class="btn btn-small btn-success" href="?recipes/edit">New recipe</a>
	</div>
	<hr>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Recipe</th>
				<th>Delay</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<?php foreach ($recipes as $row){?>
		<tr>
			<td><?=$row['id']?></td>
			<td><?=$row['name']?></td>
			<td><?=$row['delay']?></td>
			<td>
				<a class="btn btn-small btn-success" href="?recipes/edit/<?=$row['id']?>"><?=$i18n->get("Edit")?></a>
			</td>
			<td>
				<a onclick="confirm('Delete?')" class="btn btn-small btn-danger" href="?recipes/del/<?=$row['id']?>"><?=$i18n->get("Remove")?></a>
			</td>
		</tr>
		<?php }?>
	</table>
</div>
