<form method="post" id="formas" enctype="multipart/form-data">
<div class="marketing">
	<table class="table">
		<tr>
			<th>ID</th>
			<th>Recipe</th>
			<th>Delay</th>
		</tr>
		<tr>
			<td>
				<?=$recipe['id']?>&nbsp;
			</td>
			<td>
				<input type="text" name="recipe[name]"  value="<?=$recipe['name']?>">
			</td>
			<td>
				<input type="text" name="recipe[delay]"  value="<?=$recipe['delay']?>">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table class="table">
				<tr>
					<th colspan="2">Required to craft</th>
				</tr>
				<tr>
					<th>Item</th>
					<th>Amount(count)</th>
				</tr>
				<?php foreach ($req_items as $i=>$row) {?>
				<tr>
					<td>
						<select name="ritem[<?=$i?>][item_id]">
							<option></option>
							<?php foreach($items as $id=>$item) {
								$selected = "";
								if ($id==$row['item_id']) $selected = "selected='selected'";
							?>
							<option value="<?=$id?>" <?=$selected?>><?=$item?></option>
							<?php }?>
						</select>
					</td>
					<td>
						<input type="text" name="ritem[<?=$i?>][cnt]"  value="<?=$row['cnt']?>">
					</td>
				</tr>
				<?php }?>
				<?php $i++?>
				<tr>
					<td>
						<select  name="ritem[<?=$i?>][item_id]">
							<option></option>
							<?php foreach($items as $id=>$item) {?>
							<option value="<?=$id?>"><?=$item?></option>
							<?php }?>
						</select>
					</td>
					<td>
						<input type="text" name="ritem[<?=$i?>][cnt]"  value="">
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table class="table">
				<tr>
					<th colspan="2">Result items</th>
				</tr>
				<tr>
					<th>Item</th>
					<th>Amount(count)</th>
				</tr>
				<?php foreach ($res_items as $i=>$row) {?>
				<tr>
					<td>
						<select name="citem[<?=$i?>][item_id]">
							<option></option>
							<?php foreach($items as $id=>$item) {
								$selected = "";
								if ($id==$row['item_id']) $selected = "selected='selected'";
							?>
							<option value="<?=$id?>" <?=$selected?>><?=$item?></option>
							<?php }?>
						</select>
					</td>
					<td>
						<input type="text" name="citem[<?=$i?>][cnt]"  value="<?=$row['cnt']?>">
					</td>
				</tr>
				<?php }?>
				<?php $i++?>
				<tr>
					<td>
						<select  name="citem[<?=$i?>][item_id]">
							<option></option>
							<?php foreach($items as $id=>$item) {?>
							<option value="<?=$id?>"><?=$item?></option>
							<?php }?>
						</select>
					</td>
					<td>
						<input type="text" name="citem[<?=$i?>][cnt]"  value="">
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
			<td>
				<input type="submit" name="submit" class="btn btn-small btn-success" value="<?=$i18n->get("save")?>"/>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
	</table>
</div>
</form>
