<div>
	<div class="row-fluid">
		<a class="btn btn-small btn-success" href="?actions/edit">New action</a>
	</div>
	<hr>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Action</th>
				<th>Delay</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<?php foreach ($actions as $row){?>
		<tr>
			<td><?=$row['id']?></td>
			<td><?=$row['action']?></td>
			<td><?=$row['delay']?></td>
			<td>
				<a class="btn btn-small btn-success" href="?actions/edit/<?=$row['id']?>"><?=$i18n->get("Edit")?></a>
			</td>
			<td>
				<a onclick="confirm('Delete?')" class="btn btn-small btn-danger" href="?actions/del/<?=$row['id']?>"><?=$i18n->get("Remove")?></a>
			</td>
		</tr>
		<?php }?>
	</table>
</div>
