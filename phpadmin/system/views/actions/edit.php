<form method="post" id="formas" enctype="multipart/form-data">
<div class="marketing">
	<table class="table">
		<tr>
			<th>ID</th>
			<th>Action</th>
			<th>Delay</th>
		</tr>
		<tr>
			<td>
				<?=$action['id']?>&nbsp;
			</td>
			<td>
				<input type="text" name="action[action]"  value="<?=$action['action']?>">
			</td>
			<td>
				<input type="text" name="action[delay]"  value="<?=$action['delay']?>">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table class="table">
				<tr>
					<th>Item</th>
					<th>Possibility(%)</th>
					<th>Min quantity</th>
					<th>Max quantity</th>
				</tr>
				<?php foreach ($action_items as $i=>$row) {?>
				<tr>
					<td>
						<select name="aitem[<?=$i?>][item_id]">
							<option></option>
							<?php foreach($items as $id=>$item) {
								$selected = "";
								if ($id==$row['item_id']) $selected = "selected='selected'";
							?>
							<option value="<?=$id?>" <?=$selected?>><?=$item?></option>
							<?php }?>
						</select>
					</td>
					<td>
						<input type="text" name="aitem[<?=$i?>][possibility]"  value="<?=$row['possibility']?>">
					</td>
					<td>
						<input type="text" name="aitem[<?=$i?>][mind]"  value="<?=$row['min']?>">
					</td>
					<td>
						<input type="text" name="aitem[<?=$i?>][max]"  value="<?=$row['max']?>">
					</td>
				</tr>
				<?php }?>
				<?$i++?>
				<tr>
					<td>
						<select  name="aitem[<?=$i?>][item_id]">
							<option></option>
							<?php foreach($items as $id=>$item) {?>
							<option value="<?=$id?>"><?=$item?></option>
							<?php }?>
						</select>
					</td>
					<td>
						<input type="text" name="aitem[<?=$i?>][possibility]"  value="">
					</td>
					<td>
						<input type="text" name="aitem[<?=$i?>][min]"  value="">
					</td>
					<td>
						<input type="text" name="aitem[<?=$i?>][max]"  value="">
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
			<td>
				<input type="submit" name="submit" class="btn btn-small btn-success" value="<?=$i18n->get("save")?>"/>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
	</table>
</div>
</form>
