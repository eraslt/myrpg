<div>
	<div class="row-fluid">
		<a class="btn btn-small btn-success" href="?items/attr_edit">New attribute</a>
	</div>
	<hr>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Caption</th>
				<th>Default</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<?php foreach ($res as $row){?>
		<tr>
			<td><?=$row['id']?></td>
			<td><?=$row['attribute']?></td>
			<td><?=$row['default']?></td>
			<td>
				<a class="btn btn-small btn-success" href="?items/attr_edit/<?=$row['id']?>"><?=$i18n->get("Edit")?></a>
			</td>
			<td>
				<a onclick="confirm('Delete?')" class="btn btn-small btn-danger" href="?items/attr_del/<?=$row['id']?>"><?=$i18n->get("Remove")?></a>
			</td>
		</tr>
		<?php }?>
	</table>
</div>
