<form method="post" id="formas" enctype="multipart/form-data">
<div class="marketing">
	<table class="table">
		<tr>
			<th>ID</th>
			<th>Attribute</th>
			<th>Default</th>
		</tr>
		<tr>
			<td>
				<?=$attr['id']?>&nbsp;
			</td>
			<td>
				<input type="text" name="attribute[attribute]"  value="<?=$attr['attribute']?>">
			</td>
			<td>
				<input type="text" name="attribute[default]"  value="<?=$attr['default']?>">
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
			<td>
				<input type="submit" name="submit" class="btn btn-small btn-success" value="<?=$i18n->get("save")?>"/>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
	</table>
</div>
</form>
