<div>
	<div class="row-fluid">
		<a class="btn btn-small btn-success" href="?items/item_edit">New item</a>
	</div>
	<hr>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Item</th>
				<!-- <th>Type</th> -->
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<?php foreach ($res as $row){?>
		<tr>
			<td><?=$row['id']?></td>
			<td><?=$row['name']?></td>
			<!-- <td>
				<?=$i18n->get("item_type_".$row['type'])?>
			</td> -->
			<td>
				<a class="btn btn-small btn-success" href="?items/item_edit/<?=$row['id']?>"><?=$i18n->get("Edit")?></a>
			</td>
			<td>
				<a onclick="confirm('Delete?')" class="btn btn-small btn-danger" href="?items/item_del/<?=$row['id']?>"><?=$i18n->get("Remove")?></a>
			</td>
		</tr>
		<?php }?>
	</table>
</div>
