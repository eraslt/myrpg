<div class="masthead clearfix">
	<ul class="nav nav-pills pull-left">
		<li class="<?=(empty($active)?"active":"")?>"><a href="?items/attributes"><?=$i18n->get("Attributes")?></a></li>
		<li class="<?=($active=="items"?"active":"")?>"><a href="?items/witems"><?=$i18n->get("Items")?></a></li>
	</ul>
</div>
<hr>
