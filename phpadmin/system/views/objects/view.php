<form method="post" id="formas" enctype="multipart/form-data">
<div class="marketing">
	<table class="table">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Model</th>
		</tr>
		<tr>
			<td>
				<?=$object['id']?>
			</td>
			<td>
				<?=$object['name']?>
			</td>
			<td>
				<?=$object['model']?>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table class="table">
				<tr>
					<th>Action</th>
					<th>&nbsp;</th>
				</tr>
				<?php foreach ($object_actions as $i=>$row) {?>
				<tr>
					<td>
						<?=$row['action']?>
					</td>
					<td>
						<a class="btn btn-small btn-success" href="?actions/edit/<?=$row['action_id']?>"><?=$i18n->get("View")?></a>
					</td>
				</tr>
				<?php }?>
				</table>
			</td>
		</tr>
	</table>
</div>
</form>
