<div>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Model</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<?php foreach ($objects as $row){?>
		<tr>
			<td><?=$row['id']?></td>
			<td><?=$row['name']?></td>
			<td><?=$row['model']?></td>
			<td>
				<a class="btn btn-small btn-success" href="?objects/view/<?=$row['id']?>"><?=$i18n->get("View")?></a>
			</td>
		</tr>
		<?php }?>
	</table>
</div>
