<div class="container">
	<form class="content form-signin" method="post">
		<h2 class="form-signin-heading"><?=$i18n->get("login_header")?></h2>
		<input name="username" type="text" class="input-block-level" placeholder="<?=$i18n->get("username")?>"/>
		<input name="password" type="password" class="input-block-level" placeholder="<?=$i18n->get("password")?>"/>
		<button class="btn btn-large btn-primary" type="submit"><?=$i18n->get("login_button")?></button>
	</form>
</div>
