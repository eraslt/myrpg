<div style="display:inline-block;width:45%;vertical-align: top;">
    <div>
    	<p><b>(v0.4)</b></p>
    	<ul>
    		<li>radnom terrain generation + saving</li>
    		<li>grass</li>
    		<li style="color:green">trees</li>
    		<li style="color:green">skydome + day/night</li>
    		<li style="color:red">athmospheric events?</li>
    		<li style="color:red">other objects</li>
    		<li style="color:green">building</li>
    		<li style="color:red">consuming objects</li>
    		<li style="color:red">skills</li>
    	</ul>
    </div>
    
    <div>
    	<p><b>(v0.3)</b></p>
    	<ul>
    		<li style="color:red">girdLoader + objects</li>
    		<li style="color:green">objects with actions(1 obj x actions)</li>
    		<li style="color:green">crafting</li>
    		<li>ziped data provider for client side</li>
    	</ul>
    </div>
    
    <div>
    	<p><b>(v0.2)</b></p>
    	<ul>
    		<li style="color:red">gui skills visible</li>
    		<li style="color:green">gui tooltip on items</li>
    		<li style="color:green">data interfaces + providers</li>
    		<li style="color:green">add cvs or some simple flat file data provider</li>
    		<li>draw entire code struct</li>
    	</ul>
    </div>
    
    <div>
    	<p><b style="color:green">(v0.1)</b></p>
    	<ul>
    		<li>attribute system concept</li> 
    		<li>storage concept, append/remove in db</li>
    	</ul>
    </div>
    
    <div>
    	<p><b>Required for simple gameplay(v1.0)</b></p>
    	<ul>
    		<li>1 normal object placing script (building mode)</li>
    		<li>2 interaction with object (differnt types of outcomes)</li>
    		<li>3 invenotry</li>
    		<li>4 crafting</li>
    		<li>5 skills</li>
    		<li>6 code struct</li>
    		<li>7 assignable keyboard event - done?</li>
    		<li>8 random object placement</li>
    		<li>9 skydome + atmospheric events</li>
    		<li>10 code optimization and performance (this should be final)</li>
    		<li>11 npc(inlcudes talking and trading with them)</li>
    		<li>12 enemies</li>
    		<li>13 player stats</li>
    		<li>14 gui</li>
    		<li>15 maybe some db (could be flat) for store data (sqlite, Derby, Hypersonic SQL)</li>
    	</ul>
    </div>
</div>
<div style="display:inline-block;width:45%;vertical-align: top;">
  <p><b>TODO:</b></p>
  <ul>
      <li>check/fix gridloader</li>
      <li>model ipostor generator</li>
      <li>make terrain+textures</li>
      <li>add 3rd person char with controls and over shoulder camera</li>
      <li>add tress</li>
      <li>add grass</li>
      <li>add sky(maybe day only? or some planets(suns) in the sky to prevent night?)</li>
      <li>finish work on gui(alteast inventory)</li>
      <li>finish(decide which to leave) data integration</li>
  </ul>
</div>
<div>
    <p><b>to check:</b></p>
    http://hub.jmonkeyengine.org/forum/topic/third-person-cam-over-the-shoulder-perspective/#post-266144
</div>
