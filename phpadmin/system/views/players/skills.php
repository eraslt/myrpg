<div>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Skill</th>
				<th>Description</th>
			</tr>
		</thead>
		<?php foreach ($skills as $row){?>
		<tr>
			<td><?=$row['id']?></td>
			<td><?=$row['skill']?></td>
			<td><?=$row['description']?></td>
		</tr>
		<?php }?>
	</table>
</div>
