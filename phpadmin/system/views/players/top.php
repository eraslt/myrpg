<div class="masthead clearfix">
	<ul class="nav nav-pills pull-left">
		<li class="<?=($active=="players"?"active":"")?>"><a href="?players"><?=$i18n->get("Players")?></a></li>
		<li class="<?=($active=="skills"?"active":"")?>"><a href="?players/skills"><?=$i18n->get("Skills")?></a></li>
	</ul>
</div>
