<!-- http://getbootstrap.com/javascript/#tabs -->
<ul class="nav nav-tabs">
	<li class='active' ><a href="#player" data-toggle="tab">Player</a></li>
	<li><a href="#skills" data-toggle="tab">Skills</a></li>
	<li><a href="#items" data-toggle="tab">Items</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="player">
		<table class="table">
			<tr>
				<th>Attribute</th>
				<th>Value</th>
			</tr>
			<tr>
				<td>ID</td>
				<td><?=$player['id']?></td>
			</tr>
			<tr>
				<td>Username</td>
				<td><?=$player['name']?></td>
			</tr>
			<tr>
				<td>x</td>
				<td><?=$player['x']?></td>
			</tr>
			<tr>
				<td>y</td>
				<td><?=$player['y']?></td>
			</tr>
			<tr>
				<td>z</td>
				<td><?=$player['z']?></td>
			</tr>
		</table>
	</div>
	<div class="tab-pane" id="skills">
		<table class="table">
			<tr>
				<th>Skill</th>
				<th>Lvl</th>
				<th>Exp</th>
			</tr>
			<?php foreach($skills as $skill) {?>
			<tr>
				<td><?=$skill['skill']?></td>
				<td><?=$player_skills[$skill['id']]['lvl']?></td>
				<td><?=$player_skills[$skill['id']]['exp']?></td>
			</tr>
			<?php }?>
		</table>
	</div>
	<div class="tab-pane" id="items">
		<?php 
			$places = array(
				0 => 'Not equiped',
				1 => 'Helm',
			);
		?>
		<a onclick="open_inv(<?=$player['id']?>);return false;" href="javascript:void(0)" >View inventory</a>
		<table class="table">
			<tr>
				<th>Item</th>
				<th>Amount</th>
				<th>Place</th>
			</tr>
			<?php foreach($player_items as $item) {?>
			<tr>
				<td><?=$items[$item['item_id']]?></td>
				<td><?=$item['quantity']?></td>
				<td><?=$places[$item['place']]?></td>
			</tr>
			<?php }?>
		</table>
	</div>
</div>
<script type="text/javascript">
function open_inv(id) {
	window.open("?players/inv/"+id, 'inv', 'width=600,height=600,status=no,resizable=yes,location=no,toolbar=no,menubar=no,scrollbars=no');
}
</script>
