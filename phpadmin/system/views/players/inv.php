<form method="post">
<div class="clearfix" style="padding-bottom:7px;padding-left:15px;">
	<button class="btn btn-small btn-warning" type="submit">Save</button>
</div>
<table class="table" border="1">
	<tr>
	<?php for($i=1; $i<=36; $i++) {?>
		<td id="td-<?=$i?>" slot="<?=$i?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width:60px;height:60px;">
			<?php if ($pitems[$i]) {?>
			<span id="inv_dragable_<?=$i?>" draggable="true" ondrop="swap(event)" ondragover="allowDrop(event)" ondragstart="drag(event)" style="cursor:pointer;padding:3px;border: 1px solid black;display:block;overflow:hidden;">
				<input type="hidden" name="item[<?=$i?>][item_id]" value="<?=$pitems[$i]['item_id']?>" />
				<input type="hidden" name="item[<?=$i?>][cnt]" value="<?=$pitems[$i]['cnt']?>" />
				<input id="pos_inv_dragable_<?=$i?>" type="hidden" name="item[<?=$i?>][slot]" value="<?=$i?>" />
				<?php echo $pitems[$i]['name']?><br/>
				<?php echo $pitems[$i]['cnt']?>
			</span>
			<?php }?>
		</td>
	<?php
	if ($i%6==0) echo "</tr><tr>";
	}?>
	</tr>
</table>
</form>
<script type="text/javascript">
function allowDrop(ev) {
	ev.preventDefault();
}

var drag_parent = null;
var drag_element = null
function drag(ev) {
	//ev.dataTransfer.setData("id",ev.target.id);
	drag_element = document.getElementById(ev.target.id);
	drag_parent = drag_element.parentNode;
}

function drop(ev) {
	ev.preventDefault();
	//var id = ev.dataTransfer.getData("id");
	if (ev.target.nodeName!="TD") return false;
	var a = ev.target.getAttribute("slot");
	document.getElementById("pos_"+drag_element.id).value = ev.target.getAttribute("slot");
	ev.target.appendChild(drag_element);
}

function swap(ev) {
	ev.preventDefault();
	var new_parent = ev.target.parentNode;
	
	document.getElementById("pos_"+ev.target.id).value = drag_parent.getAttribute("slot");
	drag_parent.appendChild(ev.target);
	
	document.getElementById("pos_"+drag_element.id).value = new_parent.getAttribute("slot");
	new_parent.appendChild(drag_element);
}


</script>
</head>