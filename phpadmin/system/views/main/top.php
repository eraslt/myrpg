<div class="content">
	<div class="container-narrow">
		<div class="masthead clearfix">
			<ul class="nav nav-pills pull-left">
				<li class="<?=(empty($active)?"active":"")?>"><a href="?items"><?=$i18n->get("Items")?></a></li>
				<li class="<?=($active=="objects"?"active":"")?>"><a href="?objects"><?=$i18n->get("Objects")?></a></li>
				<li class="<?=($active=="actions"?"active":"")?>"><a href="?actions"><?=$i18n->get("Actions")?></a></li>
				<!-- <li class="<?=($active=="buildings"?"active":"")?>"><a href="?players"><?=$i18n->get("Buildings")?></a></li> -->
				<li class="<?=($active=="recipes"?"active":"")?>"><a href="?recipes"><?=$i18n->get("Recipes")?></a></li>
				<li class="<?=($active=="players"?"active":"")?>"><a href="?players"><?=$i18n->get("Players")?></a></li>
				<li class="<?=($active=="world"?"active":"")?>"><a href="?world"><?=$i18n->get("World")?></a></li>
			</ul>
			<ul class="nav nav-pills pull-right">
				<li class="<?=($active=="data"?"active":"")?>"><a href="?data"><?=$i18n->get("Data")?></a></li>
				<li class="<?=($active=="devlog"?"active":"")?>"><a href="?devlog"><?=$i18n->get("DevLog")?></a></li>
				<!-- <li class="<?=($active=="values"?"active":"")?>"><a href="?admin/values"><?=$i18n->get("config_values")?></a></li>
				<li><a href="?admin/logout"><?=$i18n->get("logout_button")?></a></li> -->
			</ul>
		</div>
		<hr>
