<?php

require(BASEPATH.'processing/Common'.EXT);

if ( ! is_php('5.3'))
{
	@set_magic_quotes_runtime(0); // Kill magic quotes
}


$CFG =& load_class('Config');

require_once BASEPATH.'libraries/pagination.php';
require_once BASEPATH.'libraries/db.php';

include(BASEPATH.'config/database'.EXT);
$db_params = $db[$active_group];
session_start();
	
GLOBAL $db;
$db = new DB();
$db->Connect($db_params['hostname'], $db_params['username'], $db_params['password'], $db_params['database']);
$db->Execute("set names 'utf8'"); 

require_once BASEPATH.'libraries/i18n.php';
GLOBAL $i18n;
$i18n = new I18n();

require(BASEPATH.'f'.EXT);

$URI =& load_class('URI');
$RTR =& load_class('Router');

//$RTR->_parse_routes();

$dir    = $RTR->fetch_directory();
$class  = $RTR->fetch_class();
$method = $RTR->fetch_method();

include_once(APPPATH.'controllers/'.$dir.$class.EXT);

$f = new $class();
if ($f->checkAuth()) {
	if (!method_exists($f,$method))
		show_404();
		
	call_user_func_array(array(&$f, $method), array_slice($URI->rsegments, 2));
}
//override ending
if (method_exists($f,"done"))
	call_user_func(array(&$f, done));
