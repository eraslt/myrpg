<?php
class Model_User extends Model {
	
	private $id = null;
	private $data = Array();
	
	private $table;
	
	function Model_User($table = null) {
		$this->table = new Model_table('users');
		parent::Model();
	}
	
	public function getId() {
		if (@$_SESSION['user']) {
			$user_id = $_SESSION['user']['id'];
			return $user_id;
		} 
		return false;
	}
	
	public function get($value, $id = NULL) {
		if (empty($this->data))
			$this->getData($id);
		return $this->data[$value]; 
	}
	
	public function getData($id = NULL) {
		if (!$id) {
			$_user_id = $this->getId();
		} else {
			$_user_id = $this->db->qstr($id);
		}
		if (empty($this->data)) 
			$this->data = $this->table->getRow("WHERE id=$_user_id");
		return $this->data;
	}
	
	public function logout() {
		$this->data = Array();
		$_SESSION['user']['id'] = NULL;
		unset($_SESSION['user']);
	}
	
	public function login() {
		GLOBAL $db;
		if (@$_SESSION['user']) {
			$user_id = $db->qstr($_SESSION['user']['id']);
			$user = $this->table->getRow("WHERE id=$user_id AND blocked=0");
			if ($user) {
				if ($this)
					$this->user = $user;
				return true;
			}
		}
		$pass = @$_POST['password'];
		$user = @$_POST['username'];
		if (!$pass && !$user) {
			return false;
		}
		
		if (!$pass || !$user) {
			GLOBAL $wrongPass;
			$wrongPass = true;
			return false;
		}
		
		$user = $db->qstr($user);
		$pass = md5($pass);
		$pass = $db->qstr($pass);
		$user = $this->table->getRow("WHERE username=$user AND password=$pass AND blocked=0");
		if (!@$user['id']) {
			GLOBAL $wrongPass;
			$wrongPass = true;
			return false;
		}
		$_SESSION['user'] = $user;
		$this->user = $user;
		return true;
	}
	
	public function getError(){
		return $this->error;
	}
	
	public function error(){
		return $this->error;
	}
}