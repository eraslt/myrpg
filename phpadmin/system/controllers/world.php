<?php

class World extends Controller {

	function World()	{
		parent::Controller();
	}
	
	function index() {
		$this->main();
	}
	
	function main() {
		$this->load->view("main/top", array('active'=>'world'));
		$this->load->view("main/bottom");
	}
}
