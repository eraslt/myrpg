<?php

class Recipes extends Controller {

	function Recipes()	{
		parent::Controller();
		$this->recipes = new Model_table("recipes");
	}
	
	function index() {
		$this->main();
	}
	
	function main() {
		$this->load->view("main/top", array('active'=>'recipes'));
		$this->load->view("recipes/main", array('recipes'=>$this->recipes->getAll()));
		$this->load->view("main/bottom");
	}
	

	function del($id = false) {
		if ($id) {
			$id = $this->db->qstr($id);
			$this->db->execute("DELETE FROM rrpg_recipe_results WHERE recipe_id=".$id);
			$this->db->execute("DELETE FROM rrpg_recipe_items WHERE recipe_id=".$id);
			$this->db->execute("DELETE FROM rrpg_recipes WHERE id=".$id);
		}
		header("Location:?recipes");
	}
	
	function edit($id = false) {
		$this->load->view("main/top", array('active'=>'recipes'));
	
		$data = array();
	
		if (isset($_POST['recipe']) && !empty($_POST['recipe'])) {
			$recipe = $_POST['recipe'];
			if ($id) {
				$this->recipes->update($id, $recipe);
			} else {
				$id = $this->recipes->insert($recipe);
			}
			if ($id && isset($_POST['ritem']) && !empty($_POST['ritem'])) {
				$req_items = new Model_table("recipe_items");

				$this->db->execute("DELETE FROM rrpg_recipe_items WHERE recipe_id=".$id);
				foreach ($_POST['ritem'] as $item) {
					if (empty($item['item_id'])) continue;
					$item['recipe_id'] = $id;
					$req_items->insert($item);
				}
			}
			if ($id && isset($_POST['citem']) && !empty($_POST['citem'])) {
				$res_items = new Model_table("recipe_results");
			
				$this->db->execute("DELETE FROM rrpg_recipe_results WHERE recipe_id=".$id);
				foreach ($_POST['citem'] as $item) {
					if (empty($item['item_id'])) continue;
					$item['recipe_id'] = $id;
					$res_items->insert($item);
				}
			}
			header("Location:?recipes");
		}
	
		$data['items'] = $this->db->getAssoc("SELECT i.id, a.value FROM rrpg_items AS i
				LEFT JOIN rrpg_item_attributes AS a ON (a.item_id=i.id AND a.attribute_id=2)");
		
		$data['req_items'] = array();
		$data['res_items'] = array();
		if (!empty($id)) {
			$id = $this->db->qstr($id);
			$data['recipe'] = $this->recipes->getRow("WHERE id=$id");
			$data['req_items'] = $this->db->getAll("SELECT * FROM rrpg_recipe_items WHERE recipe_id=$id");
			$data['res_items'] = $this->db->getAll("SELECT * FROM rrpg_recipe_results WHERE recipe_id=$id");
		}
	
		$this->load->view("recipes/edit", $data);
		$this->load->view("main/bottom");
	}
}
