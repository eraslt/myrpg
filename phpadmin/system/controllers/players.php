<?php

class Players extends Controller {

	function Players()	{
		parent::Controller();
		$this->player = new Model_table("players");
		$this->skills = new Model_table("skills");
		$this->player_skills = new Model_table("player_skills");
		$this->player_items = new Model_table("player_items");
	}
	
	function index() {
		$this->main();
	}
	
	function main() {
		$this->load->view("main/top", array('active'=>'players'));
		$this->load->view("players/top", array('active'=>'players'));
		$this->load->view("players/players", array(
				'players' => $this->player->getAll()	
			));
		$this->load->view("main/bottom");
	}
	
	function skills() {
		$this->load->view("main/top", array('active'=>'players'));
		$this->load->view("players/top", array('active'=>'skills'));
		$this->load->view("players/skills", array(
				'skills' => $this->skills->getAll()
			));
		$this->load->view("main/bottom");
	}
	
	function edit($id = false) {
		$this->load->view("main/top", array('active'=>'players'));
		$this->load->view("players/top", array('active'=>'players'));
	
		$data = array();
		$data['skills'] = $this->skills->getAll();
		$data['items'] = $this->db->getAssoc("SELECT i.id, ia.value AS name FROM rrpg_items AS i LEFT JOIN rrpg_item_attributes AS ia ON ia.attribute_id=2 AND ia.item_id=i.id ORDER BY i.id;");
		
		if (!empty($id)) {
			$id = $this->db->qstr($id);
			$data['player'] = $this->player->getRow("WHERE id=$id");
			//$data['player_items'] = $this->player_items->getAll("WHERE player_id=$id");
			$data['player_items'] = $this->db->getAll("SELECT item_id, sum(cnt) AS quantity, 0 AS place FROM rrpg_player_inventory WHERE player_id=$id GROUP BY item_id");
			$tmp = $this->player_skills->getAll("WHERE player_id=$id");
			foreach ($tmp as $skill) {
				$data['player_skills'][$skill['skill_id']]  = $skill;
			}
		}

		$this->load->view("players/edit", $data);
		$this->load->view("main/bottom");
	}
	
	function del($id) {
		if (false/*$id*/) {
			$id = $this->db->qstr($id);
			$this->db->execute("DELETE FROM rrpg_item_attributes WHERE attribute_id=".$id);
			$this->db->execute("DELETE FROM rrpg_attributes WHERE id=".$id);
		}
		header("Location:?players");
	}
	
	function inv($id) {
		if (isset($_POST['item']) && !empty($_POST['item'])) {
			$items = $_POST['item'];
			$player = $this->player->getRow("WHERE id=$id");
			if ($player) {
				$this->db->execute("DELETE FROM rrpg_player_inventory WHERE player_id=".$player['id']);
				foreach ($items as $item) {
					$this->db->execute("INSERT INTO rrpg_player_inventory (player_id, slot, item_id, cnt)
							VALUES ({$player['id']},  {$this->db->qstr($item['slot'])},  {$this->db->qstr($item['item_id'])},  {$this->db->qstr($item['cnt'])});");
				}
			}
		}
		
		$data = array();
		$data['items'] = $this->db->getAssoc("SELECT i.id, ia.value AS name FROM rrpg_items AS i LEFT JOIN rrpg_item_attributes AS ia ON ia.attribute_id=2 AND ia.item_id=i.id ORDER BY i.id;");
		
		$pitems = $this->db->getAll("SELECT slot, item_id, cnt FROM rrpg_player_inventory WHERE player_id=".$id);
		foreach ($pitems as $item) {
			$data['pitems'][$item['slot']] = $item;
			$data['pitems'][$item['slot']]['name'] = $data['items'][$item['item_id']];
		}
		
		$this->load->view("players/inv", $data);
	}
}
