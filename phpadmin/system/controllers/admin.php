<?php

class Admin extends Controller {

	function Admin()	{
		parent::Controller();
		$this->auth_required = true;
		$this->authorized = false;
		if (!$this->login()) {
			$this->load->view("login");
			return;
		} else
			$this->authorized = true;
			
		$this->reservations = new Model_Reservations();
		$this->clients = new Model_Clients();
		$this->history = new Model_History();
		$this->user_clients = new Model_User_Clients();
		$this->load->url = "admin";
	}
	
	public function login() {
		$passio = app_get_configuration("admin_password");
		$ussio = app_get_configuration("admin_username");
		if (@$_SESSION['cb1d3a6249c2d223c620393fa6420868']) {
			if($_SESSION['cb1d3a6249c2d223c620393fa6420868']==1)
				return true;
		}

		$pass = trim(@$_POST['password']);
		$user = trim(@$_POST['username']);
		if ($pass==$passio && $user==$ussio) {
			$_SESSION['cb1d3a6249c2d223c620393fa6420868'] = 1;
			return true;
		}
		return false;		
	}
	
	function index() {
		$this->load->view("admin/top");
		$this->load->view("admin/res_top");
		$this->load->view("reservation/filter");
		$this->load->view("admin/main", array(
			"res" => $this->reservations->getFiltered($_POST['filter'])
			)
		);
		$this->load->view("user/bottom");
	}
	
	function clients() {
		$this->load->view("admin/top", array("active"=>"clients"));
		$this->load->view("clients/top");
		$this->load->view("clients/filter", array('filter'=>$_POST['filter']));
		$this->load->view("clients/all", array("clients"=>$this->clients->getFiltered($_POST['filter'])));
		$this->load->view("user/bottom");
	}
	
	function client($id) {
		$errors = array();
		if (isset($_POST['del'])) {
			$this->clients->update($id,array("removed"=>1));
			header("Location:?admin/clients");
			exit;
		}
		if (isset($_POST['client']) && !empty($_POST['client'])) {
			$res = $_POST['client'];
			$_POST = $res;
			$validator = new lib_FormValidator();
			$validator->addValidation("username","alnum", "field_bad_simbols");
			$validator->addValidation("username","req", "field_required");
			$validator->addValidation("email","email", "field_bad_email");
			
			$isnotuniq = $this->db->getOne("SELECT COUNT(*) FROM cms_users 
				WHERE username={$this->db->qstr($res['username'])} AND id!={$this->db->qstr($id)};");
			
			if ($validator->ValidateForm() == FALSE || $isnotuniq){		
				$errors = $validator->GetErrors();
				if ($isnotuniq)
					$errors["username"] = "username_uzimtas";
			} else {
				$res['blocked'] = empty($res['blocked'])?0:1;
				if (!empty($res['password']))
					$res['password'] = md5($res['password']);
				else unset($res['password']);
				$this->clients->update($id, $res);
				header("Location:?admin/client/$id");
			}
		}
		$data = $this->clients->getById($id);
		$data['errors'] = $errors;
		$this->load->view("admin/top", array("active"=>"clients"));
		$this->load->view("clients/one", $data);
		$this->load->view("reservation/separator");
		$this->load->view("admin/main", array(
			'client' => true,
			'res'=>$this->reservations->getFiltered($_POST['filter']," AND r.user_id=".$this->db->qstr($id))
			)
		);
		$this->load->view("clients/separator", array('id'=>$id));
		$this->load->view("clients/filter", array('filter'=>$_POST['filter']));
		$this->user_clients->setParentId($id);
		$this->load->view("clients/clients", array("clients"=>$this->user_clients->getFiltered($_POST['filter'])));
		$this->load->view("user/bottom");
	}
	
	function uclient($id, $cid = false) {
		$errors = array();
		
		if (isset($_POST['del']) && $cid>0) {
			$this->user_clients->update($cid,array("removed"=>1));
			header("Location:?admin/client/$id");
			exit;
		}
		if (isset($_POST['client']) && !empty($_POST['client'])) {
			$res = $_POST['client'];
			$_POST = $res;
			$validator = new lib_FormValidator();
			$validator->addValidation("email","email", "field_bad_email");
				
			if ($validator->ValidateForm() == FALSE){
				$errors = $validator->GetErrors();
			} else {
				$res['user_id'] = $id;
				if ($cid)
					$this->user_clients->update($cid, $res);
				else
					$cid = $this->user_clients->insert($res);
				
				if ($cid)
					header("Location:?admin/client/$id");
			}
		}
		$data = $_POST;
		if ($cid)
			$data = $this->user_clients->getById($cid);
		$data['errors'] = $errors;
		$data['is_new'] = !$cid;
		$data['is_user_client'] = true;
		$this->load->view("admin/top", array("active"=>"clients"));
		$this->load->view("clients/one", $data);
		$this->load->view("user/bottom");
	}
	
	function addclient() {
		$errors = array();
		if (isset($_POST['client']) && !empty($_POST['client'])) {
			$res = $_POST['client'];
			$_POST = $res;
			$validator = new lib_FormValidator();
			$validator->addValidation("username","alnum", "field_bad_simbols");
			$validator->addValidation("username","req", "field_required");
			$validator->addValidation("email","email", "field_bad_email");
			$validator->addValidation("password","req", "field_required");
			
			$isnotuniq = $this->db->getOne("SELECT COUNT(*) FROM cms_users 
				WHERE username={$this->db->qstr($res['username'])};");
			
			if ($validator->ValidateForm() == FALSE || $isnotuniq){		
				$errors = $validator->GetErrors();
				if ($isnotuniq)
					$errors["username"] = "username_uzimtas";
			} else {
				$res['blocked'] = empty($res['blocked'])?0:1;
				$res['password'] = md5($res['password']);
				$id = $this->clients->insert($res);
				if ($id)
					header("Location:?admin/client/$id");
// 				else
// 					header("Location:?admin/addclient");
			}
		}
		$data = $_POST;
		$data['errors'] = $errors;
		$data['is_new'] = true;
		$this->load->view("admin/top", array("active"=>"clients"));
		$this->load->view("clients/one", $data);
		$this->load->view("user/bottom");
	}
	
	function langs($page = 1) {
		$this->load->view("admin/top", array("active"=>"langs"));
		if(isset($_POST['lang'])) {
			foreach ($_POST["lang"] as $string_id=>$value) {
				$value = base64_decode($value);
				$value = trim($value);
				if (empty($value)) continue;
				
				$_string_id = $this->db->qstr($string_id);
				$_value = $this->db->qstr($value);
				$this->db->execute("INSERT INTO cms_lang_values (`lang_id`, `string_id`, `value`) VALUES (1, $_string_id, $_value)
  					ON DUPLICATE KEY UPDATE `value`=$_value;");
			}
		}
		$page *=1;
		$data['strings'] = $this->db->getAssoc("SELECT id, string FROM cms_lang_strings LIMIT $page,10");
		$data["default"] = $this->db->getAssoc("SELECT string_id,value FROM cms_lang_values WHERE lang_id=1");
		$data["langc"] = $this->db->getAssoc("SELECT string_id,value FROM cms_lang_values WHERE lang_id=1");
		$data["current"] = $lang_id;
		
		$cnt = $this->db->getOne("SELECT COUNT(*) FROM cms_lang_strings");
		$data['pag'] = paginate("?admin/langs",$page,$cnt,10);
		$this->load->view("admin/langs",$data);
		$this->load->view("user/bottom");
	}
	
	function values() {
		$this->load->view("admin/top", array("active"=>"values"));
		if(isset($_POST['conf']) && !empty($_POST['conf'])) {
			foreach ($_POST["conf"] as $key=>$value) {
				app_save_configuration($key, trim($value));
			}
		}
		$this->load->view("admin/values", array("conf" => $this->db->getAssoc("SELECT `key`,`value` FROM cms_configuration ORDER BY `key`")));
		$this->load->view("user/bottom");
	}
	
	function logout() {
		$_SESSION['cb1d3a6249c2d223c620393fa6420868'] = 0;
		unset($_SESSION['cb1d3a6249c2d223c620393fa6420868']);
		header("Location:?admin");
	}
	
	function newreg() {
		$dates_err = false;
		if (isset($_POST['res']) && !empty($_POST['res'])) {
			$res = $_POST['res'];
			if ($_POST['selecttype']=="days") {
				$dates = $_POST['dates'];
				if ($this->reservations->checkDates($res['type'], $dates)) {
					if (!empty($dates)) {
						$id = $this->reservations->insert($res);
						$this->history->insert(array("user_id"=>0,"rid"=>$id,"action"=>"new"));
						foreach ($dates as $date) {
							$this->db->execute("INSERT INTO `cms_reservation_days` (`rid`, `date`) 
								VALUES ($id, {$this->db->qstr($date)});");
						}
						header("Location:?admin");
					}
				} else $dates_err = true;
			} else {
				if (!empty($_POST['to']) &&!empty($_POST['from'])) {
					$to = strtotime($_POST['to'])+1;
					$from = strtotime($_POST['from']);
					if ($from<$to) {
						$period = createDateRangeArray(Date("Y-m-d",$from),Date("Y-m-d",$to));
						if ($this->reservations->checkDates($res['type'], $period)) {
							$id = $this->reservations->insert($res);
							$this->history->insert(array("user_id"=>0,"rid"=>$id,"action"=>"new"));
							foreach ( $period as $dt )
								$this->db->execute("INSERT INTO `cms_reservation_days` (`rid`, `date`) 
									VALUES ($id, {$this->db->qstr($dt)});");
							header("Location:?admin");
						} else $dates_err = true;
					}
				}
			}
		}
		$data = array(
			"clients"=> $this->db->getAssoc("SELECT id, caption FROM cms_users WHERE removed=0;"),
			"is_dates_free" => !$dates_err
		);
		$data = array_merge($_POST,$data);
		$this->load->view("admin/top");
		$this->load->view("reservation/new",$data);
		$this->load->view("user/bottom");
	}
	
	function more($id) {
		Global $i18n;
		$mailer = new lib_Mailer();
		$email = $this->db->getOne("SELECT u.email FROM cms_reservation AS r 
				INNER JOIN cms_users AS u ON (u.id=r.user_id) 
				WHERE r.id = {$this->db->qstr($id)}");
		if (isset($_POST['delday'])) {
			$this->db->execute("DELETE FROM cms_reservation_days
				WHERE rid={$this->db->qstr($id)} AND id={$this->db->qstr($_POST['delday'])} LIMIT 1;");
		}
		if (isset($_POST['del'])) {
			$this->history->insert(array("user_id"=>0,"rid"=>$id,"action"=>"cancel"));
			$this->reservations->update($id,array("status"=>2,"removed"=>1));
			if (!empty($email)) {
				$subject = $i18n->get("rezervation_canceled_subject");
				$body = $i18n->get("rezervation_canceled_body");
				$from = app_get_configuration("email_from");
				$fromname = app_get_configuration("email_from_name");
				$replyto = app_get_configuration("email_reply_to");
				$mailer->sendEmail($email, $subject, $body, $from, $fromname, $replyto);
			}
			header("Location:?admin");
		}
		if (isset($_POST['accept'])) {
			$this->history->insert(array("user_id"=>0,"rid"=>$id,"action"=>"accept"));
			$this->reservations->update($id,array("status"=>1,"confirmed"=>Date("Y-m-d h:i:s")));
			if (!empty($email)) {
				$subject = $i18n->get("rezervation_accepted_subject");
				$body = $i18n->get("rezervation_accepted_body");
				$from = app_get_configuration("email_from");
				$fromname = app_get_configuration("email_from_name");
				$replyto = app_get_configuration("email_reply_to");
				$mailer->sendEmail($email, $subject, $body, $from, $fromname, $replyto);
			}
			header("Location:?admin");
		}
		$data = $this->reservations->get($id);
		$data['username'] = $this->clients->getOne("username",$data['reservation']['user_id']);
		$this->load->view("admin/top");
		$this->load->view("reservation/more", $data);
// 		$this->load->view("admin/more");
		$this->load->view("user/bottom");
	}

	function history() {
		$this->load->view("admin/top", array("active"=>"history"));
		$users = $this->db->getAssoc("SELECT id, caption FROM cms_users");
		$users[0] = "Administrator";
		$this->load->view("history/filter", array("users"=>$users,"filter"=>$_POST['filter']));
		$this->load->view("history/main", array(
				"res"=>$this->history->getFiltered($_POST['filter']),
				"users"=>$users
			)
		);
		$this->load->view("user/bottom");
	}
}
