<?php

class Actions extends Controller {

	function Actions()	{
		parent::Controller();
		$this->actions = new Model_table("actions");
	}
	
	function index() {
		$this->main();
	}
	
	function main() {
		$this->load->view("main/top", array('active'=>'actions'));
		$this->load->view("actions/main", array('actions'=>$this->actions->getAll()));
		$this->load->view("main/bottom");
	}
	

	function del($id = false) {
		if ($id) {
			$id = $this->db->qstr($id);
			$this->db->execute("DELETE FROM rrpg_action_items WHERE action_id=".$id);
			$this->db->execute("DELETE FROM rrpg_actions WHERE id=".$id);
		}
		header("Location:?actions");
	}
	
	function edit($id = false) {
		$this->load->view("main/top", array('active'=>'actions'));
	
		$data = array();
	
		if (isset($_POST['action']) && !empty($_POST['action'])) {
			$action = $_POST['action'];
			if ($id) {
				$this->actions->update($id, $action);
			} else {
				$id = $this->actions->insert($action);
			}
			if ($id && isset($_POST['aitem']) && !empty($_POST['aitem'])) {
				$actions_items = new Model_table("action_items");

				$this->db->execute("DELETE FROM rrpg_action_items WHERE action_id=".$id);
				foreach ($_POST['aitem'] as $item) {
					if (empty($item['item_id'])) continue;
					$item['action_id'] = $id;
					$actions_items->insert($item);
				}
			}
			header("Location:?actions");
		}
	
		$data['items'] = $this->db->getAssoc("SELECT i.id, a.value FROM rrpg_items AS i
				LEFT JOIN rrpg_item_attributes AS a ON (a.item_id=i.id AND a.attribute_id=2)");
		
		$data['action_items'] = array();
		if (!empty($id)) {
			$id = $this->db->qstr($id);
			$data['action'] = $this->actions->getRow("WHERE id=$id");
			$data['action_items'] = $this->db->getAll("SELECT * FROM rrpg_action_items WHERE action_id=$id");
		}
	
		$this->load->view("actions/edit", $data);
		$this->load->view("main/bottom");
	}
}
