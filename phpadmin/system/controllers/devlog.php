<?php

class Devlog extends Controller {

	function Devlog()	{
		parent::Controller();
	}
	
	function index() {
		$this->main();
	}
	
	function main() {
		$this->load->view("main/top", array('active'=>'devlog'));
		$this->load->view("devlog/main");
		$this->load->view("main/bottom");
	}
}
