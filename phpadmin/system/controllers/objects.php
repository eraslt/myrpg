<?php

class Objects extends Controller {

	function Objects()	{
		parent::Controller();
	}
	
	function index() {
		$this->main();
	}
	
	function main() {
		$this->load->view("main/top", array('active'=>'objects'));
		$this->load->view("objects/main", array(
				'objects' => $this->db->getAll("SELECT * FROM rrpg_objects")
				));
		$this->load->view("main/bottom");
	}
	
	function view($id) {
		$_id = $this->db->qstr($id);
		$this->load->view("main/top", array('active'=>'objects'));
		$this->load->view("objects/view", array(
				'object' => $this->db->getRow("SELECT * FROM rrpg_objects WHERE id=$_id"),
				'object_actions' => $this->db->getAll("SELECT action_id, action FROM rrpg_object_actions AS oa
						INNER JOIN rrpg_actions AS a ON (a.id=oa.action_id)
						WHERE oa.object_id=$_id"),
				));
		$this->load->view("main/bottom");
	}
}
