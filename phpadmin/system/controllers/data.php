<?php

//http://docs.oracle.com/javase/tutorial/essential/io/datastreams.html
//http://www.oracle.com/technetwork/articles/java/json-1973242.html
class Data extends Controller {

	function Data()	{
		parent::Controller();
	}
	
	function index() {
		$this->main();
	}
	
	function main() {
		$this->load->view("main/top", array('active'=>'data'));
		$this->load->view("data/main");
		$this->load->view("main/bottom");
	}
	
	private function getAttributes() {
		return $this->db->getall("SELECT * FROM rrpg_attributes");
	}
	
	private function getItems() {
		return $this->db->getall("SELECT item_id, attribute_id, value FROM rrpg_item_attributes ORDER BY item_id");
	}
	
	private function getRecipes() {
		$r = $this->db->getall("SELECT id,name,  delay FROM rrpg_recipes");
		$recipes = array();
		foreach ($r as $recipe) {
			$index = $recipe['id']*1;
			$recipes[$index]['name'] = $recipe['name'];
			$recipes[$index]['delay'] = $recipe['delay']*1;
			$required = $this->db->getAll("SELECT * FROM rrpg_recipe_items WHERE recipe_id=".$recipe['id']);
			foreach ($required as $req) {
				$recipes[$index]['required'][$req['item_id']*1] = $req['cnt']*1;;
			}
			$results = $this->db->getall("SELECT * FROM rrpg_recipe_results WHERE recipe_id=".$recipe['id']);
			foreach ($results as $rindex=>$res) {
				$recipes[$index]['results'][$res['item_id']*1] = $res['cnt']*1;;
			}
		}
		return $recipes;
	}
	
	/*public for json array for better perf*/
	public function exportAttributes($return = false) {
		$a = $this->db->getall("SELECT * FROM rrpg_attributes");
		$att = array();
		foreach ($a as $index=>$item) {
			$att[$index][0] = $item['id']*1;
			$att[$index][1] = $item['attribute'];
			$att[$index][2] = $item['default'];
		}
		if ($return) return $att;
		die(json_encode($att));
	}
	
	public function exportItems($return = false) {
		$i = $this->getItems();
		$items = array();
		foreach ($i as $index=>$item) {
			$items[$index][0] = $item['item_id']*1;
			$items[$index][1] = $item['attribute_id']*1;
			$items[$index][2] = $item['value'];
		} 
		if ($return) return $items;
		die(json_encode($items));
	}
	
	public function exportRecipes($return = false) {
		$r = $this->db->getall("SELECT id, name, delay FROM rrpg_recipes");
		$recipes = array();
		foreach ($r as $index=>$recipe) {
			$recipes[$index][0] = $recipe['id']*1;
			$recipes[$index][1] = $recipe['name'];
			$recipes[$index][2] = $recipe['delay']*1;
			$required = $this->db->getAll("SELECT * FROM rrpg_recipe_items WHERE recipe_id=".$recipe['id']);
			$recipes[$index][3] = array();
			foreach ($required as $k1=>$req) {
				$recipes[$index][3][$k1][0] = $req['item_id']*1;
				$recipes[$index][3][$k1][1] = $req['cnt']*1;
			}
			$results = $this->db->getall("SELECT * FROM rrpg_recipe_results WHERE recipe_id=".$recipe['id']);
			$recipes[$index][4] = array();
			foreach ($results as $k2=>$res) {
				$recipes[$index][4][$k2][0] = $res['item_id']*1;
				$recipes[$index][4][$k2][1] = $res['cnt']*1;
			}
		}
		if ($return) return $recipes;
		die(json_encode($recipes));
	}
	
	public function exportActions($return = false) {
		$r = $this->db->getall("SELECT * FROM rrpg_actions");
		$actions = array();
		foreach ($r as $index=>$action) {
			$actions[$index][0] = $action['id']*1;
			$actions[$index][1] = $action['delay']*1;
			$actions[$index][2] = $action['attribute']*1;
			$actions[$index][3] = $action['value']*1;
			$items = $this->db->getAll("SELECT * FROM rrpg_action_items WHERE action_id=".$action['id']);
			$actions[$index][4] = array();
			foreach ($items as $k1=>$item) {
				$actions[$index][4][$k1][0] = $item['item_id']*1;
			}
		}
		if ($return) return $actions;
		die(json_encode($actions));
	}
	
	function export() {
		$zip = new ZipArchive();
		$res = $zip->open("../data/data.jar", ZIPARCHIVE::OVERWRITE);
		$zip->addFromString("info/mindes/data/actions", json_encode($this->exportActions(true)));
		$zip->addFromString("info/mindes/data/attributes", json_encode($this->exportAttributes(true)));
		$zip->addFromString("info/mindes/data/items", json_encode($this->exportItems(true)));
		$zip->addFromString("info/mindes/data/recipes", json_encode($this->exportRecipes(true)));
		$zip->close();
		header("location: http://localhost/rrpg/admin/?data");
		exit;
	}
}
