<?php

class Items extends Controller {

	function Items()	{
		parent::Controller();
		$this->attr = new Model_Table("attributes");
		$this->items = new Model_Table("items");
		$this->item_attr = new Model_Table("item_attributes");
	}
	
	function index() {
		$this->attributes();
	}
	
	function attributes() {
		$this->load->view("main/top");
		$this->load->view("items/top");
		$this->load->view("items/attr", array(
			"res" => $this->attr->getAll()
			)
		);
		$this->load->view("main/bottom");
	}
	
	function witems() {
		$this->load->view("main/top");
		$this->load->view("items/top", array('active'=>'items'));
		$this->load->view("items/items", array(
				"res" => $this->db->getAll("SELECT i.*, ia.value AS name from rrpg_items AS i LEFT JOIN rrpg_item_attributes AS ia ON ia.attribute_id=2 AND ia.item_id=i.id ORDER BY i.id;"),
			)
		);
		$this->load->view("main/bottom");
	}
	
	function attr_del($id = false) {
		if ($id) {
			$id = $this->db->qstr($id);
			$this->db->execute("DELETE FROM rrpg_item_attributes WHERE attribute_id=".$id);
			$this->db->execute("DELETE FROM rrpg_attributes WHERE id=".$id);
		}
		header("Location:?items/attributes");
	}
	
	function attr_edit($id = false) {
		$this->load->view("main/top");
		$this->load->view("items/top", array('active'=>'attributes'));
	
		$data = array();
		$data['attr'] = $this->attr->getAll();
	
		if (isset($_POST['attribute']) && !empty($_POST['attribute'])) {
			$attr = $_POST['attribute'];
			if ($id) {
				$this->attr->update($id, $attr);
			} else {
				$this->attr->insert($attr);
			}
			header("Location:?items/attributes");
		}
		
		if (!empty($id)) {
			$id = $this->db->qstr($id);
			$data['attr'] = $this->attr->getRow("WHERE id=$id");
		}
		
		$this->load->view("items/attr_edit", $data);
		$this->load->view("main/bottom");
	}
	
	function item_del($id = false) {
		if ($id) {
			$id = $this->db->qstr($id);
			$this->db->execute("DELETE FROM rrpg_item_attributes WHERE item_id=".$id);
			$this->db->execute("DELETE FROM rrpg_items WHERE id=".$id);
			
// 			reikia is visu storage containeriu isvalyt
// 			$this->db->execute("DELETE FROM rrpg_player_items WHERE id=".$id);
		}
		header("Location:?items/witems");
	}
	
	function item_edit($id = false) {
		$this->load->view("main/top");
		$this->load->view("items/top", array('active'=>'items'));
		
		$data = array();
		$data['attr'] = $this->attr->getAll();
		
		if (isset($_POST['attribute']) && !empty($_POST['attribute'])) {
			$attr = $_POST['attribute'];
			if (!$id)
				$id = $this->items->insert(array('type' => 0));
			foreach ($attr as $attr_id=>$value) {
				$value = trim($value);
				
				$item_id = $this->db->qstr($id);
				if(!$value)
					$this->db->execute("DELETE FROM rrpg_item_attributes WHERE attribute_id=$attr_id AND item_id=".$item_id);
				else {
					$this->db->execute("INSERT INTO `rrpg_item_attributes` (`item_id`,`attribute_id`,`value`) 
							VALUES ($item_id, $attr_id, {$this->db->qstr($value)})
							ON DUPLICATE KEY UPDATE `value`= {$this->db->qstr($value)};");
				}
			}
			header("Location:?items/witems");
		}
		
		if (!empty($id)) {
			$id = $this->db->qstr($id);
			$data['item'] = $this->items->getRow("WHERE id=$id");
			$data['item_attr'] = $this->db->getAssoc("SELECT attribute_id, value FROM rrpg_item_attributes WHERE item_id=$id");
		}

		$this->load->view("items/item_edit", $data);
		$this->load->view("main/bottom");
	}
}
