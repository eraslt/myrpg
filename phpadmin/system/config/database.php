<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = "default";
$active_record = TRUE;

$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "mindaugas";
$db['default']['database'] = "my_rrpg";
$db['default']['dbdriver'] = "mysqli";
$db['default']['dbprefix'] = "rrpg_";
