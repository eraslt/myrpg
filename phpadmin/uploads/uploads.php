<?php
$callback = $_GET['CKEditorFuncNum'];
$msg='';
$url='';
if (!empty($_FILES) && !empty($_FILES['upload']) && ($_FILES['upload']['error'] == 0) && (is_uploaded_file($_FILES['upload']['tmp_name']))) {
	$filetmpname = microtime().$_FILES['upload']['name'];
	if (!file_exists("uploads/editor/" . $filetmpname)) {
		if(copy($_FILES['upload']['tmp_name'], "uploads/editor/" . $filetmpname)) {
			@unlink($_FILES['upload']['tmp_name']);
			$url = SITE_URL.'/uploads/editor/'.$filetmpname;
		} else {
			$msg = 'Cannot move file!';
		}
	} else {
		$msg = 'File already exists!';
	}
} else {
	$msg = 'File was not uploaded!';
}

$output = '<html><body><script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$callback.', "'.$url.'","'.$msg.'");</script></body></html>';
echo $output;
die;
