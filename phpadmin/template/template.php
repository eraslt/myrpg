<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?=app_get_configuration("site_title","no title")?></title>

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen,projection" href="template/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen,projection" href="template/css/style.css" />
    
	
	<script type="text/javascript">
		history.navigationMode = 'compatible';
	</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script type="text/javascript" src="template/js/bootstrap.min.js"></script>
</head>
<body>
<!-- 
Players<br/>
	player skills<br/>
	player atributes<br/>
items<br/>
	item atributes<br/>
actions<br/>
	action items<br/>
reciptes<br/>
atributes<br/>
skills<br/>
 -->
	<?php echo $f->output();?>
</body>
</html>
