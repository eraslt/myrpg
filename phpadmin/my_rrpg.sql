-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2014 at 06:53 PM
-- Server version: 5.5.34-0ubuntu0.13.04.1
-- PHP Version: 5.4.9-4ubuntu2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_rrpg`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_configuration`
--

CREATE TABLE IF NOT EXISTS `cms_configuration` (
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_configuration`
--

INSERT INTO `cms_configuration` (`key`, `value`) VALUES
('admin_username', 'admin'),
('site_title', 'Admin'),
('admin_password', 'testas'),
('site_url', 'http://localhost/SurCraft/admin/');

-- --------------------------------------------------------

--
-- Table structure for table `cms_langs`
--

CREATE TABLE IF NOT EXISTS `cms_langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_langs`
--

INSERT INTO `cms_langs` (`id`, `title`, `short`, `sort`) VALUES
(1, 'Lietuviškai', 'LT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_lang_strings`
--

CREATE TABLE IF NOT EXISTS `cms_lang_strings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `string` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`string`),
  UNIQUE KEY `string` (`string`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `cms_lang_strings`
--

INSERT INTO `cms_lang_strings` (`id`, `string`) VALUES
(1, 'logout_button'),
(3, 'filter_empty'),
(4, 'mano_reservacijos'),
(5, 'new_reservation_button'),
(6, 'reservation_time'),
(7, 'reservation_date'),
(8, 'reservation_info'),
(9, 'detaliau'),
(10, 'reservation_type_0'),
(11, 'reservation_type_1'),
(12, 'reservation_type_2'),
(13, 'reservation_status_0'),
(14, 'reservation_status_1'),
(15, 'reservation_status_2'),
(16, 'new_reservation'),
(17, 'rezervuoti'),
(18, 'reservation_type_empty'),
(19, 'reservation'),
(20, 'trinti'),
(21, 'admin_reservation'),
(22, 'admin_clients'),
(23, 'config_values'),
(24, 'config_langs'),
(25, 'year'),
(26, 'month'),
(27, 'day'),
(28, 'status'),
(29, 'client'),
(30, 'new_client_button'),
(31, 'filtras'),
(32, 'ieskoti'),
(33, 'client_username'),
(34, 'client_contact'),
(35, 'username'),
(36, 'contacts'),
(37, 'email'),
(38, 'password'),
(39, 'blocked'),
(40, 'admin_username'),
(41, 'admin_password'),
(42, 'site_title'),
(43, 'site_url'),
(44, 'smtp_host'),
(45, 'smtp_user'),
(46, 'smtp_pass'),
(47, 'email_from'),
(48, 'email_reply_to'),
(49, 'login_header'),
(50, 'login_button'),
(51, 'month_1'),
(52, 'month_2'),
(53, 'month_3'),
(54, 'month_4'),
(55, 'month_5'),
(56, 'month_6'),
(57, 'month_7'),
(58, 'month_8'),
(59, 'month_9'),
(60, 'month_10'),
(61, 'month_11'),
(62, 'month_12'),
(63, 'reservations'),
(64, 'save'),
(65, 'new_client'),
(66, 'patvirtinti'),
(67, 'atsaukti'),
(68, 'ar_trinti'),
(69, 'password_leave_empty'),
(70, 'field_bad_simbols'),
(71, 'field_required'),
(72, 'field_bad_email'),
(73, 'username_uzimtas'),
(74, 'rezervation_accepted_subject'),
(75, 'rezervation_accepted_body'),
(76, 'email_from_name'),
(77, 'rezervation_canceled_subject'),
(78, 'rezervation_canceled_body'),
(79, 'empty_value'),
(80, 'user_clients'),
(81, 'add_new'),
(82, 'caption'),
(83, 'code'),
(84, 'vat'),
(85, 'contact'),
(86, 'phone'),
(87, 'www'),
(88, 'info'),
(89, 'selected_days_taken');

-- --------------------------------------------------------

--
-- Table structure for table `cms_lang_values`
--

CREATE TABLE IF NOT EXISTS `cms_lang_values` (
  `lang_id` int(11) NOT NULL,
  `string_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`lang_id`,`string_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_lang_values`
--

INSERT INTO `cms_lang_values` (`lang_id`, `string_id`, `value`) VALUES
(1, 1, 'Logout'),
(1, 3, '----------'),
(1, 50, 'Login'),
(1, 49, 'Login'),
(1, 4, 'My reservations'),
(1, 5, '+New reservation'),
(1, 6, 'Reserved on'),
(1, 7, 'Reservation date'),
(1, 8, 'Info'),
(1, 9, 'More'),
(1, 10, 'Full residence'),
(1, 11, 'First housing'),
(1, 12, 'Second housing'),
(1, 13, 'New'),
(1, 14, 'Confirmed'),
(1, 15, 'Canceled'),
(1, 16, 'New reservation'),
(1, 17, 'Make reservation'),
(1, 18, '--------'),
(1, 19, 'Reservation'),
(1, 20, 'Delete'),
(1, 21, 'Reservations'),
(1, 22, 'Clients'),
(1, 23, 'Config'),
(1, 24, 'Translations'),
(1, 25, 'Year'),
(1, 26, 'Month'),
(1, 27, 'Day'),
(1, 28, 'Status'),
(1, 29, 'Client'),
(1, 30, 'New client'),
(1, 31, 'Search'),
(1, 32, 'Search'),
(1, 33, 'Username'),
(1, 34, 'Contacts'),
(1, 35, 'Username'),
(1, 36, 'Contacts'),
(1, 37, 'E-mail'),
(1, 38, 'Password'),
(1, 39, 'Blocked'),
(1, 40, 'Admin username'),
(1, 41, 'Admin password'),
(1, 42, 'Site title'),
(1, 43, 'Site URL'),
(1, 44, 'SMTP host'),
(1, 45, 'SMTP username'),
(1, 46, 'SMTP password'),
(1, 47, 'E-mail "From"'),
(1, 48, 'E-mail "Reply-to"'),
(1, 51, 'January'),
(1, 52, 'February'),
(1, 53, 'March'),
(1, 54, 'April'),
(1, 55, 'May'),
(1, 56, 'June'),
(1, 57, 'July'),
(1, 58, 'August'),
(1, 59, 'September'),
(1, 60, 'October'),
(1, 61, 'November'),
(1, 62, 'December'),
(1, 63, 'Reservations'),
(1, 64, 'Save/add'),
(1, 65, 'New client'),
(1, 66, 'Confirm'),
(1, 67, 'Cancel'),
(1, 68, 'Are you sure?'),
(1, 69, 'Norėdami pakeisti, įrašykite nauja spaltažodi, priešingu atveju, palikite tuščią.'),
(1, 70, 'Neleistini simboliai. Leidžiama tik raidės ir skaičai.'),
(1, 71, 'Privalomas laukas.'),
(1, 72, 'Blogas el. pašto adresas'),
(1, 73, 'Toks vartotojo prisijungimo vardas užimtas.'),
(1, 76, 'E-mail "From name"'),
(1, 74, 'Registracija patvirtinta'),
(1, 75, '<p>Registracija buvo patvirtinta.</p>'),
(1, 77, 'Registracija atšaukta'),
(1, 78, '<p>Registracija buvo at&scaron;aukta.</p>'),
(1, 79, 'No value'),
(1, 82, 'Client'),
(1, 83, 'Code'),
(1, 84, 'Vat number'),
(1, 85, 'Contact person'),
(1, 86, 'Phone'),
(1, 87, 'Website'),
(1, 88, 'Additional info'),
(1, 80, 'Client clients'),
(1, 81, 'Add new');

-- --------------------------------------------------------

--
-- Table structure for table `mmo_dialogs`
--

CREATE TABLE IF NOT EXISTS `mmo_dialogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text CHARACTER SET utf8 NOT NULL,
  `final` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mmo_dialogs`
--

INSERT INTO `mmo_dialogs` (`id`, `text`, `final`) VALUES
(1, 'go and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n\r\ngo and do something useful! this is dialog example :P\r\n<br/>\r\n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_images`
--

CREATE TABLE IF NOT EXISTS `mmo_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `mmo_images`
--

INSERT INTO `mmo_images` (`id`, `name`) VALUES
(14, 'gold-coins-large.png'),
(13, 'item110.png'),
(12, 'head-of-cabbage.png'),
(11, 'enchanted_shield.png'),
(10, 'ash_wood.png'),
(9, '300-sword.png'),
(15, 'barrel_01.gif');

-- --------------------------------------------------------

--
-- Table structure for table `mmo_inventory_images`
--

CREATE TABLE IF NOT EXISTS `mmo_inventory_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `mmo_inventory_images`
--

INSERT INTO `mmo_inventory_images` (`id`, `name`) VALUES
(16, 'sword1.png'),
(21, 'pants1.png'),
(20, 'heml1.png'),
(19, 'boots1.png'),
(18, 'armor1.png'),
(17, 'shield1.png'),
(22, 'sword2.png'),
(24, 'sword3.png');

-- --------------------------------------------------------

--
-- Table structure for table `mmo_map_actions`
--

CREATE TABLE IF NOT EXISTS `mmo_map_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `map_id` int(11) NOT NULL DEFAULT '1',
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `action` int(11) NOT NULL DEFAULT '0',
  `sprite` int(11) NOT NULL,
  `param` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `map_id` (`map_id`,`x`,`y`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2115 ;

--
-- Dumping data for table `mmo_map_actions`
--

INSERT INTO `mmo_map_actions` (`id`, `map_id`, `x`, `y`, `action`, `sprite`, `param`) VALUES
(239, 3, 3, 8, 1, 0, '1'),
(2114, 9, 19, 17, 7, 0, '0'),
(2113, 9, 18, 15, 7, 0, '0'),
(2112, 9, 17, 15, 7, 0, '0'),
(2111, 9, 17, 10, 6, 0, '0'),
(1429, 7, 7, 0, 4, 45, '2'),
(1430, 7, 7, 11, 4, 45, '2'),
(2110, 9, 16, 17, 7, 0, '0'),
(2098, 1, -1, 7, 4, -666, '0'),
(2097, 1, -2, 14, 4, -666, '0'),
(2109, 9, 15, 10, 6, 0, '0'),
(2108, 9, 15, 8, 6, 0, '0'),
(2107, 9, 14, 17, 7, 0, '0'),
(2106, 9, 14, 15, 7, 0, '0'),
(2105, 9, 13, 10, 6, 0, '0'),
(2104, 9, 13, 8, 6, 0, '0'),
(2103, 9, 12, 17, 7, 0, '0'),
(2102, 9, 12, 15, 7, 0, '0'),
(2101, 9, 11, 10, 6, 0, '0'),
(240, 3, 3, 9, 1, 0, '1'),
(241, 3, 3, 10, 1, 0, '1'),
(242, 3, 4, 8, 1, 0, '1'),
(243, 3, 4, 9, 1, 0, '1'),
(244, 3, 4, 10, 1, 0, '1'),
(245, 3, 5, 8, 1, 0, '1'),
(246, 3, 5, 9, 1, 0, '1'),
(247, 3, 5, 10, 1, 0, '1'),
(248, 3, 6, 8, 1, 0, '1'),
(249, 3, 6, 9, 1, 0, '1'),
(250, 3, 6, 10, 1, 0, '1'),
(2100, 9, 11, 8, 6, 0, '0'),
(2096, 1, 38, 4, 4, -666, '0'),
(2095, 1, 34, 10, 8, 0, '1'),
(2094, 1, 34, 3, 4, -666, '0'),
(2093, 1, 32, 24, 4, -666, '0'),
(2092, 1, 32, 15, 4, -666, '0'),
(2091, 1, 31, 33, 4, -666, '0'),
(2090, 1, 30, 15, 4, -666, '0'),
(2089, 1, 29, -1, 4, -666, '0'),
(2088, 1, 28, 29, 4, -666, '0'),
(2087, 1, 26, 16, 4, -666, '0'),
(2086, 1, 25, 33, 4, -666, '0'),
(2085, 1, 25, 13, 4, -666, '0'),
(2084, 1, 24, -12, 4, -666, '0'),
(2083, 1, 22, -5, 4, -666, '0'),
(2082, 1, 21, 9, 9, 0, '1'),
(2081, 1, 19, 0, 4, -666, '0'),
(1427, 8, 20, 6, 5, 0, '3'),
(2080, 1, 18, -5, 4, -666, '0'),
(2079, 1, 18, 14, 4, -666, '0'),
(2078, 1, 18, 10, 4, -666, '0'),
(2077, 1, 17, 19, 4, -666, '0'),
(2076, 1, 17, 6, 4, -666, '0'),
(2075, 1, 16, 12, 2, 0, '1'),
(2074, 1, 12, -3, 4, -666, '0'),
(2073, 1, 7, 12, 2, 0, '2'),
(2072, 1, 1, 22, 4, -666, '0'),
(1428, 8, 21, 9, 1, 0, '99'),
(2099, 9, 10, 8, 6, 0, '0'),
(1435, 7, 17, 12, 4, 45, '0'),
(1434, 7, 17, 8, 1, 0, '99'),
(1433, 7, 16, 8, 1, 0, '99'),
(1432, 7, 12, 0, 4, 45, '2'),
(1431, 7, 11, 8, 2, 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `mmo_map_mobs`
--

CREATE TABLE IF NOT EXISTS `mmo_map_mobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `map_id` int(11) NOT NULL DEFAULT '1',
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `mob_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `map_id` (`map_id`,`x`,`y`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `mmo_map_mobs`
--

INSERT INTO `mmo_map_mobs` (`id`, `map_id`, `x`, `y`, `mob_id`) VALUES
(43, 1, 3, 22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_mobs`
--

CREATE TABLE IF NOT EXISTS `mmo_mobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `range` smallint(6) NOT NULL,
  `speed` smallint(6) NOT NULL,
  `respawn` int(11) NOT NULL,
  `hp` int(11) NOT NULL,
  `low` int(11) NOT NULL,
  `high` int(11) NOT NULL,
  `count` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mmo_mobs`
--

INSERT INTO `mmo_mobs` (`id`, `filename`, `range`, `speed`, `respawn`, `hp`, `low`, `high`, `count`) VALUES
(1, '02.png', 4, 4, 10, 100, 10, 50, 2),
(2, 'evv.png', 4, 4, 0, 200, 30, 70, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_mob_items`
--

CREATE TABLE IF NOT EXISTS `mmo_mob_items` (
  `mob_id` mediumint(9) NOT NULL,
  `item_id` mediumint(9) NOT NULL,
  `posibility` smallint(6) NOT NULL,
  `quantity_min` smallint(6) NOT NULL,
  `quantity_max` int(11) NOT NULL,
  PRIMARY KEY (`mob_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mmo_mob_items`
--

INSERT INTO `mmo_mob_items` (`mob_id`, `item_id`, `posibility`, `quantity_min`, `quantity_max`) VALUES
(1, 7, 20, 1, 1),
(1, 3, 70, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_npcs`
--

CREATE TABLE IF NOT EXISTS `mmo_npcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `dialog` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `mapid` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mmo_npcs`
--

INSERT INTO `mmo_npcs` (`id`, `name`, `dialog`, `file`, `mapid`, `x`, `y`) VALUES
(1, 'Tommy', '', '118', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_objects`
--

CREATE TABLE IF NOT EXISTS `mmo_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mmo_objects`
--

INSERT INTO `mmo_objects` (`id`, `name`, `description`) VALUES
(1, 'Cabbage', ''),
(2, 'Basic Shield', ''),
(3, 'Wooden Sword', ''),
(4, 'Healing Potion', '');

-- --------------------------------------------------------

--
-- Table structure for table `mmo_perks`
--

CREATE TABLE IF NOT EXISTS `mmo_perks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `action` varchar(100) NOT NULL,
  `effect` varchar(100) NOT NULL,
  `rare` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mmo_player_profession`
--

CREATE TABLE IF NOT EXISTS `mmo_player_profession` (
  `uid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `something` int(11) NOT NULL,
  `something2` int(11) NOT NULL,
  `something3` int(11) NOT NULL,
  `something4` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mmo_player_profession`
--

INSERT INTO `mmo_player_profession` (`uid`, `pid`, `something`, `something2`, `something3`, `something4`, `id`) VALUES
(1, 1, 0, 0, 0, 0, 1),
(1, 5, 0, 0, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_player_quests`
--

CREATE TABLE IF NOT EXISTS `mmo_player_quests` (
  `player_id` int(11) NOT NULL,
  `quest_id` int(11) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`player_id`,`quest_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mmo_player_quests`
--

INSERT INTO `mmo_player_quests` (`player_id`, `quest_id`, `completed`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_player_spells`
--

CREATE TABLE IF NOT EXISTS `mmo_player_spells` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `spell_id` int(11) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `place` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `mmo_player_spells`
--

INSERT INTO `mmo_player_spells` (`id`, `player_id`, `spell_id`, `last_used`, `place`) VALUES
(1, 1, 1, '2011-11-04 20:31:53', 1),
(2, 1, 2, '2011-11-04 20:31:44', 3),
(3, 1, 3, '2011-11-04 20:31:51', 8);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_professions`
--

CREATE TABLE IF NOT EXISTS `mmo_professions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `something` int(11) NOT NULL DEFAULT '0',
  `something1` int(11) NOT NULL DEFAULT '0',
  `something2` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mmo_professions`
--

INSERT INTO `mmo_professions` (`id`, `name`, `something`, `something1`, `something2`) VALUES
(1, 'Blacksmith', 0, 0, 0),
(2, 'Engineer', 0, 0, 0),
(3, 'Herbalist', 0, 0, 0),
(4, 'Tailor', 0, 0, 0),
(5, 'Jewel Crafter', 0, 0, 0),
(6, 'Sorcerer', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_shops`
--

CREATE TABLE IF NOT EXISTS `mmo_shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) NOT NULL,
  `vat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mmo_shops`
--

INSERT INTO `mmo_shops` (`id`, `caption`, `vat`) VALUES
(1, 'test shop', 15);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_shop_items`
--

CREATE TABLE IF NOT EXISTS `mmo_shop_items` (
  `shop_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`shop_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mmo_shop_items`
--

INSERT INTO `mmo_shop_items` (`shop_id`, `item_id`, `quantity`) VALUES
(1, 2, 10),
(1, 9, 15),
(1, 7, 6),
(1, 8, 3),
(1, 4, 5),
(1, 1, 58);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_spells`
--

CREATE TABLE IF NOT EXISTS `mmo_spells` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `low` int(11) NOT NULL,
  `high` int(11) NOT NULL,
  `mana` int(11) NOT NULL,
  `reload` int(11) NOT NULL,
  `miss` varchar(3) NOT NULL,
  `critical` varchar(3) NOT NULL,
  `levelreq` int(11) NOT NULL,
  `image` varchar(75) NOT NULL,
  `animation` varchar(255) NOT NULL,
  `frames` smallint(6) NOT NULL,
  `target` enum('center','feet') NOT NULL,
  `type` enum('target','range') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `mmo_spells`
--

INSERT INTO `mmo_spells` (`id`, `name`, `low`, `high`, `mana`, `reload`, `miss`, `critical`, `levelreq`, `image`, `animation`, `frames`, `target`, `type`) VALUES
(1, 'Lightning Strike', 50, 100, 50, 3, '10', '3', 1, 'lightning.jpg', '', 0, 'center', 'target'),
(2, 'Ice Blast', 25, 60, 30, 20, '9', '4', 1, 'iceblast.jpg', '', 0, 'center', 'target'),
(3, 'hit', 10, 15, 0, 3, '10', '3', 0, 'lightning.jpg', '', 0, 'center', 'target');

-- --------------------------------------------------------

--
-- Table structure for table `mmo_sprites`
--

CREATE TABLE IF NOT EXISTS `mmo_sprites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=196 ;

--
-- Dumping data for table `mmo_sprites`
--

INSERT INTO `mmo_sprites` (`id`, `filename`, `type`) VALUES
(44, 'treestump.png', 3),
(48, 'test.png', 25),
(45, 'treestump2.png', 3),
(41, 'logs_01.gif', 3),
(49, 'medieval_building_tiles_01.png', 8),
(50, 'medieval_building_tiles_02.png', 8),
(51, 'medieval_building_tiles_03.png', 8),
(52, 'medieval_building_tiles_04.png', 8),
(53, 'medieval_building_tiles_05.png', 8),
(54, 'medieval_building_tiles_06.png', 8),
(55, 'medieval_building_tiles_07.png', 8),
(56, 'medieval_building_tiles_08.png', 8),
(57, 'medieval_building_tiles_09.png', 8),
(58, 'medieval_building_tiles_10.png', 8),
(59, 'medieval_building_tiles_11.png', 8),
(60, 'medieval_building_tiles_12.png', 8),
(61, 'medieval_building_tiles_13.png', 8),
(62, 'medieval_building_tiles_14.png', 8),
(63, 'medieval_building_tiles_15.png', 8),
(64, 'medieval_building_tiles_16.png', 8),
(65, 'medieval_building_tiles_17.png', 8),
(66, 'medieval_building_tiles_18.png', 8),
(67, 'medieval_building_tiles_19.png', 8),
(68, 'medieval_building_tiles_20.png', 8),
(69, 'medieval_building_tiles_21.png', 8),
(70, 'medieval_building_tiles_22.png', 8),
(71, 'medieval_building_tiles_23.png', 8),
(72, 'medieval_building_tiles_24.png', 8),
(73, 'medieval_building_tiles_25.png', 8),
(74, 'medieval_building_tiles_26.png', 8),
(75, 'medieval_building_tiles_27.png', 8),
(76, 'medieval_building_tiles_28.png', 8),
(77, 'medieval_building_tiles_29.png', 8),
(78, 'medieval_building_tiles_30.png', 8),
(79, 'medieval_building_tiles_31.png', 8),
(80, 'medieval_building_tiles_32.png', 8),
(81, 'medieval_building_tiles_33.png', 8),
(82, 'medieval_building_tiles_34.png', 8),
(83, 'medieval_building_tiles_35.png', 8),
(84, 'medieval_building_tiles_36.png', 8),
(85, 'medieval_building_tiles_37.png', 8),
(86, 'medieval_building_tiles_38.png', 8),
(87, 'medieval_building_tiles_39.png', 8),
(88, 'medieval_building_tiles_40.png', 8),
(89, 'medieval_building_tiles_41.png', 8),
(90, 'medieval_building_tiles_42.png', 8),
(91, 'medieval_building_tiles_43.png', 8),
(92, 'medieval_building_tiles_44.png', 8),
(93, 'medieval_building_tiles_45.png', 8),
(94, 'medieval_building_tiles_46.png', 8),
(95, 'medieval_building_tiles_47.png', 8),
(96, 'medieval_building_tiles_48.png', 8),
(97, 'medieval_building_tiles_49.png', 8),
(98, 'medieval_building_tiles_50.png', 8),
(99, 'medieval_building_tiles_51.png', 8),
(100, 'medieval_building_tiles_52.png', 8),
(101, 'medieval_building_tiles_53.png', 8),
(102, 'medieval_building_tiles_54.png', 8),
(103, 'medieval_building_tiles_55.png', 8),
(104, 'medieval_building_tiles_56.png', 8),
(105, 'medieval_building_tiles_57.png', 8),
(106, 'medieval_building_tiles_58.png', 8),
(107, 'medieval_building_tiles_59.png', 8),
(108, 'medieval_building_tiles_60.png', 8),
(109, 'barrel_01.gif', 0),
(110, 'tree.png', 3),
(111, 'tree2.png', 3),
(112, 'tree1.png', 3),
(113, 'treelarge.png', 3),
(114, 'trunk_large.gif', 3),
(115, 'trunk_smal.gif', 3),
(116, 'sign1.png', 6),
(117, 'temple_entrance_vines_0.png', 1),
(118, 'me.png', 24),
(119, '68px-Stone_Bend.png', 6),
(120, '68px-Stone_Road.png', 6),
(121, '72px-Stone_Crossing.png', 6),
(122, '37px-Yellow_Flower_Bed.png', 0),
(123, '37px-Red_Flower_Bed.png', 0),
(124, '37px-Violet_Flower_Bed.png', 0),
(125, '37px-White_Flower_Bed.png', 0),
(126, '40px-Small_Cactus.png', 0),
(127, '50px-AlpineGoat.png', 5),
(128, '50px-Chamoise.png', 5),
(129, '50px-ForestFoal.png', 5),
(130, '50px-ForestHorse.png', 5),
(131, '50px-RoeDeer.png', 5),
(132, '50px-Fence.png', 23),
(133, 'npc.png', 24),
(134, 'tree.png', 3),
(139, '50px-fence2.png', 23),
(140, 'tree_01.png', 3),
(141, 'tree_02.png', 3),
(142, 'tree_03.png', 3),
(143, 'tree_04.png', 3),
(144, 'tree_05.png', 3),
(145, 'tree_06.png', 3),
(146, 'tree_07.png', 3),
(147, 'tree_08.png', 3),
(148, 'tree_09.png', 3),
(149, 'tree_10.png', 3),
(150, 'tree_11.png', 3),
(151, 'tree_12.png', 3),
(152, 'tree_13.png', 3),
(153, 'tree_14.png', 3),
(154, 'tree_15.png', 3),
(155, 'tree_16.png', 3),
(156, 'appletree.png', 3),
(157, 'testile.png', 0),
(158, 'tree3.png', 3),
(159, 'tree4.png', 3),
(160, 'tree5.png', 3),
(161, 'tree6.png', 3),
(162, 'corner.png', 10),
(163, 'tile.png', 10),
(164, 'tile2.png', 10),
(165, '1.png', 0),
(166, '2.png', 0),
(167, '3.png', 0),
(168, '4.png', 0),
(169, '5.png', 0),
(170, '6.png', 0),
(171, '7.png', 0),
(172, '8.png', 0),
(173, '9.png', 0),
(174, '10.png', 0),
(175, '11.png', 0),
(181, '12.png', 0),
(182, '13.png', 0),
(183, '14.png', 0),
(184, '15.png', 0),
(185, '16.png', 0),
(186, '17.png', 0),
(187, '18.png', 0),
(188, '19.png', 0),
(189, '20.png', 0),
(190, 'grassup.png', 0),
(191, '21.png', 0),
(193, 'merchant_tent_0.png', 0),
(195, 'Campfire3.gif', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_templates`
--

CREATE TABLE IF NOT EXISTS `mmo_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mmo_templates`
--

INSERT INTO `mmo_templates` (`id`, `name`) VALUES
(6, 'adssdd'),
(5, '1221'),
(4, 'tttt'),
(7, 'sdadsd'),
(8, '11111112'),
(9, '4564'),
(10, 'hhhh');

-- --------------------------------------------------------

--
-- Table structure for table `mmo_template_items`
--

CREATE TABLE IF NOT EXISTS `mmo_template_items` (
  `template_id` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `s` int(11) NOT NULL,
  PRIMARY KEY (`template_id`,`x`,`y`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mmo_template_items`
--

INSERT INTO `mmo_template_items` (`template_id`, `x`, `y`, `s`) VALUES
(6, 23, -9, 126),
(6, 20, -7, 126),
(5, 9, 4, 124),
(5, 10, 0, 124),
(5, 8, 5, 124),
(5, 5, 0, 142),
(4, 0, 0, 142),
(4, 5, 0, 142),
(6, 24, -3, 126),
(6, 27, -5, 126),
(7, 0, 4, 109),
(7, 3, 7, 109),
(7, 4, 0, 109),
(7, 7, 6, 109),
(8, 17, 5, 123),
(8, 17, 9, 123),
(8, 19, 7, 123),
(8, 22, 10, 123),
(9, 2, 7, 123),
(9, 4, 1, 123),
(9, 5, 6, 123),
(9, 1, 4, 123),
(9, 0, 0, 123),
(10, 0, 0, 165);

-- --------------------------------------------------------

--
-- Table structure for table `mmo_tiles`
--

CREATE TABLE IF NOT EXISTS `mmo_tiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `mmo_tiles`
--

INSERT INTO `mmo_tiles` (`id`, `filename`, `type`) VALUES
(10, 'grass.png', 0),
(14, '15.gif', 9),
(15, 'sand.gif', 0),
(16, 'sand2.gif', 0),
(17, 'tiletest.gif', 0),
(19, 'tilesand.png', 2),
(25, 'water.png', 4),
(24, 'grass2.png', 0),
(27, 'floor.png', 13),
(28, 'exit.png', 13),
(29, 'tile1_01.png', 0),
(30, 'tile1_02.png', 0),
(31, 'tile1_03.png', 0),
(32, 'tile1_04.png', 0),
(33, 'tile1_05.png', 0),
(34, 'tile1_06.png', 0),
(35, 'muddygrass.png', 0),
(36, 'grass5.png', 0),
(37, 'grass_and_water_0.png', 0),
(38, 'grass_and_water_1.png', 0),
(39, 'grass_and_water_2.png', 0),
(40, 'grass_and_water_3.png', 0),
(41, 'grass_and_water_4.png', 0),
(42, 'grass_and_water_5.png', 0),
(43, 'grass_and_water_6.png', 0),
(44, 'grass_and_water_7.png', 0),
(45, 'grass_and_water_8.png', 0),
(46, 'grass_and_water_9.png', 0),
(47, 'grass_and_water_10.png', 0),
(48, 'grass_and_water_11.png', 0),
(49, 'grass_and_water_12.png', 0),
(50, 'grass_and_water_13.png', 0),
(51, 'grass_and_water_14.png', 0),
(52, 'grass_and_water_15.png', 0),
(53, 'grass_and_water_16.png', 0),
(54, 'grass_and_water_17.png', 0),
(55, 'grass_and_water_18.png', 0),
(56, 'grass_and_water_19.png', 0),
(57, 'grass_and_water_20.png', 0),
(58, 'grass_and_water_21.png', 0),
(59, 'grass_and_water_22.png', 0),
(60, 'grass_and_water_23.png', 0),
(61, 'water2.png', 4),
(62, 'water3.png', 4),
(63, 'water4.png', 4),
(64, 'coast.png', 4),
(65, 'sand.png', 2),
(66, 'grass2.png', 0),
(67, 'grass1.png', 0),
(68, 'grass2.png', 0),
(69, 'grass3.png', 0),
(70, 'grass4.png', 0),
(71, 'grass5.png', 0),
(72, 'grass6.png', 0),
(73, 'grass7.png', 0),
(74, 'grass8.png', 0),
(75, 'grass9.png', 0),
(76, 'grass10.png', 0),
(77, 'grass11.png', 0),
(78, 'grass12.png', 0),
(79, 'grass13.png', 0),
(80, 'grass14.png', 0),
(81, 'grass15.png', 0),
(82, 'grass16.png', 0),
(83, 'grass17.png', 0),
(84, 'grass18.png', 0),
(85, 'grass19.png', 0),
(86, 'grass20.png', 0),
(87, 'grass21.png', 0),
(88, 'grass22.png', 0),
(89, 'grass23.png', 0),
(90, 'grass24.png', 0),
(91, 'grass25.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_actions`
--

CREATE TABLE IF NOT EXISTS `rrpg_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8 NOT NULL,
  `delay` smallint(1) NOT NULL DEFAULT '0',
  `attribute` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='attribute + value should be uniq' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rrpg_actions`
--

INSERT INTO `rrpg_actions` (`id`, `action`, `delay`, `attribute`, `value`) VALUES
(1, 'dig stone', 6, 0, 0),
(2, 'cut a tree', 6, 0, 0),
(4, 'do stuff', 10, 0, 0),
(5, 'cut a tree tier1', 5, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_action_items`
--

CREATE TABLE IF NOT EXISTS `rrpg_action_items` (
  `action_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `possibility` int(11) NOT NULL,
  `min` smallint(6) NOT NULL DEFAULT '0',
  `max` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`action_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rrpg_action_items`
--

INSERT INTO `rrpg_action_items` (`action_id`, `item_id`, `possibility`, `min`, `max`) VALUES
(4, 2, 10, 0, 1),
(1, 1, 100, 0, 1),
(5, 20, 100, 0, 5),
(2, 20, 75, 1, 2),
(5, 15, 75, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_attributes`
--

CREATE TABLE IF NOT EXISTS `rrpg_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute` varchar(255) NOT NULL,
  `default` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rrpg_attributes`
--

INSERT INTO `rrpg_attributes` (`id`, `attribute`, `default`) VALUES
(1, 'description', 'no description'),
(2, 'name', 'noname'),
(3, 'image', 'None.png'),
(4, 'model', 'rock1.j3o'),
(5, 'woodcuting', '0');

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_items`
--

CREATE TABLE IF NOT EXISTS `rrpg_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `rrpg_items`
--

INSERT INTO `rrpg_items` (`id`, `type`) VALUES
(21, 0),
(2, 3),
(20, 0),
(19, 0),
(18, 0),
(23, 0),
(1, 4),
(22, 0),
(15, 0),
(24, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_item_attributes`
--

CREATE TABLE IF NOT EXISTS `rrpg_item_attributes` (
  `item_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rrpg_item_attributes`
--

INSERT INTO `rrpg_item_attributes` (`item_id`, `attribute_id`, `value`) VALUES
(1, 1, 'Simple stone blah blah'),
(1, 2, 'Stone'),
(1, 3, 'Stone.png'),
(1, 4, 'oziukas/oziukas.j3o'),
(2, 1, 'Stone + stick'),
(2, 2, 'Stone axe'),
(2, 3, 'akmeninis_kirvis_1.png'),
(2, 5, '1'),
(15, 1, 'axe + log'),
(15, 2, 'fire wood'),
(15, 3, 'malkos_1.png'),
(18, 1, 'sharp stone'),
(18, 2, 'sharp stone'),
(18, 3, 'smailus_akmuo_1.png'),
(19, 1, 'Branch from a tree'),
(19, 2, 'Branch'),
(19, 3, 'saka_1.png'),
(20, 1, 'stick'),
(20, 2, 'stick'),
(20, 3, 'pagalys_1.png'),
(21, 1, 'axe + tree'),
(21, 2, 'log'),
(21, 3, 'Rastas_1.png'),
(22, 1, 'fire wood + axe'),
(22, 2, 'something'),
(22, 3, 'smaliekas_1.png'),
(23, 1, 'fire place'),
(23, 2, 'fireplace'),
(23, 3, 'Lauzas_1.png'),
(24, 1, 'stone + stick?'),
(24, 2, 'hammer'),
(24, 3, 'akmeninis_plaktukas_1.png');

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_item_types`
--

CREATE TABLE IF NOT EXISTS `rrpg_item_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `place` tinyint(11) NOT NULL,
  `stackable` tinyint(1) NOT NULL DEFAULT '0',
  `two_handed` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rrpg_item_types`
--

INSERT INTO `rrpg_item_types` (`id`, `type`, `place`, `stackable`, `two_handed`) VALUES
(1, 'Sword', 6, 0, 0),
(2, 'Two handed Sword', 6, 0, 1),
(3, 'Shield', 7, 0, 0),
(4, 'Misc', 0, 1, 0),
(5, 'Usable', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_objects`
--

CREATE TABLE IF NOT EXISTS `rrpg_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `rrpg_objects`
--

INSERT INTO `rrpg_objects` (`id`, `name`, `model`) VALUES
(1, 'test tree', 'tree1/tree.j3o'),
(2, 'test2', 'test2');

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_object_actions`
--

CREATE TABLE IF NOT EXISTS `rrpg_object_actions` (
  `object_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rrpg_object_actions`
--

INSERT INTO `rrpg_object_actions` (`object_id`, `action_id`) VALUES
(1, 2),
(1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_players`
--

CREATE TABLE IF NOT EXISTS `rrpg_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `faction_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `x` int(11) NOT NULL DEFAULT '0',
  `y` int(11) NOT NULL DEFAULT '0',
  `z` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `rrpg_players`
--

INSERT INTO `rrpg_players` (`id`, `user_id`, `faction_id`, `name`, `x`, `y`, `z`) VALUES
(1, 1, 0, 'eraslt', 12, 3, 0),
(2, 1, 0, 'estaslt', 3, 22, 0),
(3, 2, 1, 'test', 0, 0, 0),
(4, 3, 1, 'test1', 0, 0, 0),
(5, 0, 0, 'player', 15, 15, 0),
(6, 0, 0, 'editor', 28, 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_player_inventory`
--

CREATE TABLE IF NOT EXISTS `rrpg_player_inventory` (
  `player_id` int(11) NOT NULL,
  `slot` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cnt` int(11) NOT NULL,
  PRIMARY KEY (`player_id`,`slot`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_player_items`
--

CREATE TABLE IF NOT EXISTS `rrpg_player_items` (
  `player_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `place` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`player_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rrpg_player_items`
--

INSERT INTO `rrpg_player_items` (`player_id`, `item_id`, `quantity`, `place`) VALUES
(1, 15, 2, 1),
(1, 20, 21, 2),
(3, 2, 3, 4),
(3, 1, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_player_skills`
--

CREATE TABLE IF NOT EXISTS `rrpg_player_skills` (
  `player_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `lvl` decimal(4,1) NOT NULL,
  `exp` int(11) NOT NULL,
  PRIMARY KEY (`player_id`,`skill_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rrpg_player_skills`
--

INSERT INTO `rrpg_player_skills` (`player_id`, `skill_id`, `lvl`, `exp`) VALUES
(1, 1, 1.0, 0),
(1, 2, 1.0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_recipes`
--

CREATE TABLE IF NOT EXISTS `rrpg_recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `delay` smallint(1) NOT NULL DEFAULT '0',
  `faction` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='attribute + value should be uniq' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `rrpg_recipes`
--

INSERT INTO `rrpg_recipes` (`id`, `name`, `delay`, `faction`) VALUES
(2, 'test', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_recipe_items`
--

CREATE TABLE IF NOT EXISTS `rrpg_recipe_items` (
  `recipe_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cnt` int(11) NOT NULL,
  PRIMARY KEY (`recipe_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rrpg_recipe_items`
--

INSERT INTO `rrpg_recipe_items` (`recipe_id`, `item_id`, `cnt`) VALUES
(2, 1, 1),
(2, 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_recipe_results`
--

CREATE TABLE IF NOT EXISTS `rrpg_recipe_results` (
  `recipe_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cnt` int(11) NOT NULL,
  PRIMARY KEY (`recipe_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rrpg_recipe_results`
--

INSERT INTO `rrpg_recipe_results` (`recipe_id`, `item_id`, `cnt`) VALUES
(2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_skills`
--

CREATE TABLE IF NOT EXISTS `rrpg_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `rrpg_skills`
--

INSERT INTO `rrpg_skills` (`id`, `skill`, `description`) VALUES
(1, 'Logging', 'cuting trees and etc'),
(2, 'Diging', 'diging some stuff from the ground');

-- --------------------------------------------------------

--
-- Table structure for table `rrpg_users`
--

CREATE TABLE IF NOT EXISTS `rrpg_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rrpg_users`
--

INSERT INTO `rrpg_users` (`id`, `email`, `password`) VALUES
(1, 'eraslt', '2405b6d875212b3461f3c89783c173dc'),
(2, 'test', '098f6bcd4621d373cade4e832627b4f6'),
(3, 'test2', 'ad0234829205b9033196ba818f7a872b');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
