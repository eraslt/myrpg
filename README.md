# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact




### some ideas for you ###

- use instancing

- use a modulo grid roughly centered on the camera (to load/unload instance data from gpu)

- use a combination of manual and procedural positioning and variation

- use fixed seeds with Random for consistency

- use height map and normals for placing procedural objects

- quadtree/octree - look at grid registration (simpler, faster)


once it is working you will need to use imposters
for really large quantities you can use a hierarchy of imposters
(pixel perfect > approximate > aggregate > surround)

a good imposter trick is to render a normal map also - so the imposters
respond to lighting without being rerendered

imposters are very challenging