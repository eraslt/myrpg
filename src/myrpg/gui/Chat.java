package myrpg.gui;

import java.util.concurrent.Callable;

import myrpg.game.states.NetworkState;
import networking.NetworkListener;
import networking.messages.GuiMessages;

import com.jme3.app.Application;
import com.jme3.math.Vector2f;

import tonegod.gui.controls.extras.ChatBox;
import tonegod.gui.core.ElementManager;

public class Chat extends ChatBox implements NetworkListener {

	NetworkState conn;
	Application app;
	
	public Chat(Application app, NetworkState conn, ElementManager screen, Vector2f position) {
		super(screen, position);
		this.app = app;
		this.conn = conn;
		
		screen.addElement(this);
		conn.addListener(this);
		hide();
	}

	@Override
	public void onSendMsg(String arg0) {
		conn.sendUDP(new GuiMessages.ChatMessage(arg0));
	}

	@Override
	public boolean onConnect(int id) {
		return false;
	}

	@Override
	public boolean onDisconnect(int id) {
		return false;
	}

	@Override
	public boolean onMessageReceived(int id,final Object message) {
		if (!(message instanceof GuiMessages.ChatMessage)) return false;
		app.enqueue(new Callable<Void>() {
			public Void call() throws Exception {
				receiveMsg(((GuiMessages.ChatMessage)message).msg);
				return null;
			}
		});
		return false;
	}

}
