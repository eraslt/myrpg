package myrpg.gui;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;

import tonegod.gui.controls.extras.Indicator;
import tonegod.gui.core.Screen;

public class ProgressBar {

	public interface ProgressBarEvent {
		void complete();
		void cancel();
	}
	
	private static ProgressBar instance = null;
	
	private boolean active = false;
	
	private float delay = 0;//in secs
    private float time = 0.0f;
    
	private ProgressBarEvent event = null;
	Indicator indicator;
	
	public static ProgressBar getInstance() {
		return instance;
	}
	
	public ProgressBar(Screen screen) {
		
		instance = this;
		float x = screen.getWidth()/2-150;
		float y = screen.getHeight()/2+100;
		indicator = new Indicator(screen, 
				new Vector2f(x,y), 
				new Vector2f(300,30), 
				Indicator.Orientation.HORIZONTAL) 
		{
			@Override
			public void onChange(float arg0, float arg1) {}
		};
		
		indicator.setBaseImage(screen.getStyle("Window").getString("defaultImg"));
		indicator.setIndicatorColor(ColorRGBA.Green);
		indicator.setAlphaMap(screen.getStyle("Indicator").getString("alphaImg"));
		indicator.setIndicatorPadding(new Vector4f(7,7,7,7));
		indicator.setMaxValue(100);
		indicator.setDisplayPercentage();
		
		screen.addElement(indicator);
		
		indicator.hide();
	}
	
	public boolean startEvent(ProgressBarEvent event, float delay) {
		if (active) return false;
		this.delay = delay;
    	active = true;
		this.event = event;
		indicator.show();
		return true;
	}
	
	private void complete() {
		if (null!=event)
			event.complete();
		event = null;
		active = false;
		indicator.hide();
	}
	
	public void cancel() {
		if (null!=event)
			event.complete();
		event = null;
		active = false;
		indicator.hide();
	}
	
	public void update(float tpf) {
		if (!active) return;
		time += tpf;
		indicator.setCurrentValue((time/delay)*100);
    	if (time>=delay) {
    		complete();
    		time = 0;
    	}
    	
	}
}
