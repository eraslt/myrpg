package myrpg.gui;


import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import myrpg.player.Player;

import tonegod.gui.controls.scrolling.ScrollAreaAdapter;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.windows.Window;
import tonegod.gui.core.Screen;

import com.jme3.math.Vector2f;

import info.mindes.DataProviders.DataProvider;
//http://hub.jmonkeyengine.org/forum/topic/tonegodgui-documentation/
//http://hub.jmonkeyengine.org/wiki/doku.php/jme3:contributions:tonegodgui
public class BuildingWindow {
	
	private Window window;
	private Screen screen;
	
	int last_seen_item_id = 0;
	
	public BuildingWindow(Screen screen) {
		this.screen = screen;
		//window = new Window(screen, new Vector2f( (screen.getWidth()/2)-175, (screen.getHeight()/2)-100 ));
		window = new Window(screen, new Vector2f( 175, 100 ), new Vector2f( 200, 500 ));
		window.setWindowTitle("Building");
		window.setIsResizable(false);
		window.setIsDragDropDropElement(false);
		screen.addElement(window);
		window.hide();
	}
	
	private void show(int building_id) {
		
		if (last_seen_item_id!=building_id) {
			window.removeAllChildren();
			Map<Integer, String> att = DataProvider.item.getAllAttributes();
			Set<Integer> keys = att.keySet();
			int poxY = 0;
			for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
				int key = i.next();
				Label l = new Label(screen, new Vector2f( 10, poxY+=20 ), new Vector2f( 200, 20 ));
				String value = DataProvider.item.getItemAttribute(building_id, key);
	    		l.setText(att.get(key) + ": " + value);
	    		window.addChild(l);
			}
			last_seen_item_id = building_id;
		}
		
		Set<Integer> keys = Player.getInstance().getSkills().keySet();
		ScrollAreaAdapter sa = new ScrollAreaAdapter(screen,"asd",new Vector2f( 0, 0 ));
		int poxY = 10;
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
    		int id = i.next();
    		Label l = new Label(screen, new Vector2f( 10, poxY+=20 ), new Vector2f( 200, 20 ));
    		l.setText("skill_id="+id+" val="+Player.getInstance().getSkills().get(id));
    		sa.addScrollableChild(l);
//    		sa.addChild(l);
		}
		window.addChild(sa);
	}
	
	public void clip() {
		if (window.getIsVisible()) window.hide();
		else window.show();
	}
}
