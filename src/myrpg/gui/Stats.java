package myrpg.gui;

import tonegod.gui.controls.extras.Indicator;
import tonegod.gui.core.Screen;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;


public class Stats {
	
	private Indicator health;
	private Indicator water;
	private Indicator food;
	
	public Stats(Screen screen) {
		
		health = new Indicator(screen, new Vector2f(50,50), new Vector2f(300,30), Indicator.Orientation.HORIZONTAL) {
			
			@Override
			public void onChange(float arg0, float arg1) {}
		};
		
		health.setBaseImage(screen.getStyle("Window").getString("defaultImg"));
		health.setIndicatorColor(ColorRGBA.Red);
		health.setAlphaMap(screen.getStyle("Indicator").getString("alphaImg"));
		health.setIndicatorPadding(new Vector4f(7,7,7,7));
//		health.setDisplayPercentage();
		
		screen.addElement(health);
		
		water = new Indicator(screen, new Vector2f(50,90), new Vector2f(300,30), Indicator.Orientation.HORIZONTAL) {
			
			@Override
			public void onChange(float arg0, float arg1) {}
		};
		
		water.setBaseImage(screen.getStyle("Window").getString("defaultImg"));
		water.setIndicatorColor(ColorRGBA.Blue);
		water.setAlphaMap(screen.getStyle("Indicator").getString("alphaImg"));
		water.setIndicatorPadding(new Vector4f(7,7,7,7));
//		water.setDisplayPercentage();
		
		screen.addElement(water);
		
		food = new Indicator(screen, new Vector2f(50,130), new Vector2f(300,30), Indicator.Orientation.HORIZONTAL) {
			
			@Override
			public void onChange(float arg0, float arg1) {}
		};
		
		food.setBaseImage(screen.getStyle("Window").getString("defaultImg"));
		food.setIndicatorColor(ColorRGBA.Yellow);
		food.setAlphaMap(screen.getStyle("Indicator").getString("alphaImg"));
		food.setIndicatorPadding(new Vector4f(7,7,7,7));
//		food.setDisplayPercentage();
		
		screen.addElement(food);
	}
	
	public void setMaxHealth(int value) {
		health.setMaxValue(value);
	}
	
	public void setHealth(int value) {
		health.setCurrentValue(value);
	}
	
	public void setMaxWater(int value) {
		water.setMaxValue(value);
	}
	
	public void setWater(int value) {
		water.setCurrentValue(value);
	}
	
	public void setMaxFood(int value) {
		food.setMaxValue(value);
	}
	
	public void setFood(int value) {
		food.setCurrentValue((Integer)value);
	}
}
