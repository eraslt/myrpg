package myrpg.gui;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.core.Screen;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;

public class GameMenu {

	Panel gameMenu;
	CraftingWindow crafting;
	Inventory inventory;
	SkillsWindow skills;
	
	public GameMenu(Screen screen) {
		gameMenu = new Panel(screen, new Vector2f(0, 0),new Vector2f(400, 30));
		screen.addElement(gameMenu);
			
		ButtonAdapter showChat = new ButtonAdapter( screen, new Vector2f(0, 0) ) {
			@Override
			public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
				if (null!=crafting)
					crafting.clip();
			}
		};
		showChat.setText("craft");
		gameMenu.addChild(showChat);
		
		ButtonAdapter showInv = new ButtonAdapter( screen, new Vector2f(100, 0) ) {
			@Override
			public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
				if (null!=inventory)
					inventory.clip();
			}
		};
		showInv.setText("inv");
		gameMenu.addChild(showInv);
		
		ButtonAdapter showSkills = new ButtonAdapter( screen, new Vector2f(200, 0) ) {
			@Override
			public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
				if (null!=skills)
					skills.clip();
			}
		};
		showSkills.setText("skl");
		gameMenu.addChild(showSkills);
	}
	
	public void setCrafting(CraftingWindow crafting) {
		this.crafting = crafting;
	}
	
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	
	public void setSkills(SkillsWindow skills) {
		this.skills = skills;
	}
	
}
