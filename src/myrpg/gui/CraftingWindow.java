package myrpg.gui;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import myrpg.player.Player;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.lists.SelectList;
import tonegod.gui.controls.text.LabelElement;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.controls.windows.Window;
import tonegod.gui.core.Element;
import tonegod.gui.core.Screen;
import tonegod.gui.core.Element.Docking;
import tonegod.gui.core.layouts.LayoutHelper;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;

import info.mindes.DataProviders.DataProvider;
import info.mindes.DataProviders.RecipesData.Recipe;


public class CraftingWindow {
	
	public static float contentIndent = 15;
	public static float padding = 5;

	Screen screen;
	private Window window;
	private Panel rightPanel;
	
	private int last_selected_id = 0;
	
	public CraftingWindow(Screen screen) {
		this.screen = screen;
		window = new Window(screen, new Vector2f( 50, 50 ), new Vector2f( 600, 400 ));
		window.setWindowTitle("Crafting");
		window.setIsResizable(false);
		window.setIsDragDropDropElement(true);
		
		ButtonAdapter btnClose = new ButtonAdapter(screen,
				"craft-btnClose",
				Vector2f.ZERO,
				new Vector2f( 20, 20 )
			) {
				@Override
				public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggle) {
					window.hide();
				}
			};
		btnClose.setDocking(Docking.SE);
		btnClose.setText("X");
		window.getDragBar().addChild(btnClose);
		btnClose.centerToParent();
		btnClose.setX(window.getDragBar().getWidth()-btnClose.getY()-btnClose.getWidth()-5);
		
		screen.addElement(window, true);
		
		window.hide();
		
		fill_recipes();
		
		rightPanel = new Panel(screen, "craft-rightPanel", new Vector2f(320, 35), new Vector2f(270, 300), new Vector4f(4, 4, 4, 4), "tonegod/gui/style/def/Button/button_x_u.png");
		window.addChild(rightPanel);
		
		ButtonAdapter btnCraft = new ButtonAdapter(screen,
				"craft-btnCraft",
				new Vector2f( 320, 350 ),
				new Vector2f( 100, 20 )
			) {
				@Override
				public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggle) {
					if (last_selected_id>0)
						Player.getInstance().craft(last_selected_id);
				}
		};
		btnCraft.setText("Craft");
		window.addChild(btnCraft);
	}
	
	public Window getWindow() {
		return window;
	}
	
	public void clip() {
		if (window.getIsVisible()) window.hide();
		else window.show();
	}
	
	
	private void fill_recipes() {
		
		SelectList selectList = new SelectList(screen, new Vector2f(10, 35), new Vector2f(300, 350)) {
			@Override
			public void onChange() {
				last_selected_id = (int) getListItem(getSelectedIndex()).getValue();
				rightPanel.removeAllChildren();
				Map <Integer, Integer> req = DataProvider.recipes.getRequiredItems(last_selected_id);
				Set<Integer> keys = req.keySet();
				int poxY = 0;
				for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
					int key = i.next();
					String img = DataProvider.item.getItemAttribute(key, "image");
					
					rightPanel.addChild(new Element(
								screen,
								"craft_item-"+key, 
								new Vector2f(10,(50+10)*poxY+10),
								new Vector2f(50,50),
								new Vector4f(1, 1, 1, 1),
								"Textures/Items/" + img
							));
					rightPanel.addChild(getLabel(DataProvider.item.getItemAttribute(key, "name"), new Vector2f(70,(50+10)*poxY+10)));
					rightPanel.addChild(getLabel(" x "+req.get(key), new Vector2f(70,(50+10)*poxY+30)));
					poxY++;
				}
			}
		};
		
		Set<Integer> keys  = DataProvider.recipes.getAll().keySet();
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			Recipe recipe = DataProvider.recipes.get(i.next());
			selectList.addListItem(recipe.name, recipe.id);
		}
		
		window.addChild(selectList);
	}
	
	public LabelElement getLabel(String text, Vector2f position) {
		LabelElement te = new LabelElement(screen, position, 
			LayoutHelper.dimensions(200,20)) {
		};
		te.setDocking(Element.Docking.SW);
		te.setSizeToText(true);
		te.setText(text);
		return te;
	}
}
