package myrpg.gui;

/* TODO:
 * clip error on first time
 * hide error on mouse click
 */

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import info.mindes.DataProviders.DataProvider;
import tonegod.gui.controls.extras.DragElement;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.controls.windows.Window;
import tonegod.gui.core.Element;
import tonegod.gui.core.Screen;

import com.jme3.font.BitmapFont.Align;
import com.jme3.font.BitmapFont.VAlign;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;


public class Inventory {
	
	public interface onDragEndEvent {
		void fire (int item_id);
	}
	
	class InventoryItem extends DragElement {
		private int item_id = 0;
		
		public InventoryItem(Screen screen, int item_id, int count, Vector2f pos, Vector2f dimensions, String img) {
			super(screen, "inventory-item-"+item_id, pos, dimensions, Vector4f.ZERO, img);
			setTextAlign(Align.Right);
			setTextVAlign(VAlign.Bottom);
			setFontColor(ColorRGBA.Red);
			setTextPadding(5);
			setUseSpringBack(true);
			setAmmount(count);
			this.item_id = item_id;
		}
		
		private void setAmmount(int count) {
			if (count>1) setText(Integer.toString(count));
			else setText("");
		}
		
		@Override
		public boolean onDragEnd(MouseButtonEvent arg0, Element el) {
			if (null == screen.getDropElement()) {
				if (null != event)
					event.fire(item_id);
				return false;
			}
			if (screen.getDropElement().equals(window)) {
				return true;
			}
			return true;
		}

		@Override
		public void onDragStart(MouseButtonEvent arg0) {
			tooltip.hide();
		}
		
		public void onMouseRightPressed(MouseButtonEvent evt) {
			tooltip.setPosition(evt.getX() - window.getAbsoluteX() + 10, evt.getY() - window.getAbsoluteY() - 10 - tooltip.getHeight());
			showToolTip(item_id);
		}
		public void onMouseRightReleased(MouseButtonEvent evt) {
			tooltip.hide();
		}
	}
	
	private Window window;
	private Panel tooltip;
	private Screen screen;
	
	private int last_seen_item_id = 0;
	
	private onDragEndEvent event = null;
	
	public void setEvent(onDragEndEvent event) {
		this.event = event;
	}
	
	public Inventory(Screen screen) {
		this.screen = screen;
		//window = new Window(screen, new Vector2f( (screen.getWidth()/2)-175, (screen.getHeight()/2)-100 ));
		window = new Window(screen, new Vector2f( 175, 100 ));
		window.setWindowTitle("Inventory");
		window.setIsResizable(false);
		window.setIsDragDropDropElement(true);

		tooltip = new Panel(screen, "inventory_tooltip", new Vector2f( 175, 100 ));
		tooltip.setIgnoreMouse(true);
		window.addChild(tooltip);
		screen.addElement(window);
		
		window.hide();
		tooltip.hide();
	}
	
	public Window getWindow() {
		return window;
	}
	
	private void showToolTip(int item_id) {
		System.out.println("showToolTip:"+item_id);
		if (last_seen_item_id!=item_id) {
			tooltip.removeAllChildren();
			Map<Integer, String> att = DataProvider.item.getAllAttributes();
			Set<Integer> keys = att.keySet();
			int poxY = 0;
			for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
				int key = i.next();
				Label l = new Label(screen, new Vector2f( 10, poxY+=20 ), new Vector2f( 200, 20 ));
				String value = DataProvider.item.getItemAttribute(item_id, key);
	    		l.setText(att.get(key) + ": " + value);
	    		tooltip.addChild(l);
			}
			last_seen_item_id=item_id;
		}
		tooltip.show();
	}
	
	public void clip() {
		if (window.getIsVisible()) window.hide();
		else window.show();
	}
	
	public void removeFromWindow(int item_id) {
		((InventoryItem)screen.getElementById("inventory-item-"+item_id)).removeFromParent();
	}
	
	//TODO: need img sizes and container sizes
	public void updateOnWindow(int item_id, int count) {
		if (null == screen.getElementById("inventory-item-"+item_id)) {
			
			String img = DataProvider.item.getItemAttribute(item_id, "image");
			img = "Textures/Items/" + img;
			
			//Random position in window
			Random r = new Random();
			int x = r.nextInt(Math.round(window.getWidth()));
			int y = r.nextInt(Math.round(window.getHeight()));
			
			window.addChild(new InventoryItem(
						screen,
						item_id,
						count,
						new Vector2f((x>100)?(x-100):x,(y>100)?(y-100):y),
						new Vector2f(100,100),
						img
					));
		} else {
			((InventoryItem)screen.getElementById("inventory-item-"+item_id)).setAmmount(count);
		}
	}

}
