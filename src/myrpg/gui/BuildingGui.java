package myrpg.gui;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import myrpg.buildings.Building;
import myrpg.buildings.BuildingManager;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;

import info.mindes.DataProviders.DataProvider;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.text.LabelElement;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.controls.windows.Window;
import tonegod.gui.core.Element;
import tonegod.gui.core.Screen;
import tonegod.gui.core.Element.Docking;

public class BuildingGui {
	
	Window window;
	Screen screen;
	Panel rightPanel;
	
	private static BuildingGui instance = null;
	
	public static BuildingGui getInstance() {
		return instance;
	}
	
	public BuildingGui(Screen screen) {
		instance = this;
		this.screen = screen; 
		
		window = new Window(screen, new Vector2f( 50, 50 ), new Vector2f( 200, 400 ));
		window.setWindowTitle("Building...");
		window.setIsResizable(false);
		window.setIsDragDropDropElement(true);
		
		ButtonAdapter btnClose = new ButtonAdapter(screen,
				"build-btnClose",
				Vector2f.ZERO,
				new Vector2f( 20, 20 )
			) {
				@Override
				public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggle) {
					window.hide();
				}
			};
		btnClose.setDocking(Docking.SE);
		btnClose.setText("X");
		window.getDragBar().addChild(btnClose);
		btnClose.centerToParent();
		btnClose.setX(window.getDragBar().getWidth()-btnClose.getY()-btnClose.getWidth()-5);
		
		screen.addElement(window, true);
		
		window.hide();
		
		rightPanel = new Panel(screen, "build-rightPanel", new Vector2f(10, 35), new Vector2f(210, 300), new Vector4f(4, 4, 4, 4), "tonegod/gui/style/def/Button/button_x_u.png");
		rightPanel.setIsDragDropDragElement(false);
		window.addChild(rightPanel);
		
		ButtonAdapter btnCraft = new ButtonAdapter(screen,
				"build-btnBuild",
				new Vector2f( 10, 350 ),
				new Vector2f( 100, 20 )
			) {
				@Override
				public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggle) {
					if (selected_building_id>0 && !selected_node_name.isEmpty()) {
						BuildingManager.getInstance().build(
								selected_building_id,
								selected_node_name
						);
						init(selected_building_id, selected_node_name);
					} else
						System.out.println("build-btnDel error, no id");
				}
		};
		btnCraft.setText("Build");
		window.addChild(btnCraft);
		
		ButtonAdapter btnDel = new ButtonAdapter(screen,
				"build-btnDel",
				new Vector2f( 120, 350 ),
				new Vector2f( 100, 20 )
			) {
				@Override
				public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggle) {
					if (selected_building_id>0 && !selected_node_name.isEmpty())
						BuildingManager.getInstance().removeBuilding(
								selected_building_id,
								selected_node_name
						);
					else
						System.out.println("build-btnDel error, no id");
					window.hide();
				}
		};
		btnDel.setText("Remove");
		window.addChild(btnDel);
	}
	
	private int selected_building_id = 0;
	private String selected_node_name;
	public void init(int building_id, String node_name) {
		selected_node_name = node_name;
		selected_building_id = building_id;
		Building b = BuildingManager.getInstance().getBuildingInfo(building_id);
		
		rightPanel.removeAllChildren();
		
		Map <Integer, Integer> req = b.items;
		Set<Integer> keys = req.keySet();
		int poxY = 0;
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			int key = i.next();
			String img = DataProvider.item.getItemAttribute(key, "image");

			rightPanel.addChild(new Element(
						screen,
						"build_item-"+key, 
						new Vector2f(10,(50+10)*poxY+10),
						new Vector2f(50,50),
						new Vector4f(1, 1, 1, 1),
						"Textures/Items/" + img
					));
			rightPanel.addChild(getLabel(DataProvider.item.getItemAttribute(key, "name"), new Vector2f(70,(50+10)*poxY+10)));
			rightPanel.addChild(getLabel(" x "+req.get(key), new Vector2f(70,(50+10)*poxY+30)));
			poxY++;
		}
		window.show();
	}
	
	public LabelElement getLabel(String text, Vector2f position) {
		LabelElement te = new LabelElement(screen, position, new Vector2f(200,20)) {};
		te.setDocking(Element.Docking.SW);
		te.setSizeToText(true);
		te.setText(text);
		return te;
	}
}
