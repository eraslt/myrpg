package myrpg.actions;

import myrpg.gui.ProgressBar;
import myrpg.gui.ProgressBar.ProgressBarEvent;

import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import myrpg.player.Player;
import info.mindes.DataProviders.ObjectData.Action;
import info.mindes.DataProviders.DataProvider;
import info.mindes.DataProviders.ObjectData.Action.ActionItem;

import com.jme3.app.state.AbstractAppState;


public class ActionManager extends AbstractAppState implements ProgressBarEvent {
	
    private static ActionManager instance = null;
    
    public ActionManager() {
    	instance = this;
    }
    
    public static ActionManager getInstance() {
		if(instance == null) {
			instance = new ActionManager();
		}
		return instance;
	}
    
    private Action action;
    public void objectCheck(int object_id) {
    	int item_id = Player.getInstance().getActiveItem();
    	
    	action = null;
    	
    	if (0==item_id) {
    		action = DataProvider.object.getDefaultAction(object_id);
    	} else
    		action = DataProvider.object.getItemAction(object_id, item_id);
    	if (null!=action) {
			ProgressBar.getInstance().startEvent(this, action.delay);
		}
    }
    
    public void complete() {
    	if (null==action) {
    		System.out.println("no action defined error");
    		return;
    	}
    	
    	if (!action.items.isEmpty()) {
    		Random random = new Random();
    		Set<Integer> keys = action.items.keySet();
    		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
        		int id = i.next();
        		ActionItem item = action.items.get(id);
        		if (random.nextInt(100)<item.pos) {
        			int cnt = random.nextInt(item.max) + item.min;
        			System.out.println("action adding item:" + id + " cnt:" + cnt);
        			Player.getInstance().getInventory().addItem(id, cnt);
				}
    		}
    	}
    }
    
    public void cancel() {
    	action = null;
    	System.out.println("action canceled");
    }
    
    @Override
    public void update(float tpf) {
    }
}
