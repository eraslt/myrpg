package myrpg.world;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.Stomrage.grassybanana.GrassArea.GrassArea;
import com.Stomrage.grassybanana.GrassArea.GrassArea.ColorChannel;
import com.Stomrage.grassybanana.GrassArea.GrassArea.DensityMap;
import com.Stomrage.grassybanana.GrassArea.GrassAreaControl;
import com.jme3.asset.AssetManager;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.terrain.geomipmap.TerrainQuad;

//	persiuret situos
//  http://hub.jmonkeyengine.org/forum/topic/shader-based-grass-or-fur/
//  http://hub.jmonkeyengine.org/forum/topic/help-with-forester-plugin-grass/
//  http://hub.jmonkeyengine.org/forum/topic/grass-utility-in-the-sdk/
//  https://l2jserver-client.googlecode.com/svn/trunk/FCClient/src/com/l2client/util/GrassLayerUtil.java
//  http://hub.jmonkeyengine.org/forum/topic/wip-grass-generator/
//  http://hub.jmonkeyengine.org/forum/topic/a-very-simplegrasscontrol/
//  http://hub.jmonkeyengine.org/forum/topic/isosurface-demo-dev-blog-experiment/

//  made by this http://hub.jmonkeyengine.org/t/how-to-grass-reborn-the-forest/33143

public class GrassProvider {

    private static GrassProvider instance = null;
    private GrassArea grassArea;

    private GrassProvider() {
    }

    public static GrassProvider getInstance() {
        if(instance == null) {
            instance = new GrassProvider();
        }
        return instance;
    }

    public void createGrassArea(AssetManager assetManager, Node node, TerrainQuad terrain, Camera cam) {
        try {
            grassArea = new GrassArea(terrain, 8, assetManager, 75);
            grassArea.setColorTexture(assetManager.loadTexture("grass/tile_1.png"));
            grassArea.setDissolveTexture(assetManager.loadTexture("grass/noise.png"));
//            grassArea.addDensityMap(assetManager.loadTexture("grass/noise_large.png"));
            grassArea.addDensityMap(assetManager.loadTexture("Terrain/test2/heiht.png"));
//            grassArea.addDensityMap(assetManager.loadTexture("grass/noise_2.png"));
            grassArea.addDensityMap(assetManager.loadTexture("Terrain/test2/heiht.png"));
            grassArea.addLayer(0f, 0.5f, 2f, ColorChannel.RED_CHANNEL, DensityMap.DENSITY_MAP_1, 2f, 3f);
            grassArea.addLayer(0.5f, 0.5f, 2f, ColorChannel.BLUE_CHANNEL, DensityMap.DENSITY_MAP_2, 2f, 3f);
            grassArea.generate();
        } catch (Exception ex) {
            Logger.getLogger("GrassProvider").log(Level.SEVERE, null, ex);
        }
        GrassAreaControl grassAreaControl = new GrassAreaControl(cam);
        grassArea.addControl(grassAreaControl);
        grassArea.setAutoUpdate(true);
        node.attachChild(grassArea);
    }
}
