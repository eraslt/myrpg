package myrpg.world.sky;

import java.util.Calendar;

import jme3utilities.sky.SkyControl;
import jme3utilities.sky.Updater;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.FastMath;
import com.jme3.post.filters.BloomFilter;

/**
 * 
 * @author eraslt
 *
 *check 
 *  https://code.google.com/p/jme3-utilities/wiki/AddToExistingGame
 *  https://code.google.com/p/jme3-utilities/source/browse/trunk/src/jme3utilities/sky/test/TestSkyControl.java
 *
 */
public class SkyState extends AbstractAppState {
	Application app;

	float hour = 0f;
    SkyControl sc;
    Updater updater;
    DirectionalLight mainLight = new DirectionalLight();
    AmbientLight ambientLight = new AmbientLight();

    public SkyState(Application app) {
        this.app = app;
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }
    
	@Override
    public void update(float tpf) {
		if (simple) return;
	    sc.getSunAndStars().setHour(hour);
        hour+=tpf/2;
        if (hour>24) {
            hour = 0;
        }
    }
	
	@Override
	public void cleanup() {
	}
	
	private boolean simple = true;
	public void initSky() {
        mainLight.setName("main");
//        mainLight.setDirection(new Vector3f(-0.37352666f, -0.50444174f, -0.7784704f));
//        mainLight.setColor(ColorRGBA.White.clone().multLocal(2));
        ((SimpleApplication)app).getRootNode().addLight(mainLight);

        ambientLight.setName("ambient");
        ((SimpleApplication)app).getRootNode().addLight(ambientLight);

        if (simple) return;

	    sc = new SkyControl(app.getAssetManager(), app.getCamera(), 0.9f, true, true);
        ((SimpleApplication)app).getRootNode().addControl(sc);
        sc.getSunAndStars().setHour(6f);
        sc.getSunAndStars().setObserverLatitude(37.4046f * FastMath.DEG_TO_RAD);
        sc.getSunAndStars().setSolarLongitude(Calendar.FEBRUARY, 10);
        sc.setCloudiness(0.8f);
        sc.setEnabled(true);
        
        /*
         * Put SkyControl in charge of updating both lights plus the
         * viewport background. (all optional)
         */
        updater = sc.getUpdater();
        updater.addViewPort(app.getViewPort());
        updater.setAmbientLight(ambientLight);
        updater.setMainLight(mainLight);
       
        //sviesumas
        updater.setAmbientMultiplier(5f);
        updater.setMainMultiplier(5f);
	}

	public DirectionalLight getMainLight() {
		return mainLight;
	}

	public AmbientLight getAmbientLight() {
		return ambientLight;
	}
	
	public void addBloomFilter(BloomFilter bloom) {
	    if (simple) return;
	    sc.getUpdater().addBloomFilter(bloom);
	}
	
	public Updater getUpdater() {
	    return updater;
	}
}
