package myrpg.world;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.HeightfieldCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.geomipmap.lodcalc.DistanceLodCalculator;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.terrain.noise.ShaderUtils;
import com.jme3.terrain.noise.basis.FilteredBasis;
import com.jme3.terrain.noise.filter.IterativeFilter;
import com.jme3.terrain.noise.filter.OptimizedErode;
import com.jme3.terrain.noise.filter.PerturbFilter;
import com.jme3.terrain.noise.filter.SmoothFilter;
import com.jme3.terrain.noise.fractal.FractalSum;
import com.jme3.terrain.noise.modulator.NoiseModulator;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;


public class TerrainProvider  {
	
	private static TerrainProvider instance = null;
	
	private TerrainQuad terrain = null;
	Material mat_terrain;
	
	public TerrainProvider() {
	}
	
	public static TerrainProvider getInstance() {
		if(instance == null) {
			instance = new TerrainProvider();
		}
		return instance;
	}
	
	public TerrainQuad get(Camera cam, AssetManager am, PhysicsSpace ps) {
		if (null!=terrain) return terrain;
		terrain = generateMap(cam, am, ps);
//		terrain = loadMapFromImage(cam, am, ps);
//		terrain = testHm(cam, am, ps);
		return terrain;
	}
	
	
	public TerrainQuad generateMap(Camera cam, AssetManager am, PhysicsSpace ps) {

    	FractalSum base = new FractalSum();
    	base.setRoughness(0.7f);
        base.setFrequency(1.0f);
        base.setAmplitude(1.0f);
        base.setLacunarity(3.12f);
        base.setOctaves(8);
        base.setScale(0.02125f);
        base.addModulator(new NoiseModulator() {

            @Override
            public float value(float... in) {
                return ShaderUtils.clamp(in[0] * 0.5f + 0.5f, 0, 1);
            }
        });

        FilteredBasis ground = new FilteredBasis(base);

        PerturbFilter perturb = new PerturbFilter();
        perturb.setMagnitude(0.519f);

        OptimizedErode therm = new OptimizedErode();
        therm.setRadius(5);
        therm.setTalus(0.511f);

        SmoothFilter smooth = new SmoothFilter();
        smooth.setRadius(1);
        smooth.setEffect(0.7f);

        IterativeFilter iterate = new IterativeFilter();
        iterate.addPreFilter(perturb);
        iterate.addPostFilter(smooth);
        iterate.setFilter(therm);
        iterate.setIterations(1);

        ground.addPreFilter(iterate);
        
        float heights[] = ground.getBuffer(0, 0, 0, 1024).array();
        for (int i =0; i<heights.length; i++) {
        	heights[i] = heights[i]*256;
        }
    	terrain = new TerrainQuad("terrain", 65, 1024+1, heights);
//    	terrain.scale(3, 1, 3);
        TerrainLodControl control = new TerrainLodControl(terrain, cam);
        control.setLodCalculator( new DistanceLodCalculator(65, 2.7f) ); // patch size, and a multiplier
        terrain.addControl(control);
        
        terrain.setMaterial(getHeightBasedTerrainMaterial(am));
        
        terrain.setLocalTranslation(0, 0, 0);
        
        terrain.addControl(new RigidBodyControl(new HeightfieldCollisionShape(terrain.getHeightMap(), terrain.getLocalScale()), 0));
        ps.add(terrain);
        terrain.setShadowMode(ShadowMode.CastAndReceive);
        
        TerrainProvider.getInstance().terrain = terrain;
        
        return terrain;
	}


	public TerrainQuad loadMapFromImage(Camera cam, AssetManager am, PhysicsSpace ps) {
	    AbstractHeightMap heightmap = null;
	    try {
			Texture heightMapImage = am.loadTexture("Terrain/test2/heiht1.png");
	
	        heightmap = new ImageBasedHeightMap(heightMapImage.getImage());
	        heightmap.load();
	        heightmap.smooth(0.9f, 1);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

		terrain = new TerrainQuad("terrain", 65, 1025, heightmap.getScaledHeightMap()/*heightmap.getHeightMap()*/);
		terrain.scale(3, 1, 3);
		System.out.println("size"+terrain.getTerrainSize());
	    TerrainLodControl control = new TerrainLodControl(terrain, cam);
	    control.setLodCalculator( new DistanceLodCalculator(65, 2.7f) ); // patch size, and a multiplier
	    terrain.addControl(control);
	    
//	    terrain.setMaterial(getSimpleMat(am));
	    terrain.setMaterial(getHeightBasedTerrainMaterial(am));
//	    terrain.setMaterial(getTestMaterial(am));
	    
	    terrain.setLocalTranslation(0, 0, 0);
	    
	    terrain.addControl(new RigidBodyControl(new HeightfieldCollisionShape(terrain.getHeightMap(), terrain.getLocalScale()), 0));
	    ps.add(terrain);
	    terrain.setShadowMode(ShadowMode.CastAndReceive);
	    
	    TerrainProvider.getInstance().terrain = terrain;
	    
	    return terrain;
	}
	
	public TerrainQuad testHm(Camera cam, AssetManager am, PhysicsSpace ps) {
        AbstractHeightMap heightmap = null;
        try {
            Texture heightMapImage = am.loadTexture("Terrain/test1/hm.png");
            heightmap = new ImageBasedHeightMap(heightMapImage.getImage());
            heightmap.load();
            heightmap.smooth(0.9f, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        terrain = new TerrainQuad("terrain", 65, 1025, heightmap.getScaledHeightMap()/*heightmap.getHeightMap()*/);
        TerrainLodControl control = new TerrainLodControl(terrain, cam);
        control.setLodCalculator( new DistanceLodCalculator(65, 2.7f) ); // patch size, and a multiplier
        terrain.addControl(control);

        Material matTerrain = new Material(am, "Common/MatDefs/Light/Lighting.j3md");
        Texture tex_ml = am.loadTexture("Terrain/test1/texture.png");
        matTerrain.setTexture("DiffuseMap", tex_ml);
        terrain.setMaterial(matTerrain);

        terrain.setLocalTranslation(0, 0, 0);

        terrain.addControl(new RigidBodyControl(new HeightfieldCollisionShape(terrain.getHeightMap(), terrain.getLocalScale()), 0));
        ps.add(terrain);
        terrain.setShadowMode(ShadowMode.CastAndReceive);

        TerrainProvider.getInstance().terrain = terrain;

        return terrain;
    }

	private Material getSimpleMat(AssetManager am) {
    	Material matTerrain = new Material(am, "Common/MatDefs/Light/Lighting.j3md");
        Texture tex_ml = am.loadTexture("Terrain/test2/texture.png");
        matTerrain.setTexture("DiffuseMap", tex_ml);
        return matTerrain;
    }
    
    public TerrainQuad getTerrain() {
    	return terrain;
    }
    
    public float getHeight(Vector2f xz) {
    	return terrain.getHeight(xz);
        /*Ray ray = new Ray(new Vector3f(xz.x, 300, xz.y), new Vector3f(xz.x, -1, xz.y));
        CollisionResults res = new CollisionResults();
        ray.collideWith(terrain, res);
        
        if(res.size() > 0) {
            return res.getClosestCollision().getContactPoint().y;
        }
        System.out.println("nono");
        
        return 0f;*/
    }
    
    private Material getTestMaterial(AssetManager assetManager) {
        Material matTerrain = new Material(assetManager, "Common/MatDefs/Terrain/TerrainLighting.j3md");
        matTerrain.setBoolean("useTriPlanarMapping", false);
        matTerrain.setFloat("Shininess", 0.0f);
        matTerrain.setTexture("AlphaMap", assetManager.loadTexture("Textures/Terrain/splat/alpha1.png"));
        matTerrain.setTexture("AlphaMap_1", assetManager.loadTexture("Textures/Terrain/splat/alpha2.png"));

        Texture dirt = assetManager.loadTexture("Textures/Grass_1.jpg");
        dirt.setWrap(Texture.WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap", dirt);
        matTerrain.setFloat("DiffuseMap_0_scale", 64);
        Texture darkRock = assetManager.loadTexture("Textures/Grass_1.jpg");
        darkRock.setWrap(Texture.WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_1", darkRock);
        matTerrain.setFloat("DiffuseMap_1_scale", 64);
        Texture pinkRock = assetManager.loadTexture("Textures/Grass_1.jpg");
        pinkRock.setWrap(Texture.WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_2", pinkRock);
        matTerrain.setFloat("DiffuseMap_2_scale", 64);
        Texture riverRock = assetManager.loadTexture("Textures/Grass_1.jpg");
        riverRock.setWrap(Texture.WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_3", riverRock);
        matTerrain.setFloat("DiffuseMap_3_scale", 64);
        Texture grass = assetManager.loadTexture("Textures/Grass_1.jpg");
        grass.setWrap(Texture.WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_4", grass);
        matTerrain.setFloat("DiffuseMap_4_scale", 64);
        Texture brick = assetManager.loadTexture("Textures/Grass_1.jpg");
        brick.setWrap(Texture.WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_5", brick);
        matTerrain.setFloat("DiffuseMap_5_scale", 64);
        Texture road = assetManager.loadTexture("Textures/Grass_1.jpg");
        road.setWrap(Texture.WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_6", road);
        matTerrain.setFloat("DiffuseMap_6_scale", 64);
        return matTerrain;
    }

    private Material getHeightBasedTerrainMaterial(AssetManager am)
    {
        Material terrainMaterial = new Material(am, "Common/MatDefs/Terrain/HeightBasedTerrain.j3md");

        float grassScale = 16;
        float dirtScale = 16;
        float rockScale = 16;

        // GRASS texture
        Texture grass = am.loadTexture("Textures/Terrain/splat/grass.jpg");
        grass.setWrap(WrapMode.Repeat);
        terrainMaterial.setTexture("region1ColorMap", grass);
        terrainMaterial.setVector3("region1", new Vector3f(35, 200, grassScale));

        // DIRT texture
        Texture dirt = am.loadTexture("Textures/Terrain/splat/dirt.jpg");
        dirt.setWrap(WrapMode.Repeat);
        terrainMaterial.setTexture("region2ColorMap", dirt);
        terrainMaterial.setVector3("region2", new Vector3f(0, 40, dirtScale));

        // ROCK texture

        Texture rock = am.loadTexture("Textures/Terrain/Rock/Rock.PNG");
        rock.setWrap(WrapMode.Repeat);
        terrainMaterial.setTexture("region3ColorMap", grass);
        terrainMaterial.setVector3("region3", new Vector3f(198, 260, rockScale));

        terrainMaterial.setTexture("region4ColorMap", rock);
        terrainMaterial.setVector3("region4", new Vector3f(198, 260, rockScale));

        Texture rock2 = am.loadTexture("Textures/Terrain/Rock2/rock.jpg");
        rock2.setWrap(WrapMode.Repeat);

        terrainMaterial.setTexture("slopeColorMap", rock2);
        terrainMaterial.setFloat("slopeTileFactor", 16);

        terrainMaterial.setFloat("terrainSize", 129);

        return terrainMaterial;
    }
}
