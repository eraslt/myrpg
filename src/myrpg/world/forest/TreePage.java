package myrpg.world.forest;

import java.util.Random;

public class TreePage implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    public int tree_count = 0;
    public TreeData[] trees;
    
    public TreePage() {
    	Random rnd = new Random();
    	if (rnd.nextInt(2)==1) { //if this region hass trees
    	    tree_count = rnd.nextInt(ForestGridSettings.DENSITY);
    	}
    	trees = new TreeData[tree_count];
    	for (int i = 0; i < tree_count; i++)
    		trees[i] = new TreeData();
	}
}
