package myrpg.world.forest;

import info.mindes.SimpleGrid.Position;
import info.mindes.SimpleGrid.SimpleGridUtils;

import myrpg.world.TerrainProvider;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Geometry;
import com.jme3.scene.SimpleBatchNode;
import com.jme3.scene.shape.Box;

public class TestWorkerThread implements Runnable {

    final Position position;
    final SimpleGridEvent event;
    final ForestState forestState;

    public TestWorkerThread(ForestState forestState, Position position, SimpleGridEvent event){
        this.position = position;
        this.event = event;
        this.forestState = forestState;
    }

    @Override
    public void run() {
        final SimpleBatchNode b = new SimpleBatchNode();
        float[] pos = SimpleGridUtils.getPosByTile(position, ForestGridSettings.TILE_SIZE, ForestGridSettings.OFFSET_X, ForestGridSettings.OFFSET_Y);

        Geometry geom = new Geometry("Box", new Box(1, 1, 1));
        Material mat = new Material(forestState.app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        geom.setMaterial(mat);

        float x = (int)Math.floor(pos[0])+32;
        float z = (int)Math.floor(pos[1])+32;
        float y = TerrainProvider.getInstance().getHeight(new Vector2f(x, z));
        geom.setLocalTranslation(x, y, z);

        b.attachChild(geom);
        b.batch();

        event.nodes_ready.put(position, b);
    }
}
