package myrpg.world.forest;

import info.mindes.SimpleGrid.Position;
import info.mindes.SimpleGrid.SimpleGrid;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

import myrpg.player.Player;

import tests.OtherGrid;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;

@SuppressWarnings({ "unchecked", "unused" })
public class ForestState extends AbstractAppState {
	Application app;
	SimpleGrid modelGrid, impostorGrid;

	ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(4);
    
	private ConcurrentHashMap<Position, TreePage> trees = new ConcurrentHashMap<>();
	
	SimpleGridEvent impostorGridEvent, modelGridEvent;

    public ForestState(Application app) {
        this.app = app;

//        loadForest();
        Vector3f pos = Player.getInstance().getLastPos();

//        impostorGrid = new SimpleGrid(ForestGridSettings.UNLOAD_DIST);
        impostorGrid = new OtherGrid(ForestGridSettings.UNLOAD_DIST);
        impostorGrid.setGridSize(ForestGridSettings.TILE_SIZE);
        impostorGrid.setOffset(ForestGridSettings.OFFSET_X, ForestGridSettings.OFFSET_Y);
        impostorGridEvent = new SimpleGridEvent(this, ImpostorWorkerThread.class);
        impostorGrid.addEvent(impostorGridEvent);
        impostorGrid.setPosition(pos.x, pos.z);

//        modelGrid = new SimpleGrid(ForestGridSettings.MODEL_LOADED_TILES);
        modelGrid = new OtherGrid(ForestGridSettings.MODEL_LOADED_TILES);
        modelGrid.setGridSize(ForestGridSettings.TILE_SIZE);
        modelGrid.setOffset(ForestGridSettings.OFFSET_X, ForestGridSettings.OFFSET_Y);
        modelGridEvent = new SimpleGridEvent(this, ModelWorkerThread.class);
        modelGrid.addEvent(modelGridEvent);
        modelGrid.setPosition(pos.x, pos.z);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }
    
	@Override
    public void update(float tpf) {
	    impostorGrid.update(app.getCamera().getLocation().getX(), app.getCamera().getLocation().getZ());
		impostorGridEvent.threadSafeUpdate(tpf);
	    
	    modelGrid.update(app.getCamera().getLocation().getX(), app.getCamera().getLocation().getZ());
		modelGridEvent.threadSafeUpdate(tpf);
    }
	
	@Override
	public void cleanup() {
//	    saveForest();
		executor.shutdown();
	}

	private final static String DATA_FILE = System.getProperty("user.home") + "/" + System.getProperty("AppName") + "/trees.data";
	public void saveForest() {
	    try (
	            OutputStream file = new FileOutputStream(DATA_FILE);
	            OutputStream buffer = new BufferedOutputStream(file);
	            ObjectOutput output = new ObjectOutputStream(buffer);
            ){
	            output.writeObject(trees);
	    }
	    catch(IOException ex){
	        Logger.getLogger(ForestState.class.getName()).log(Level.SEVERE, "Cannot perform output.", ex);
	    }
    }

    private void loadForest() {
        try(
                InputStream file = new FileInputStream(DATA_FILE);
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input = new ObjectInputStream (buffer);
            ){
            trees = (ConcurrentHashMap<Position, TreePage>) input.readObject();
        } catch(ClassNotFoundException ex) {
            Logger.getLogger(ForestState.class.getName()).log(Level.SEVERE, "Cannot perform input. Class not found.", ex);
        } catch(IOException ex) {
            Logger.getLogger(ForestState.class.getName()).log(Level.SEVERE, "Cannot perform input.", ex);
        }
    }

    public synchronized TreePage getTreeData(Position position) {
        if (!trees.containsKey(position)) {
            trees.put(position, new TreePage());
        }
        return trees.get(position);
    }
}
