package myrpg.world.forest;

/*
 * tree model
 *   blur from 2.5*grid to 3*grid
 *   remove on 3 grid
 *  
 * tree impostor
 *   blur in from 2*grid to 2.5*grid
 *   remove on grid 5
 *   
 * tree sprite
 *   remove on grid 4(not remove just cull that grid)
 *   
*/
public class ForestGridSettings {

	public static int GRID_SIZE = 1024;
	public static int TILE_SIZE = 64;
	public static int DENSITY = 20;
	public static int UNLOAD_DIST = 8;//GRID_SIZE/2;
	public static int OFFSET_X = 0;//GRID_SIZE/2;
	public static int OFFSET_Y = 0;//GRID_SIZE/2;

	public static int SPRITE_FADE_RANGE = 4;
	
	public static int MODEL_LOADED_TILES = 2;
	
	/** 
	 * some descr
	 */
	public static int IMPOSTOR_FADE_RANGE = 10;

    /**
     *  fefe
     */
    public static int IMPOSTOR_FADE_END = TILE_SIZE*MODEL_LOADED_TILES-IMPOSTOR_FADE_RANGE;

    /**
     *  lvl(tiles from 0) to load impostor
     */
	public static int IMPOSTOR_LOAD_RANGE = SPRITE_FADE_RANGE+1;

	/**
     *  fefe
     */
	public static int IMPOSTOR_FADEOUT_RANGE = TILE_SIZE/2;//how much it takes completely fade-out
	public static int IMPOSTOR_FADEOUT_END = TILE_SIZE*SPRITE_FADE_RANGE+IMPOSTOR_FADEOUT_RANGE;//where to start

	/**
     *  range which takes tree to fade
     */
	public static int TREE_FADE_RANGE = TILE_SIZE;

	/**
     *  range where tree is faded
     */
    public static int TREE_FADE_END = 10;

}
