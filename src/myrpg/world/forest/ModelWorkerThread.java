package myrpg.world.forest;

import tests.SimpleTree;
import info.mindes.SimpleGrid.Position;
import info.mindes.SimpleGrid.SimpleGridUtils;

import myrpg.world.TerrainProvider;

import com.jme3.math.Vector2f;
import com.jme3.scene.BatchNode;
import com.jme3.scene.Spatial;

public class ModelWorkerThread implements Runnable {

    final Position position;
    final SimpleGridEvent event;
    final ForestState forestState;

    public ModelWorkerThread(ForestState forestState, Position position, SimpleGridEvent event){
        this.position = position;
        this.event = event;
        this.forestState = forestState;
    }

    @Override
    public void run() {
        final TreePage p = forestState.getTreeData(position);
        final BatchNode b = new BatchNode();
        float[] pos = SimpleGridUtils.getPosByTile(position, ForestGridSettings.TILE_SIZE, ForestGridSettings.OFFSET_X, ForestGridSettings.OFFSET_Y);

        TreeData t;
        Spatial tree;
        
        tree = new SimpleTree(forestState.app.getAssetManager());
        float x = (int)Math.floor(pos[0]);
        float z = (int)Math.floor(pos[1]);
        float y = TerrainProvider.getInstance().getHeight(new Vector2f(x, z));
        tree.setLocalTranslation(x, y, z);
        b.attachChild(tree);

/*        for (int i=0; i<p.tree_count; i++) {
            t = p.trees[i];

//            tree = forestState.app.getAssetManager().loadModel("Models/lowpoly/tree"+t.type+".j3o");
//            tree.scale((15+t.scale)/10f*4);
            tree = new SimpleTree(forestState.app.getAssetManager());
            
            float x = t.x+(int)Math.floor(pos[0]);
            float z = t.y+(int)Math.floor(pos[1]);
            float y = TerrainProvider.getInstance().getHeight(new Vector2f(x, z));

            tree.setLocalTranslation(x, y, z);
            b.attachChild(tree);
        }*/

//        b.batch();//doesnt work on models somehow
        event.nodes_ready.put(position, b);
    }
}
