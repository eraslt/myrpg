package myrpg.world.forest;

import java.util.Random;

/*
 * TODO hold all data
 * size for different types: impostor, sprite, model(also offsets)
 * all 3 coords, better generated and loaded from disk
 */
public class TreeData implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    int x, y, rotation, scale, type;
	
	public TreeData() {
		Random rnd = new Random();
		this.scale = rnd.nextInt(10)-5;
		this.type = rnd.nextInt(5)+1;//5 kad nebutu no 0
		this.rotation = rnd.nextInt(180);
		this.x = rnd.nextInt(ForestGridSettings.TILE_SIZE);
		this.y = rnd.nextInt(ForestGridSettings.TILE_SIZE);
	}
	
	public TreeData(int x, int y, int r, int s, int t) {
		this.x = x;
		this.y = y;
		this.rotation = r;
		this.scale = s;
		this.type = t;
	}
}
