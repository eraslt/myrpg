package myrpg.world.forest;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.app.SimpleApplication;
import com.jme3.scene.Node;

import info.mindes.SimpleGrid.*;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class SimpleGridEvent implements EventHandler {

	HashMap<Position, Node> nodes = new HashMap<>();
	ConcurrentHashMap<Position, Node> nodes_ready = new ConcurrentHashMap<>();

	Class<Runnable> worker;
	
    ForestState forestState;

    public SimpleGridEvent(ForestState forestState, Class worker) {
    	this.forestState = forestState;
    	this.worker = worker;
    }

    @Override
    public void load(Position position) {
        if (!nodes.containsKey(position)) {
            System.out.println("load:"+position);
            try {
                forestState.executor.execute(worker.getConstructor(ForestState.class, Position.class, SimpleGridEvent.class).newInstance(forestState, position, this));
            } catch (InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException
                    | NoSuchMethodException | SecurityException e) {
                Logger.getLogger("GrassProvider").log(Level.SEVERE, null, e);
            }
        }
    }

    @Override
    public void update(Position position) {
        System.out.println("update:"+position);
    }

    @Override
    public void unload(Position position) {
        if (nodes.containsKey(position)) {
            System.out.println("unload:"+position);
            nodes.get(position).detachAllChildren();
            nodes.get(position).removeFromParent();
            nodes.remove(position);
        }
    }

    public void threadSafeUpdate(float tpf) {
        if (!nodes_ready.isEmpty()) {
            Set<Position> keys = nodes_ready.keySet();
            for (Iterator<Position> i = keys.iterator(); i.hasNext();) {
                Position position = i.next();
                if (null!=nodes.get(position)) {
                    nodes.get(position).detachAllChildren();
                    nodes.remove(position);
                }
                Node n = (Node) nodes_ready.get(position);
                nodes.put(position, n);
                ((SimpleApplication)forestState.app).getRootNode().attachChild(n);
                nodes_ready.remove(position);
            }
        }
    }
}
