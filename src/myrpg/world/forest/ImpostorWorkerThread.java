package myrpg.world.forest;

import info.mindes.SimpleGrid.Position;
import info.mindes.SimpleGrid.SimpleGridUtils;

import myrpg.core.Impostor;
import myrpg.world.TerrainProvider;

import com.jme3.material.Material;
import com.jme3.math.Vector2f;
import com.jme3.scene.Geometry;
import com.jme3.scene.SimpleBatchNode;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;

public class ImpostorWorkerThread implements Runnable {

    final Position position;
    final SimpleGridEvent event;
    final ForestState forestState;

    public ImpostorWorkerThread(ForestState forestState, Position position, SimpleGridEvent event){
        this.position = position;
        this.event = event;
        this.forestState = forestState;
    }

    @Override
    public void run() {
        final TreePage p = forestState.getTreeData(position);
        final SimpleBatchNode b = new SimpleBatchNode();
        float[] pos = SimpleGridUtils.getPosByTile(position, ForestGridSettings.TILE_SIZE, ForestGridSettings.OFFSET_X, ForestGridSettings.OFFSET_Y);

        TreeData t;
        Spatial tree;

        Texture tex = forestState.app.getAssetManager().loadTexture("biomonkey/textures/noise.png");
        
        for (int i=0; i<p.tree_count; i++) {
            t = p.trees[i];

            Material impostorMaterial = new Material(forestState.app.getAssetManager(), "biomonkey/matdefs/TreeImpostorBase.j3md");
            impostorMaterial.setTexture("ImpostorTexture", forestState.app.getAssetManager().loadTexture("Models/lowpoly/tree"+t.type+".png"));
            impostorMaterial.setTexture("AlphaNoiseMap", tex);
            impostorMaterial.setFloat("FadeEnd", ForestGridSettings.IMPOSTOR_FADE_END);
            impostorMaterial.setFloat("FadeRange", ForestGridSettings.IMPOSTOR_FADE_RANGE);

            tree = new Geometry("batchNode-"+position.hashCode()+"-tree-"+i, new Impostor((15+t.scale)/10f*1.9f*4));
            tree.setMaterial(impostorMaterial);

            float x = t.x+(int)Math.floor(pos[0]+32);
            float z = t.y+(int)Math.floor(pos[1]+32);//pos fix OK
            float y = TerrainProvider.getInstance().getHeight(new Vector2f(x, z));

            tree.setLocalTranslation(x, y, z);
            b.attachChild(tree);
        }

        b.batch();
        event.nodes_ready.put(position, b);
    }
}
