package myrpg.game;

import java.io.File;

import myrpg.gui.BuildingGui;

import myrpg.gui.CraftingWindow;
import myrpg.gui.GameMenu;
import myrpg.gui.Inventory;
import myrpg.gui.ProgressBar;
import myrpg.gui.SkillsWindow;
import myrpg.gui.Stats;


import myrpg.core.Controls;

import tonegod.gui.core.Screen;
import myrpg.world.GrassProvider;
import myrpg.world.TerrainProvider;
import myrpg.world.forest.ForestState;
import myrpg.world.sky.SkyState;

import myrpg.player.Player;

import myrpg.actions.ActionManager;
import myrpg.buildings.BuildingManager;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.post.filters.DepthOfFieldFilter;
import com.jme3.post.filters.FXAAFilter;
import com.jme3.post.filters.FogFilter;
import com.jme3.post.filters.LightScatteringFilter;
import com.jme3.post.ssao.SSAOFilter;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.shadow.DirectionalLightShadowFilter;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.texture.Texture2D;
import com.jme3.water.WaterFilter;

import jme3utilities.Misc;


/*
 * sitas lokaliai be servo 
 */

public class GameTest3 extends SimpleApplication {
	
	private static GameTest3 instance = null;

	public BulletAppState bulletAppState;

	public Node collidable;
	
	public static void main(String[] args) {
		
		System.setProperty("AppName", "RRPG");
		
		GameTest3 app = new GameTest3();
		app.start();
	}
	
	public GameTest3() {
		super();
		instance = this;
	}
	
	public static GameTest3 getRoot() {
		return instance;
	}
	
	@Override
	public void simpleInitApp() {
		setPauseOnLostFocus(false);
		assetManager.registerLocator("assets/", FileLocator.class);
		flyCam.setMoveSpeed(100);
		
		bulletAppState = new BulletAppState();
		stateManager.attach(bulletAppState);
		bulletAppState.setDebugEnabled(true); 
		
		Player.getInstance().load();

		// prepare folders
		File file = new File(System.getProperty("user.home") + "/" + System.getProperty("AppName"));
		file.mkdirs();

		//TODO check priority
		initMenu();
		initWorld();
		initControls();

//		stateManager.attach(new VideoRecorderAppState());
	}

	@Override
	public void simpleUpdate(float tpf) {
		if (controls.playing) {
			controls.update();
			cam.setLocation(controls.getLocation());
			Player.getInstance().update(tpf, controls.getLocation());
			progressBar.update(tpf);
		}
	}

	 /**
     * width and height of rendered shadow maps (pixels per side, &gt;0)
     */
    final private static int shadowMapSize = 4_096;
    /**
     * number of shadow map splits (&gt;0)
     */
    final private static int shadowMapSplits = 3;
    
    private boolean use_WaterFilter = false;
    private boolean use_DirectionalLightShadowFilter = false;
    private boolean use_DirectionalLightShadowRenderer = false;
    private boolean use_FogFilter = false;
    private boolean use_SSAOFilter = false;
    private boolean use_FXAAFilter = false;
    private boolean use_BloomFilter = false;
    private boolean use_LightScatteringFilter = false;
    private boolean use_DepthOfFieldFilter = false;
    
    SkyState skyState;
	private void initWorld() {
        rootNode.attachChild(
        		new TerrainProvider().get(
        				cam, 
        				assetManager, 
        				bulletAppState.getPhysicsSpace()
        			)
        		);
        
        stateManager.attach(ActionManager.getInstance());
        
        stateManager.attach(new ForestState(this));
        
        stateManager.attach(new BuildingManager(this));
        BuildingManager.getInstance().setup();

//        GrassProvider.getInstance().createGrassArea(assetManager, rootNode, TerrainProvider.getInstance().getTerrain(), cam);

        skyState = new SkyState(this);
        stateManager.attach(skyState);
        skyState.initSky();

        // TODO: move to separate class
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);

        if (use_DirectionalLightShadowFilter) {
        	DirectionalLightShadowFilter shadows = new DirectionalLightShadowFilter(assetManager, shadowMapSize, shadowMapSplits);
	        shadows.setShadowIntensity(0.3f);
	        shadows.setLight(skyState.getMainLight());
	        fpp.addFilter(shadows);
	        Misc.getFpp(viewPort, assetManager).addFilter(shadows);
	        skyState.getUpdater().addShadowFilter(shadows);
	        skyState.getUpdater().setShadowFiltersEnabled(true);
        } else if (use_DirectionalLightShadowRenderer) {
        	DirectionalLightShadowRenderer shadows_rendered = new DirectionalLightShadowRenderer(assetManager, shadowMapSize, shadowMapSplits);
        	shadows_rendered.setEdgeFilteringMode(EdgeFilteringMode.PCF8);
        	shadows_rendered.setLight(skyState.getMainLight());
        	skyState.getUpdater().addShadowRenderer(shadows_rendered);
            viewPort.addProcessor(shadows_rendered);
            skyState.getUpdater().setShadowFiltersEnabled(true);
        }
        
        if (use_FogFilter) {
	        FogFilter fog = new FogFilter();
	        fog.setFogColor(new ColorRGBA(0.9f, 0.9f, 0.9f, 1.0f));
	        fog.setFogDistance(200);
	        fog.setFogDensity(0.5f);
	        fpp.addFilter(fog);
        }
        
        if (use_SSAOFilter) {
        	SSAOFilter ssao = new SSAOFilter();
	        fpp.addFilter(ssao);
	
	        ssao.setIntensity(0.01f);
	        ssao.setBias(0.01f);
	        ssao.setSampleRadius(1);
	        ssao.setScale(0.01f);
        }
        
        if (use_WaterFilter) {
        	WaterFilter water = new WaterFilter(rootNode, skyState.getMainLight().getDirection());
	        water.setFoamIntensity(0.1f);
	        water.setWaterHeight(100);
	        water.setFoamTexture( (Texture2D)assetManager.loadTexture("Common/MatDefs/Water/Textures/foam2.jpg"));
	        fpp.addFilter(water);
        }
        
        if (use_LightScatteringFilter) {
        	LightScatteringFilter lightScattering = new LightScatteringFilter();
            lightScattering.setLightDensity(1);
            lightScattering.setBlurWidth(1.1f);
            fpp.addFilter(lightScattering);
        }
        
        if (use_FXAAFilter) {
        	FXAAFilter fxaa = new FXAAFilter();
//            fxaa.setReduceMul(0.01f);
//			fxaa.setSpanMax(0.01f);
//			fxaa.setSubPixelShift(0.01f);
//			fxaa.setVxOffset(0.01f);
            fpp.addFilter(fxaa);
        }
        
        if (use_BloomFilter) {
        	BloomFilter bloom = new BloomFilter(BloomFilter.GlowMode.Objects);
	        bloom.setBlurScale(1.5f);
	        bloom.setExposurePower(1f);//55
//	        bloom.setBloomIntensity(0.5f);
	        fpp.addFilter(bloom);
	        Misc.getFpp(viewPort, assetManager).addFilter(bloom);
	        skyState.addBloomFilter(bloom);
        }
        
        if (use_DepthOfFieldFilter) {
        	DepthOfFieldFilter dof = new DepthOfFieldFilter();
	        dof.setFocusDistance(0);//1-100
	        dof.setBlurScale(0.8f);//0-1
	        dof.setFocusRange(200);//0-600
	        fpp.addFilter(dof);
        }

        viewPort.addProcessor(fpp);
        
        
        Spatial center = assetManager.loadModel("Models/lowpoly/tree1.j3o");
        center.scale(15);
        center.setLocalTranslation(0, 0, 0);//y = 98 jei kitas
        rootNode.attachChild(center);
    }
	
	Controls controls;
	private void initControls() {
    	controls = new Controls(this);
    	Vector3f pos = Player.getInstance().getLastPos();
    	pos.y = TerrainProvider.getInstance().getHeight(new Vector2f(pos.x, pos.z))+10;
    	controls.initCharControl(pos);
    	controls.enableControls();
    	initCrossHairs();
    }
	
	Screen screen;
	GameMenu gameMenu;
	Inventory inventory;
	CraftingWindow crafting;
	ProgressBar progressBar;
	BuildingGui buildingGui;
	private void initMenu() {
		screen = new Screen(this/*, "Interface/GUI/style_map.gui.xml"*/);
		guiNode.addControl(screen);
		crafting =  new CraftingWindow(screen);
		inventory = new Inventory(screen);
		progressBar = new ProgressBar(screen);
		buildingGui = new BuildingGui(screen);
		
	    gameMenu = new GameMenu(screen);
	    gameMenu.setInventory(inventory);
	    gameMenu.setCrafting(crafting);
	    gameMenu.setSkills(new SkillsWindow(screen));
	    
	    Player.getInstance().load();
	    Player.getInstance().getInventory().setGui(inventory);
		Player.getInstance().setStats(new Stats(screen));
	}
	
	@Override
	public void stop() {
		Player.getInstance().save();
//		BuildingManager.getInstance().saveBuildings();
		super.stop(); // continue quitting the game
	}
	
	protected void initCrossHairs() {
	    guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
	    BitmapText ch = new BitmapText(guiFont, false);
	    ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
	    ch.setText("+"); // crosshairs
	    ch.setLocalTranslation( // center
	      settings.getWidth() / 2 - ch.getLineWidth()/2, settings.getHeight() / 2 + ch.getLineHeight()/2, 0);
	    guiNode.attachChild(ch);
	  }
}