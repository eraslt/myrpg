package game.states;

import java.util.ArrayList;

import networking.NetworkListener;
import networking.kryonet.KryoNetworkClient;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;

@SuppressWarnings("rawtypes")
public class NetworkState extends AbstractAppState {
	
	private final String host = "localhost";
	private final int tcp_port = 1114;
	private final int udp_port = 1115;
	private final static ArrayList<Class> messages = new ArrayList<>();
	
	KryoNetworkClient networkClient;
 
	public NetworkState() {
		networkClient = new KryoNetworkClient();
	}
	
	public void connect() {
		networkClient.registerMessages(messages);
		networkClient.connect(host, tcp_port, udp_port);
	}
	
	public void sendUDP(Object udpMsg) {
        networkClient.sendMessage(udpMsg, false);
    }

    public void sendTCP(Object tcpMsg) {
        networkClient.sendMessage(tcpMsg, true);
    }
	
	public void registerMessage(Class newMessage) {
		messages.add(newMessage);
	}
	
	public void registerMessages(ArrayList<Class> newMessages) {
		messages.addAll(newMessages);
	}
	
	public void addListener(NetworkListener newListener) {
		networkClient.addListener(newListener);
	}
	
	public NetworkListener getListener(Class listener) {
		return networkClient.getListener(listener);
	}

	public void removeListener(Class listener) {
		networkClient.removeListener(listener);
	}
	
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }
}
