package myrpg.game.states;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import networking.Identificator;
import networking.NetworkListener;
import networking.client.NetworkControlled;
import networking.messages.EntityMessages.*;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

public class EntitiesState extends AbstractAppState implements NetworkListener {
	Application app;
	
    public EntitiesState(Application app) {
        this.app = app;
    }
 
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }
    
	@Override
    public void update(float tpf) {
    }

	@Override
	public boolean onConnect(int id) {
		return false;
	}

	@Override
	public boolean onDisconnect(int id) {
		return false;
	}

	@Override
	public boolean onMessageReceived(int id, Object message) {
		if (message instanceof MoveEntityMessage) {
			processEntities((MoveEntityMessage)message);
		}
		if (message instanceof removeEntityMessage) {
			int entity = ((removeEntityMessage)message).id;
			if (entities.containsKey(entity)) {
				entities.get(entity).remove();
				entities.remove(entity);
			}
		}
		return false;
	}
	
	public Map<Integer, NetworkControlled> entities = new ConcurrentHashMap<>();
	private void processEntities(final MoveEntityMessage m) {
		app.enqueue(new Callable<Void>() {
			public Void call() throws Exception {
				Set<Integer> keys = m.entities.keySet();
				for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
					int id = i.next();
					if (id==Identificator.connection_id) continue;
					if (entities.get(id)==null) {
						entities.put(id, makeBox());
					}
					EntityState p = m.entities.get(id);
					entities.get(id).addState(p.pos, p.rot);
                }
                return null;
            }
        });
	}
	
	private NetworkControlled makeBox() {
        Geometry geom = new Geometry("Box", new Box(1, 1, 1));
        Material mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.randomColor());
        geom.setMaterial(mat);
        geom.setLocalTranslation(0, 0, 0);
        NetworkControlled control = new NetworkControlled();
        ((SimpleApplication)app).getRootNode().attachChild(geom);
        geom.addControl(control);
        return control;
	}
}
