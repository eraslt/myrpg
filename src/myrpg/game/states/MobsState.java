package game.states;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import networking.NetworkListener;
import networking.messages.CommonMessages.MobTest;
import networking.messages.CommonMessages.NPCData;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Sphere;

@SuppressWarnings("unused")
public class MobsState extends AbstractAppState implements NetworkListener {
	
	public static class MyMob {
		public Geometry  geom;
		public float time = 0f;
		public float speed = 1f;
		public int action = 0;
		public Vector3f position = Vector3f.ZERO;
		public Vector3f target = Vector3f.ZERO;
		public Vector3f new_target = Vector3f.ZERO;
		public NPCData data;
		
		public int anim = 0;
	}
	
	public Map<Integer, MyMob> myMobs = new ConcurrentHashMap<>();
	
	Application app;
	
    public MobsState(Application app) {
        this.app = app;
    }
 
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }
    
	@Override
    public void update(float tpf) {
		Set<Integer> keys = myMobs.keySet();
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			int id = i.next();
			MyMob b = myMobs.get(id);
			if (null == b.geom) {
				b.geom = makeMob();
			}
			b.geom.setLocalTranslation(interpolate3(tpf, b));
//			myMobs.put(id,b);
		}
    }

	@Override
	public boolean onConnect(int id) {
		return false;
	}

	@Override
	public boolean onDisconnect(int id) {
		return false;
	}

	@Override
	public boolean onMessageReceived(int id, Object message) {
		if (message instanceof MobTest) {
			processMobs((MobTest)message);
		}
		return false;
	}
	
	private void processMobs(final MobTest m) {
		app.enqueue(new Callable<Void>() {
			public Void call() throws Exception {
				Set<Integer> keys = m.data.keySet();
				for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
					int id = i.next();
					NPCData data = m.data.get(id);
					if (!myMobs.containsKey(id)) {
						MyMob mob = new MyMob();
//						mob.position = data.position;
//						mob.target = data.target;
//						mob.action = data.action;
						myMobs.put(id, mob);
					}
//					} else {
						myMobs.get(id).data = m.data.get(id);
//					}
                }
                return null;
            }
        });
	}
	
	private Vector3f interpolate(float tpf, MyMob m) {
		if (m.new_target!=null) {
			m.position = m.target;
			m.time = 0f;
			m.target = m.new_target;
			m.new_target = null;
		}
		if (m.time>=1) {
			m.anim = 0;
			return m.position;
		}
		
		m.time += 0.01;
		System.out.println(m.time );
		m.position = FastMath.interpolateLinear(
                m.time,
                m.position,
                m.target);
		return m.position;
	}
	
	private Vector3f interpolate2(float tpf, MyMob m) {
		/*Vector3f lpos = m.position;
		Vector3f spos = m.server_pos;

		if (lpos.subtract(spos).length()>3) {
			m.position = m.server_pos;
		}*/
			
		if (m.new_target!=null) {
			m.position = m.target;
			m.target = m.new_target;
			m.new_target = null;
		}
		
		Vector3f loc = m.position;
		Vector3f targ = m.target;

		Vector3f dir = targ.subtract(loc);
		float length = dir.length();
		if (length<=0.01) {
			m.anim = 0;
			return m.position;
		}
		Vector3f scaledDir = dir.mult((m.speed/length)*tpf);
		m.position = loc.add(scaledDir);
		return m.position;
	}
	
	private Vector3f interpolate3(float tpf, MyMob m) {
		
		if (m.data!=null) {
//			System.out.println("cur pos:"+m.data.position.toString());
//			System.out.println("cur targ:"+m.data.target.toString());
//			System.out.println("cur speed:"+m.data.speed);
//			System.out.println("pos:"+m.data.position.toString());
			//if (m.position==null) {
				m.position = m.data.position;
			//}
			//if (m.target==null) {
				m.target = m.data.target;
			//}
			m.action = m.data.action;
			m.speed = m.data.speed;
			m.data = null;
		}
		
		if (m.target==null) {
			if (m.position==null) {
				return Vector3f.ZERO;
			} else {
				return m.position;
			}
		}
		
		Vector3f loc = m.position;
		Vector3f targ = m.target;
		loc.y = 0;
		targ.y = 0;
//		System.out.println("loc:"+loc.toString());
//		System.out.println("targ:"+targ.toString());

		Vector3f dir = targ.subtract(loc);
		float length = dir.length();
//		System.out.println("length:"+length);
		if (length<=0.01) {
			m.anim = 0;
			m.target = null;
			return m.position;
		}
		dir = dir.normalize();
		Vector3f scaledDir = dir.mult((m.speed)*tpf);
//		Vector3f scaledDir = dir.mult((m.speed/length)*tpf);
//		System.out.println("scaledDir:"+scaledDir.toString());
		m.position = loc.add(scaledDir);
//		System.out.println("ret:"+m.position.toString());
		
		return m.position;
	}
	
	private Geometry makeMob() {
        Geometry geom = new Geometry("MyMob", new Sphere(32, 32, 1f, true, false));
        Material mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.randomColor());
        geom.setMaterial(mat);
        geom.setLocalTranslation(0, 0, 0);
        ((SimpleApplication)app).getRootNode().attachChild(geom);
        return geom;
	}
}
