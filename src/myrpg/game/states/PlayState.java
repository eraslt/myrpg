package game.states;

import networking.messages.EntityMessages.*;
import gui.Chat;
import gui.GameMenu;
import gui.Inventory;
import system.Controls;
import tonegod.gui.core.Screen;
import world.TerrainProvider;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

public class PlayState extends AbstractAppState {
	Application app;
    Screen screen;
    Controls controls;
    Inventory inventory;
    Chat chat;
    GameMenu gameMenu;
    
    public PlayState(Application app, Screen screen) {
        this.app = app;
        this.screen = screen;
    }
 
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        initControls();
        initMenu();
        initWorld();
        EntitiesState entitiesState = new EntitiesState(app);
        app.getStateManager().attach(entitiesState);
        app.getStateManager().getState(NetworkState.class).addListener(entitiesState);
        MobsState mobsState = new MobsState(app);
        app.getStateManager().attach(mobsState);
        app.getStateManager().getState(NetworkState.class).addListener(mobsState);
    }
    
    private void initControls() {
    	controls = new Controls(app);
    	controls.initCharControl();
    	controls.enableControls();
    }
    
    private void initMenu() {
    	inventory = new Inventory(screen);
    	chat = new Chat(app, app.getStateManager().getState(NetworkState.class), screen, new Vector2f(0,0));
    	gameMenu = new GameMenu(screen);
    	gameMenu.setChat(chat);
    	gameMenu.setInventory(inventory);
    }
    
    private void initWorld() {
    	DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.37352666f, -0.50444174f, -0.7784704f));
        sun.setColor(ColorRGBA.White.clone().multLocal(2));
        ((SimpleApplication)app).getRootNode().addLight(sun);
        
        ((SimpleApplication)app).getRootNode().attachChild(
        		new TerrainProvider().generateMap(
        				app.getCamera(), 
        				app.getAssetManager(), 
        				app.getStateManager().getState(BulletAppState.class).getPhysicsSpace()
        			)
        		);
    }
    
    private float syncFreq = 1/25; // sync 25 times per second
    private float syncTime = 0.0f;
    
	@Override
    public void update(float tpf) {
		if (controls.playing) {
			controls.update();
			app.getCamera().setLocation(controls.getLocation());
		}
		if(syncTime >= syncFreq) {
			app.getStateManager().getState(NetworkState.class).sendUDP(
					new UpdateEntityMessage(
							new EntityState(app.getCamera().getLocation(), app.getCamera().getRotation())
						)
					);
        	syncTime = 0.0f;
        }
    }
}
