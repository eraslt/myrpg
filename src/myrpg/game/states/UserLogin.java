package game.states;

import networking.Identificator;
import networking.NetworkListener;
import networking.messages.ConnectionMessages.*;
import tonegod.gui.controls.windows.LoginBox;
import tonegod.gui.core.Screen;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;


public class UserLogin extends AbstractAppState implements NetworkListener {
	Application app;
    Screen screen;
 
    LoginBox loginWindow;
 
    public UserLogin(Application app, Screen screen) {
        this.app = app;
        this.screen = screen;
    }
 
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
 
        initLoginWindow();
    }
 
    public void initLoginWindow() {
    	loginWindow = new LoginBox(screen, "loginWindow", new Vector2f(screen.getWidth()/2-175,screen.getHeight()/2-125)) {

			@Override
			public void onButtonCancelPressed(MouseButtonEvent arg0, boolean arg1) {
				app.destroy();
			}

			@Override
			public void onButtonLoginPressed(MouseButtonEvent arg0, boolean arg1) {
				app.getStateManager().getState(NetworkState.class).sendTCP(
					new LoginMessage(this.getTextUserName(), this.getTextPassword())
				);
			}
        };
        loginWindow.setIsResizable(false);
        loginWindow.setIsMovable(false);
        screen.addElement(loginWindow);
    }
 
    @Override
    public void cleanup() {
        super.cleanup();
 
        screen.removeElement(loginWindow);
    }
    
    public void finalizeUserLogin() {
    	loginWindow.hide();
    	setEnabled(false);
    	app.getStateManager().detach(app.getStateManager().getState(UserLogin.class));
    	app.getStateManager().attach(new PlayState(app, screen));
    }

	@Override
	public boolean onConnect(int id) {
		return false;
	}

	@Override
	public boolean onDisconnect(int id) {
		System.out.println("dc :( "+id);
		cleanup();
		app.destroy();
		return false;
	}

	@Override
	public boolean onMessageReceived(int id, Object message) {
		if (message instanceof AuthorizedMessage) {
			Identificator.connection_id = ((AuthorizedMessage)message).conn;
			Identificator.player_id = ((AuthorizedMessage)message).id;
			finalizeUserLogin();
		}
		if (message instanceof BadCredetnialsMessage) {
			System.out.println("bad credentials");
			loginWindow.setMsg("bad credentials");
		}
		if (message instanceof DisconnectedMessage) {
			cleanup();
			app.destroy();
		}
		return false;
	}
}