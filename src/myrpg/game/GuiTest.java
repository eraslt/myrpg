package myrpg.game;

import myrpg.gui.CraftingWindow;

import tonegod.gui.core.Screen;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;


public class GuiTest extends SimpleApplication {
	
	private static GuiTest instance = null;

	public BulletAppState bulletAppState;

	public Node collidable;
	
	public static void main(String[] args) {
		
		System.setProperty("AppName", "RRPG");
		
		GuiTest app = new GuiTest();
		
		AppSettings appSettings = new AppSettings(false);
        appSettings.setResolution(800, 600);
        appSettings.setFullscreen(false);
        appSettings.setVSync(false);
        appSettings.setTitle("SurCraft");
        appSettings.setUseInput(true);
        appSettings.setFrameRate(30);
        appSettings.setSamples(0);
        appSettings.setRenderer(AppSettings.LWJGL_OPENGL_ANY);
        appSettings.setAudioRenderer(AppSettings.LWJGL_OPENAL);
        app.setSettings(appSettings);
        app.setShowSettings(false);
		app.start();
	}
	
	public GuiTest() {
		super();
		instance = this;
	}
	
	public static GuiTest getRoot() {
		return instance;
	}
	
	Screen screen;
	CraftingWindow crafting;
	
	@Override
	public void simpleInitApp() {
		setPauseOnLostFocus(false);
		assetManager.registerLocator("assets/", FileLocator.class);
		flyCam.setEnabled(false);
		
		bulletAppState = new BulletAppState();
		stateManager.attach(bulletAppState);
		bulletAppState.setDebugEnabled(true); 
		
//		screen = new Screen(this, "Interface/GUI/style_map.gui.xml");
		screen = new Screen(this, "Interface/def/style_map.gui.xml");
		guiNode.addControl(screen);
		crafting =  new CraftingWindow(screen);
		crafting.clip();
	}
	
	@Override
	public void simpleUpdate(float tpf) {
	}
	
	
	@Override
	public void stop() {
		super.stop(); // continue quitting the game
	}
}