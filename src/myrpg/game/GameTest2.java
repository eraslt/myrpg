package myrpg.game;

import myrpg.game.states.MobsState;
import myrpg.game.states.MobsState.MyMob;
import myrpg.world.TerrainProvider;


import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;

/*
 * sitas lokaliai be servo 
 */

public class GameTest2 extends SimpleApplication {
	
	private static GameTest2 instance = null;

	public BulletAppState bulletAppState;

	public Node collidable;
	
	public static void main(String[] args) {
		GameTest2 app = new GameTest2();
		
		AppSettings appSettings = new AppSettings(false);
        appSettings.setResolution(800, 600);
        appSettings.setFullscreen(false);
        appSettings.setVSync(false);
        appSettings.setTitle("SurCraft");
        appSettings.setUseInput(true);
        appSettings.setFrameRate(30);
        appSettings.setSamples(0);
        appSettings.setRenderer(AppSettings.LWJGL_OPENGL_ANY);
        appSettings.setAudioRenderer(AppSettings.LWJGL_OPENAL);
        app.setSettings(appSettings);
        app.setShowSettings(false);
		app.start();
	}
	
	public GameTest2() {
		super();
		instance = this;
	}
	
	public static GameTest2 getRoot() {
		return instance;
	}
	
	@Override
	public void simpleInitApp() {
		setPauseOnLostFocus(false);
		assetManager.registerLocator("assets/", FileLocator.class);
		cam.setLocation(new Vector3f(0, 10, 0));
		flyCam.setMoveSpeed(100);
		
		bulletAppState = new BulletAppState();
		stateManager.attach(bulletAppState);
		
		
		initWorld();
		MobsState mobsState = new MobsState(this);
		stateManager.attach(mobsState);
		
		MyMob m = new MyMob();
		m.position = new Vector3f(0, 10, 0);
		m.target = new Vector3f(100, 10, 0);
		m.speed = 1f;
		mobsState.myMobs.put(0, m);
	}
	
	@Override
	public void simpleUpdate(float tpf) {
//		System.out.println(tpf);
	}
	
	private void initWorld() {
    	DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.37352666f, -0.50444174f, -0.7784704f));
        sun.setColor(ColorRGBA.White.clone().multLocal(2));
        rootNode.addLight(sun);
        
        rootNode.attachChild(
        		new TerrainProvider().generateMap(
        				cam, 
        				assetManager, 
        				bulletAppState.getPhysicsSpace()
        			)
        		);
    }

}