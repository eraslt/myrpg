package myrpg.game;

import networking.messages.CommonMessages;
import networking.messages.ConnectionMessages;
import networking.messages.EntityMessages;
import networking.messages.GuiMessages;
import myrpg.game.states.NetworkState;
import myrpg.game.states.UserLogin;
//import system.GameSettings;
import tonegod.gui.core.Screen;


import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;


public class GameTest extends SimpleApplication {
	
	private static GameTest instance = null;

	public BulletAppState bulletAppState;
	public NetworkState network;

	public Node collidable;
	
	public static void main(String[] args) {
		GameTest app = new GameTest();
		
		AppSettings appSettings = new AppSettings(false);
        appSettings.setResolution(800, 600);
        appSettings.setFullscreen(false);
        appSettings.setVSync(false);
        appSettings.setTitle("SurCraft");
        appSettings.setUseInput(true);
        appSettings.setFrameRate(30);
        appSettings.setSamples(0);
        appSettings.setRenderer(AppSettings.LWJGL_OPENGL_ANY);
        appSettings.setAudioRenderer(AppSettings.LWJGL_OPENAL);
        app.setSettings(appSettings);
        app.setShowSettings(false);
//		app.setSettings(GameSettings.settings.getAppSettings());
		app.start();
	}
	
	public GameTest() {
		super();
		instance = this;
	}
	
	public static GameTest getRoot() {
		return instance;
	}
	
	@Override
	public void simpleInitApp() {
		setPauseOnLostFocus(false);
		assetManager.registerLocator("assets/", FileLocator.class);
		getFlyByCamera().setEnabled(false);
        mouseInput.setCursorVisible(true);
		
		bulletAppState = new BulletAppState();
		stateManager.attach(bulletAppState);
		
		network = new NetworkState();
		stateManager.attach(network);
		
		/* register messages and connect*/
		network.registerMessages(CommonMessages.GetMessages());
		network.registerMessages(ConnectionMessages.GetMessages());
		network.registerMessages(GuiMessages.GetMessages());
		network.registerMessages(EntityMessages.GetMessages());
		
		network.connect();
		/* register messages and connect*/
		
		screen = new Screen(this, "Interface/GUI/style_map.gui.xml");
		guiNode.addControl(screen);
		
		initializeLogin();
	}
	
	public void initializeLogin(){
		
		UserLogin login = new UserLogin(this, screen);
		
		stateManager.attach(login);
		
		network.addListener(login);
	}
	
	private Screen screen;
	public Screen getScreen() {
		return screen;
	}
	
	@Override
	public void simpleUpdate(float tpf) {
//		System.out.println(tpf);
	}

}