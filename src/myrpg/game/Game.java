package myrpg.game;

import myrpg.objects.ObjectControl;
import myrpg.objects.events.ObjectAction;
import simpleGrid.SimpleGrid;
import system.*;
import system.grid.Grid;
import system.grid.GridLoader;

import tonegod.gui.core.Screen;
import myrpg.world.TerrainProvider;


import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;


import actions.ActionManager;
import actions.LocalActionConnector;
import actor.BuildMode;
import actor.Player;


public class Game extends SimpleApplication {
	
	private static Game instance = null;

	public BulletAppState bulletAppState;
	public BuildMode buildMode;
	private Controls controls = null;
	public RawInputManager rawInputManager;
	public Player player;
	ActionManager am;
	

	public Node collidable;
	
	public static void main(String[] args) {
		Game app = new Game();
		app.setSettings(GameSettings.settings.getAppSettings());
		app.start();
	}
	
	public Game() {
		super();
		instance = this;
	}
	
	public static Game getRoot() {
		return instance;
	}
	
	@Override
	public void simpleInitApp() {
		assetManager.registerLocator("assets/", FileLocator.class);
		getFlyByCamera().setEnabled(false);
        mouseInput.setCursorVisible(true);
		
		rawInputManager = new RawInputManager();
		inputManager.addRawInputListener(rawInputManager);
		
		bulletAppState = new BulletAppState();
		stateManager.attach(bulletAppState);
		
//		bulletAppState.getPhysicsSpace().enableDebug(assetManager);
		
		controls = new Controls(this);
		rawInputManager.setHandler(controls);
		
		screen = new Screen(this);
		guiNode.addControl(screen);
		
		initializeGame();
	}
	
	SimpleGrid mg;
	public SimpleGrid getGrid() {
		return mg;
	}
	
	
	public void initializeGame(){
		
		collidable = new Node("collidable");
		rootNode.attachChild(collidable);
		
		buildMode = new BuildMode(this);

		getFlyByCamera().setEnabled(true);
        mouseInput.setCursorVisible(false);

        rootNode.attachChild(new TerrainProvider().generateMap(cam, assetManager, bulletAppState.getPhysicsSpace()));
        
		controls.initCharControl();
		
		player = new Player();
		
		//XXX: just for testing now
		am = new ActionManager(new LocalActionConnector());
		stateManager.attach(am);
		cam.setFrustumFar(3000);
		
		
		
		DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.37352666f, -0.50444174f, -0.7784704f));
        sun.setColor(ColorRGBA.White.clone().multLocal(2));
        rootNode.addLight(sun);

    	mg = new SimpleGrid(1024, 128, new GridLoader());
        mg.setInvertY(false);
        mg.setOffset(512, 512);
        
        Spatial spatial = assetManager.loadModel("Models/rock1.j3o");
		ObjectControl.add(spatial, new ObjectAction(1));
		spatial.setLocalTranslation(Vector3f.ZERO);
		rootNode.attachChild(spatial);
	}
	
	private Screen screen;
	public Screen getScreen() {
		return screen;
	}
	
	public void addInfo(String text) {
		//TODO
	}
	
	
	@Override
	public void destroy() {
//		ClientNetworkManager.close();
		super.destroy();
	}

	@Override
	public void simpleUpdate(float tpf) {
		if (controls.playing) {
			controls.update();
			cam.setLocation(controls.getLocation());
			mg.update(controls.getLocation().x,controls.getLocation().z);
		}
	}

	public BulletAppState getPhysics() {
		return bulletAppState;
	}

	public Controls getControls(){
		return controls;
	}
	
	public void closeGame(){
		guiNode.detachAllChildren();
		rootNode.detachAllChildren();
		this.stop();
	}
	
	public ActionManager getActionManager(){
		return am;
	}
	
	//http://code.google.com/p/jmonkeyengine/source/browse/trunk/engine/src/test/jme3test/tools/TestSaveGame.java
	//http://jmonkeyengine.org/wiki/doku.php/jme3:advanced:save_and_load
	/* This is called when the user quits the app. */
	@Override
	public void stop() {
		Grid.save();
		player.savePlayerData();
//		BinaryExporter exporter = BinaryExporter.getInstance();
//		File file = new File("assets/save2.j3o");
//		try {
//			exporter.save(collidable, file);
//	    } catch (IOException ex) {
//	    	//Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Error: Failed to save game!", ex);
//	    	ex.printStackTrace();
//	    }
		super.stop(); // continue quitting the game
	}
	
	/*info stuff*/
}