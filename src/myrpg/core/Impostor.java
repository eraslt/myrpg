package myrpg.core;

import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;

public class Impostor extends Mesh {

	public Impostor() {
        updateGeometry(1);
    }
	
	public Impostor(float size) {
        updateGeometry(size);
    }
	
    public void updateGeometry(float size) {
        setBuffer(VertexBuffer.Type.Position, 3, new float[]{
            0, 0, 0,
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        });

        setBuffer(VertexBuffer.Type.TexCoord, 2, new float[]{
            0, 0,
            1, 0,
            0, 1,
            1, 1
        });

        setBuffer(VertexBuffer.Type.TexCoord2, 3, new float[]{
            -1*size, 0,
             0, 1*size,
             0, 0,
            -1*size, 2*size,
             0, 1*size,
             2*size, 0
        });
        
        setBuffer(VertexBuffer.Type.Normal, 3, new float[]{
    		0, 0, 1,
    		0, 0, 1,
    		0, 0, 1,
    		0, 0, 1
		});

        setBuffer(VertexBuffer.Type.Index, 1, new short[]{
            0, 1, 2,
            1, 3, 2
        });

        updateBound();
    }
}
