package myrpg.core;

import java.util.List;

import myrpg.objects.ObjectControl;
import myrpg.objects.events.BuildingAction;
import myrpg.objects.events.ObjectAction;
import myrpg.player.Player;
import myrpg.world.TerrainProvider;

import myrpg.buildings.BuildingManager;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CylinderCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.GhostControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResults;
import com.jme3.input.MouseInput;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

@SuppressWarnings({"deprecation", "unused"})
public class Controls implements EventHandler {
	
	private boolean left = false, right = false, up = false, down = false, attack = false;
	private CharacterControl physicsCharacter;
	private Vector3f walkDirection = Vector3f.ZERO;
	private Application app;
	
	private RawInputManager rawInputManager;
	
	private float moveSpeend = 1;
	
	public boolean playing = false;
	
	private InputKeys keys = new InputKeys();
	
	/*building mode stuff*/
	private Material RED;
	private Material GREEN;
	private Spatial buildSpatial = null;
	private Spatial realSpatial = null;
	private boolean isOkToPlace = true;
	private boolean isBuildingMode = false;
	
	public Controls(Application app) {
		this.app = app;
		rawInputManager = new RawInputManager(this);
		app.getInputManager().addRawInputListener(rawInputManager);
		
		RED = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		RED.setColor("Color", new ColorRGBA(1,0,0,0.9f));
		RED.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
	         
		GREEN = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		GREEN.setColor("Color", new ColorRGBA(0,1,0,0.9f));
		GREEN.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
	}
	
	public void initCharControl(Vector3f pos) {
		physicsCharacter = new CharacterControl(new CapsuleCollisionShape(1.5f, 6f, 1), 0.05f);
		app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().add(physicsCharacter);
		physicsCharacter.setJumpSpeed(20);
		physicsCharacter.setFallSpeed(30);
		physicsCharacter.setGravity(50);
		physicsCharacter.setPhysicsLocation(pos);
		playing = true;
	}
	
	public void update() {
		Vector3f camDir = app.getCamera().getDirection().clone().multLocal(0.6f);
		Vector3f camLeft = app.getCamera().getLeft().clone().multLocal(0.4f);
		walkDirection.set(0, 0, 0);
		if (left)  { walkDirection.addLocal(camLeft); }
		if (right) { walkDirection.addLocal(camLeft.negate()); }
		if (up)    { walkDirection.addLocal(camDir); }
		if (down)  { walkDirection.addLocal(camDir.negate()); }
		physicsCharacter.setWalkDirection(walkDirection.mult(moveSpeend));
		
		//TODO: attack timer for auto attack and send message for attack
	}
	
	//TODO: check distance
	private void pick() {
		CollisionResults results = new CollisionResults();
		Ray ray = new Ray(app.getCamera().getLocation(), app.getCamera().getDirection());
		((SimpleApplication)app).getRootNode().collideWith(ray, results);
		if (results.size() > 0) {
			if (results.getClosestCollision().getDistance()>5) return;
			//for simple geoms
			Spatial spatial = results.getClosestCollision().getGeometry();
			//for complex geoms
			for( Spatial s = results.getClosestCollision().getGeometry(); s.getParent() != null; s = s.getParent() ) {
				if (s.getName().startsWith("nodeforgeom")) {
					spatial = s;
					break;
				}
			}
			if (!ObjectControl.exec(spatial.getName()))
				ObjectControl.exec(spatial.getParent().getName());
		}
	}
	
	@Override
	public void onKeyInput(KeyInputEvent event, int keyCode, boolean pressed) {
		if (!isBuildingMode)
			handleInput(keyCode, pressed);
	}
	
	@Override
	public void onMouseInput(int button, int x, int y, boolean pressed) {
		if (!isBuildingMode)
			handleInput(button /*- 76*/, pressed);
		else
			onMouseInputForBuilding(button /*- 76*/, pressed);
	}
	
	@Override
	public void onMouseMove(MouseMotionEvent event) {
		if (isBuildingMode)
			onMouseMoveForBuilding(event);
	}
	
	private boolean mouse_locked = true;
	
	private void handleInput(int input, boolean pressed){
		if (input == keys.get(InputKeys.LEFT)) left = (pressed)?(true):(false);
		if (input == keys.get(InputKeys.UP)) up = (pressed)?(true):(false);
		if (input == keys.get(InputKeys.RIGHT)) right = (pressed)?(true):(false);
		if (input == keys.get(InputKeys.DOWN)) down = (pressed)?(true):(false);
		if (input == keys.get(InputKeys.JUMP)) if(!pressed) jump();		
//		if (input == keys.get(InputKeys.INFO)) if(!pressed) info();
		if (input == keys.get(InputKeys.MOUSE1)) if(!pressed) pick(); //attack = (pressed)?(true):(false);
//		TODO: add player state??
//		if (input == keys.get(InputKeys.INVENTORY)) if(!pressed) root.getPlayer().getInventory().clip();
//		if (input == keys.get(InputKeys.SKILLS)) if(!pressed) root.getPlayer().getSkills().clip();
		if (input == keys.get(InputKeys.MOUSE3))
			if(!pressed) {
				if (!mouse_locked) {
					enableControls();
				} else {
					disableControls();
				}
			}
		
		if (input == keys.get(InputKeys.BUILD)) {
			Geometry b = new Geometry("box", new Box(1,1,1));
			Material mat1 = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
			mat1.setColor("Color", ColorRGBA.Brown);
			b.setMaterial(mat1);
			startBuildingMode(b);
		}
	}
	
	public void disableControls() {
		mouse_locked = false;
		((SimpleApplication)app).getFlyByCamera().setEnabled(false);
		app.getInputManager().setCursorVisible(true);
	}
	
	public void enableControls() {
		mouse_locked = true;
		((SimpleApplication)app).getFlyByCamera().setEnabled(true);
		app.getInputManager().setCursorVisible(false);
	}
	
	private void jump(){
		//test for height
		physicsCharacter.jump();
	}
	
	public Vector3f getRotation() {
		// TODO Auto-generated method stub
		return Vector3f.ZERO;
	}

	public Vector3f getViewDirection() {
		return physicsCharacter.getViewDirection();
	}
	
	public Vector3f getWalkDirection() {
		return walkDirection;
	}
	
	public Vector3f getLocation() {
//		return Vector3f.ZERO;
		return physicsCharacter.getPhysicsLocation();
	}
	
	private void onMouseInputForBuilding(int button, boolean pressed) {
		if (!pressed) return;
		if (button==MouseInput.BUTTON_LEFT) {
			if (isOkToPlace) {
				realSpatial.setLocalRotation(buildSpatial.getLocalRotation());
				realSpatial.setLocalTranslation(buildSpatial.getLocalTranslation());
				((SimpleApplication)app).getRootNode().attachChild(realSpatial);
				
				realSpatial.addControl(new RigidBodyControl(new BoxCollisionShape(new Vector3f(1, 1, 1)), 0f));
				app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().add(realSpatial);
				int id = BuildingManager.getInstance().addBuilding(
						1/*selected_building_id*/, 
						realSpatial.getLocalTranslation(), 
						realSpatial.getLocalRotation()
					);
				ObjectControl.add(realSpatial, new BuildingAction(id));
				
				realSpatial = null;
				buildSpatial.removeFromParent();
				buildSpatial = null;
				isBuildingMode = false;
				app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().remove(ghostControl);
				//TODO fire building event;
			}
		}
		if (button==MouseInput.BUTTON_RIGHT) {
			if (null != buildSpatial) buildSpatial.removeFromParent();
			buildSpatial = null;
			realSpatial = null;
			isBuildingMode = false;
			app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().remove(ghostControl);
		}
	}
	
	private void onMouseMoveForBuilding(MouseMotionEvent event) {
		if (null == buildSpatial) return;
		if(event.getDeltaWheel() > 0){
            buildSpatial.rotate(0, 0.1f, 0);
        }
        if(event.getDeltaWheel() < 0){
            buildSpatial.rotate(0, -0.1f, 0);
        }
        CollisionResults results = new CollisionResults();
		Ray ray = new Ray(((SimpleApplication)app).getCamera().getLocation(), ((SimpleApplication)app).getCamera().getDirection());
		TerrainProvider.getInstance().getTerrain().collideWith(ray, results);
		if (results.size() > 0) {
			isOkToPlace(results.getClosestCollision().getDistance());
			buildSpatial.setLocalTranslation(results.getClosestCollision().getContactPoint());
		}
	}
	
	private GhostControl ghostControl;
	private void isOkToPlace(float d) {
		// TODO: state_changed
		isOkToPlace = true;
		List<PhysicsCollisionObject> l = ghostControl.getOverlappingObjects();
		int c = ghostControl.getOverlappingCount();
		for (int i=0; i<c; i++) {
			if (!l.get(i).getUserObject().getClass().getName().contains("TerrainQuad"))  {
				isOkToPlace = false;
			}
		}
		if (isOkToPlace)
			if (d<10 || d>50) isOkToPlace = false;
		if (isOkToPlace)
			buildSpatial.setMaterial(GREEN);
		else
			buildSpatial.setMaterial(RED);
	}
	
	public void startBuildingMode(Spatial Spatial) {
		enableControls();
		isBuildingMode = true;
		realSpatial = Spatial;
		buildSpatial = realSpatial.clone();
		buildSpatial.setMaterial(RED);
		//test with ghost
		ghostControl = new GhostControl(CollisionShapeFactory.createBoxShape(buildSpatial));
		buildSpatial.addControl(ghostControl);
		app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().add(ghostControl);

		//test with rigid
//		phy = new RigidBodyControl(0.0f);
//		buildSpatial.addControl(phy);
//		app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().add(phy);
		
		((SimpleApplication)app).getRootNode().attachChild(buildSpatial);
	}
}
