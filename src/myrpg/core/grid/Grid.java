package myrpg.core.grid;

import myrpg.game.Game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.thoughtworks.xstream.XStream;

import myrpg.core.InputKeys;

public class Grid {
	
	private final static String DATA_FILE = "data/cells.xml";
	
	private static HashMap<Integer, HashMap<Integer, GridItem>> cells = Grid.read();
	
	static public boolean hassCell(int cell) {
		return cells.containsKey(cell);
	}
	
	static public HashMap<Integer, GridItem> getCell(int cell) {
		return cells.get(cell);
	}
	
	static public boolean hassItem(int cell, int item) {
		return cells.get(cell).containsKey(item);
	}
	
	static public GridItem getItem(int cell, int item) {
		return cells.get(cell).get(item);
	}
	
	static public void removeItem(int cell, int item) {
		cells.get(cell).remove(item);
	}

	static public int putItem(int item_id, int count, float x, float y, float z) {
		int cell = Game.getRoot().getGrid().getCurentTile();
		if (!cells.containsKey(cell)) {
			cells.put(cell, new HashMap<Integer, GridItem>());
		}
		cells.get(cell).put(cells.get(cell).size(), new GridItem(item_id, count, x, y, z));
		return cell;
	}
	
	/*static public int putObject(int id, float x, float y, float z) {
		int cell = Game.getRoot().getGrid().getCurentTile();
		if (!cells.containsKey(cell)) {
			cells.put(cell, new HashMap<Integer, GridItem>());
		}
		cells.get(cell).put(cells.get(cell).size(), new GridItem(item_id, count, x, y, z));
		return cell;
	}*/
	

	public static void save() {
		try {
			File file = new File("data");
			file.mkdirs();
			
			XStream xstream = new XStream();
			
			FileOutputStream fos = new FileOutputStream(DATA_FILE);
			xstream.toXML(cells,fos);
		} catch (IOException ex) {
		    Logger.getLogger(InputKeys.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	@SuppressWarnings("unchecked")
	static private HashMap<Integer, HashMap<Integer, GridItem>> read() {
		//TODO: remove this, file should be always
		File f = new File(DATA_FILE);
		if(!f.exists()) { 
	    	Grid.save();
		}
		
		cells = new HashMap<Integer, HashMap<Integer, GridItem>>();
		try {
			XStream xstream = new XStream();
			FileInputStream fis = new FileInputStream(DATA_FILE);
			cells = (HashMap<Integer, HashMap<Integer, GridItem>>) xstream.fromXML(fis);
		} catch (IOException ex) {
		    Logger.getLogger(InputKeys.class.getName()).log(Level.SEVERE, null, ex);
		}
		return cells;
	}
}
