package myrpg.core.grid;


import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;

import myrpg.objects.ObjectControl;
import myrpg.objects.events.ObjectAction;
import myrpg.objects.events.ObjectPickup;

import myrpg.game.Game;
import info.mindes.SimpleGrid.*;
import info.mindes.DataProviders.DataProvider;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class GridLoader implements SimpleGridEvent {
	
	private static GridLoader instance = null;
	
	public static GridLoader getInstance() {
		if(instance == null) {
			instance = new GridLoader();
		}
		return instance;
	}
	
	
	public void addItem(int item_id, int cnt, Vector3f pos) {
		int tile = Grid.putItem(item_id, cnt, pos.x, pos.y, pos.z);
		Node a = (Node)Game.getRoot().collidable.getChild("loader-node-"+tile);
		if (null!=a) {
			String str = DataProvider.item.getItemAttribute(item_id, "model");
			Spatial spatial = Game.getRoot().getAssetManager().loadModel("Models/"+str);
			ObjectControl.add(spatial, new ObjectPickup(item_id, cnt));
			spatial.setLocalTranslation(pos);
			//XXX: different nodes for items??
			a.attachChild(spatial);
		}
	}
	
	public void addObject(int object_id, Vector3f pos) {
		int tile = Grid.putItem(object_id, 0, pos.x, pos.y, pos.z);
		Node a = (Node)Game.getRoot().collidable.getChild("loader-node-"+tile);
		if (null!=a) {
			String str = DataProvider.item.getItemAttribute(object_id, "model");
			Spatial spatial = Game.getRoot().getAssetManager().loadModel("Models/"+str);
			ObjectControl.add(spatial, new ObjectAction(object_id));
			spatial.setLocalTranslation(pos);
			//XXX: different nodes for items??
			a.attachChild(spatial);
		}
	}
	
	/*
	 * Assinc item loading?
	 * http://hub.jmonkeyengine.org/wiki/doku.php/jme3:advanced:multithreading
     * show how to do it properly 
	 */
	@Override
	public void load(final int tile) {
		Game.getRoot().enqueue(new Callable<Void>() {
            public Void call(){
            	aload(tile);
            	return null;
            }
		});
	}
	
	
	public void aload(int tile) {
		Node a = (Node)Game.getRoot().collidable.getChild("loader-node-"+tile);
		if (null==a) {
			a = new Node();
			a.setName("loader-node-"+tile);
		}
		Game.getRoot().collidable.attachChild(a);
		
		if (Grid.hassCell(tile) && !Grid.getCell(tile).isEmpty()) {
			Set<Integer> keys = Grid.getCell(tile).keySet();
    		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
        		
    			GridItem item = Grid.getItem(tile, i.next());
        		
    			String str = DataProvider.item.getItemAttribute(item.id, "model");
    			
    			Spatial spatial = Game.getRoot().getAssetManager().loadModel("Models/"+str);
    			spatial.setLocalTranslation(item.x, item.y, item.z);
    			
    			ObjectControl.add(spatial, new ObjectPickup(item.id, item.count));
    			
    			a.attachChild(spatial);
    		}
		}
	}

	@Override
	public void unload(int tile) {
		Spatial a = Game.getRoot().collidable.getChild("loader-node-"+tile);
		if (null!=a)
			a.removeFromParent();
	}


	public void removeItem(int id) {
		int tile = Game.getRoot().getGrid().getCurentTile();
		if (Grid.hassCell(tile) && Grid.hassItem(tile, id))
			Grid.removeItem(tile, id);
	}
}
