package myrpg.core.grid;

public class GridItem {
	
	public GridItem(int id, int count, float x, float y, float z) {
		this.id = id;
		this.count = count;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public int count = 1;
	public int id = 0;
	public int type = 0;
	public float x = 0;
	public float y = 0;
	public float z = 0;
	
}
