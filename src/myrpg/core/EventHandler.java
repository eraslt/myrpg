package myrpg.core;

import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseMotionEvent;

public interface EventHandler {
	
	void onKeyInput(KeyInputEvent event, int keyKode, boolean pressed);

    void onMouseMove(MouseMotionEvent event);

    void onMouseInput(int button, int x, int y, boolean pressed);
    
}
