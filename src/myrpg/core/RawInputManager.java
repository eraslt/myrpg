package myrpg.core;

import com.jme3.input.RawInputListener;
import com.jme3.input.event.JoyAxisEvent;
import com.jme3.input.event.JoyButtonEvent;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.input.event.TouchEvent;


public class RawInputManager implements RawInputListener {

	private EventHandler eh;
	
	public RawInputManager(EventHandler eh) {
		this.eh = eh;
	}
	
	public RawInputManager(){
		
	}
	
	public void setHandler(EventHandler eh) {
		this.eh = eh;
	}
	
	@Override
	public void beginInput() {
	}

	@Override
	public void endInput() {
	}

	@Override
	public void onJoyAxisEvent(JoyAxisEvent arg0) {
		
	}

	@Override
	public void onJoyButtonEvent(JoyButtonEvent arg0) {
	}

	@Override
	public void onKeyEvent(KeyInputEvent arg0) {
		if (null != eh)
			eh.onKeyInput(arg0, arg0.getKeyCode(), arg0.isPressed());
	}

	@Override
	public void onMouseButtonEvent(MouseButtonEvent arg0) {
		if (null != eh)
			eh.onMouseInput(arg0.getButtonIndex(), arg0.getX(), arg0.getY(), arg0.isPressed());
	}

	@Override
	public void onMouseMotionEvent(MouseMotionEvent arg0) {
		if (null != eh)
			eh.onMouseMove(arg0);
	}

	@Override
	public void onTouchEvent(TouchEvent arg0) {
	}
	

}
