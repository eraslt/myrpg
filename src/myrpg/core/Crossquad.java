package myrpg.core;

import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer.Type;

public class Crossquad extends Mesh {

	private float width;
    private float height;

    public Crossquad() {
        updateGeometry(1, 1);
    }
    
    public Crossquad(float size) {
        updateGeometry(size, size);
    }

    public Crossquad(float width, float height){
    	updateGeometry(width, height);
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }

    public void updateGeometry(float width, float height) {
        this.width = width;
        this.height = height;
        
        float half = width/2;
        
        setBuffer(Type.Position, 3, new float[]{0,      0,      0,
                                                width,  0,      0,
                                                width,  height, 0,
                                                0,      height, 0,
                                                
                                                half,	0,	-half,
                                                half,	0,	 half,
                                                half,	height, -half,
                                                half,	height, half
                                                });
        
        setBuffer(Type.TexCoord, 2, new float[]{0, 0,
                                                1, 0,
                                                1, 1,
                                                0, 1,
                                                0, 0,
                                                1, 0,
                                                0, 1,
                                                1, 1});
        
        setBuffer(Type.Normal, 3, new float[]{0, 0, 1,
                                              0, 0, 1,
                                              0, 0, 1,
                                              0, 0, 1,
                                              0, 0, 1,
                                              0, 0, 1,
                                              0, 0, 1,
                                              0, 0, 1});
        if (height < 0){
            setBuffer(Type.Index, 3, new short[]{0,2,1, 0,3,2, 4,6,5, 5,6,7});
        }else{
            setBuffer(Type.Index, 3, new short[]{0,1,2, 0,2,3, 4,5,6, 5,7,6});
        }
        updateBound();
    }
}
