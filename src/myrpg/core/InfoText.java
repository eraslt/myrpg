package myrpg.core;

import java.util.LinkedList;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

//TODO: something bad need to clear text first
public class InfoText extends AbstractControl {

	private long curTime = System.currentTimeMillis();
	private long lastTime;
	private LinkedList<String> infoText = new LinkedList<String>();
	private LinkedList<Long> infoTime = new LinkedList<Long>();
	private BitmapFont guiFont;
	private int tlen = 5;
	
	public InfoText(BitmapFont font) {
		guiFont = font;
	}
	
	public void addInfo(String text) {
		long l = 5000;
		infoText.addLast(text);
		infoTime.addLast(l);
		while (infoText.size()>tlen) {
			infoText.removeFirst();
			infoTime.removeFirst();
		}
	}
	
	private BitmapText createText(String text, int x, int y) {
		BitmapText line = new BitmapText(guiFont, false);
		line.setText(text);
		line.setColor(ColorRGBA.Blue);
		line.setSize(20);
		line.setLocalTranslation(x, y, 0);
		return line;
	}
	
	@Override
	protected void controlUpdate(float tpf) {
		curTime = System.currentTimeMillis();
		if (infoText.size() != 0) {
			for (int i = 0; i < infoText.size(); i++) {
				((Node)getSpatial()).attachChild(createText(infoText.get(i), 150,
						20 + (infoText.size() - i) * 25));
				infoTime.set(i, infoTime.get(i) - curTime - lastTime);
			}
		}
		lastTime = curTime;
	}

	@Override
	protected void controlRender(RenderManager arg0, ViewPort arg1) {
		// TODO Auto-generated method stub
	}
	
}
