package myrpg.core;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;

public class InputKeys {
	
	//keyboard events 
	public static final int LEFT = 1;
	public static final int RIGHT = 2;
	public static final int UP = 3;
	public static final int DOWN = 4;
	public static final int BUILD = 5;
	public static final int JUMP = 6;
	public static final int INVENTORY = 7;
	public static final int INFO = 8;
	public static final int SKILLS = 9;
	
	//mouse event
	public static final int MOUSE1 = 66;
	public static final int MOUSE2 = 67;
	public static final int MOUSE3 = 68;
	
	private Hashtable<Integer, Integer> keys = new Hashtable<Integer, Integer>();
	
	public InputKeys() {
//		if (!load()) {
			setDefault();
			save();
//		}
	}
	
	public void setDefault() {
		keys.put(LEFT, KeyInput.KEY_A);
		keys.put(RIGHT, KeyInput.KEY_D);
		keys.put(UP, KeyInput.KEY_W);
		keys.put(DOWN, KeyInput.KEY_S);
		keys.put(JUMP, KeyInput.KEY_SPACE);
		keys.put(BUILD, KeyInput.KEY_B);
		keys.put(INVENTORY, KeyInput.KEY_I);
		keys.put(INFO, KeyInput.KEY_F1);
		keys.put(SKILLS, KeyInput.KEY_C);

		keys.put(MOUSE1, MouseInput.BUTTON_LEFT);
		keys.put(MOUSE2, MouseInput.BUTTON_RIGHT);
		keys.put(MOUSE3, MouseInput.BUTTON_MIDDLE);
	}
	
	
	public void set(int key, int value) {
		keys.put(key, value);
	}
	
	public int get(int key) {
		int key_numb = -1;
		try {
			key_numb = keys.get(key);
		} catch(Exception ex){}
		return key_numb;
	}
	
	private final static String DATA_FILE = "keys.properties";
	public void save() {
		try {
			File file = new File(System.getProperty("user.home") + "/" + System.getProperty("AppName"));
			file.mkdirs();
			
			FileOutputStream fos = new FileOutputStream(System.getProperty("user.home") + "/" + System.getProperty("AppName") + "/" + DATA_FILE);
			XMLEncoder oos = new XMLEncoder(fos);
			oos.writeObject(keys);
			oos.close();
		} catch (IOException ex) {
		    Logger.getLogger(InputKeys.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean load() {
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.home") + "/" + System.getProperty("AppName") + "/" + DATA_FILE);
			XMLDecoder ois = new XMLDecoder(fis);
			//TODO: check for a type
			keys = (Hashtable<Integer, Integer>) ois.readObject();
			ois.close();
			return true;
		} catch (IOException ex) {
		    Logger.getLogger(InputKeys.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}
}
