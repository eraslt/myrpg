package myrpg.objects;

public interface ObjectEvent {

	public boolean exec(String name);
	
}
