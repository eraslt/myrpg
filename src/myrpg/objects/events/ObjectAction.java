package myrpg.objects.events;


import myrpg.actions.ActionManager;
import myrpg.objects.ObjectEvent;

public class ObjectAction implements ObjectEvent {
	
	private int object_id;
	
	public ObjectAction(int object_id) {
		this.object_id = object_id;
	}
	
	public void setId(int id) {
		object_id = id;
	}
	
	@Override
	public boolean exec(String name) {
		ActionManager.getInstance().objectCheck(object_id);
		return false;
	}
	
}
