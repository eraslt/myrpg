package myrpg.objects.events;

import myrpg.gui.BuildingGui;
import myrpg.objects.ObjectEvent;

public class BuildingAction implements ObjectEvent {
	
	private int building_id;
	
	public BuildingAction(int building_id) {
		this.building_id = building_id;
	}
	
	@Override
	public boolean exec(String name) {
		BuildingGui.getInstance().init(building_id, name);
		return false;
	}
	
}
