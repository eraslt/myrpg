package myrpg.objects.events;

import myrpg.objects.ObjectEvent;
import myrpg.player.Player;
import myrpg.core.grid.GridLoader;

public class ObjectPickup implements ObjectEvent {
	
	private int id;
	private int count = 0;

	public ObjectPickup() {
		this.id = 0;
		this.count = 0;
	}
	
	public ObjectPickup(int id, int count) {
		this.id = id;
		this.count = count;
	}

	@Override
	public boolean exec(String name) {
		Player.getInstance().getInventory().addItem(id, count);
//		Game.getRoot().collidable.getChild(name).removeFromParent();
		GridLoader.getInstance().removeItem(id);
		return true;
	}
}
