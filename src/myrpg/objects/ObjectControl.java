package myrpg.objects;


import java.util.concurrent.ConcurrentHashMap;

import com.jme3.scene.Spatial;

/*
TODO:
all interactable objects hold id in name
have collection of objects by id(might have link to spatial) + additional data
have attached events(actions) by id
data update with spatial updates for mp(later)

getByid
getByComonent
hassComponent
event(for mp)
*/
public final class ObjectControl {

	private static long idx = 0;

	private static ConcurrentHashMap <Long, ObjectEvent> objects = new ConcurrentHashMap <Long, ObjectEvent>();
	
	public static boolean exec(String name) {
//		System.out.println("ObjectControl got name:" + name);
		String[] tokens = name.split("-");
		for(String token : tokens ){
			if (token.contains("Object_")) {
				String[] tmp = token.split("_");
				if (tmp.length==2 && contains(Long.parseLong(tmp[1]))) {
					if (get(Long.parseLong(tmp[1])).exec(name)) {
						remove(Long.parseLong(tmp[1]));
					}
					return true;
				} 
				return false;
			}
		}
		return false;
	}
	
	private static String makeName(Long id) {
		return "Object_" + id;
	}
	
	private static /*synchronized*/ long MakeID() {
		++idx;
		return idx;
	}

	public static long add(Spatial spatial, ObjectEvent o) {
		long id = MakeID();
		//XXX: make ref to spatial
		spatial.setName(makeName(id));
		objects.put(id, o);
		return id;
	}

	public static boolean contains(long id) {
		return objects.containsKey(id);
	}
	
	public static ObjectEvent get(long id) {
		return objects.get(id);
	}        

	private static void remove(long id) {
		synchronized(objects) {
			objects.remove(id);
		}
	}        
}
