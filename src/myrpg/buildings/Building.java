package myrpg.buildings;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

public class Building {
	
	int id = 0;
	int type = 0;
	float px = 0;
	float py = 0;
	float pz = 0;
	
	float rw = 0;
	float rx = 0;
	float ry = 0;
	float rz = 0;
	
	String name;
	boolean ready = false;
	
	public Map<Integer, Integer> items = new HashMap<>();
	
	public Building() {}
	
	public Building(Vector3f pos, Quaternion rot) {
		setPosition(pos);
		setRotation(rot);
	}
	
	public void setPosition(Vector3f pos) {
		this.px = pos.getX();
		this.py = pos.getY();
		this.pz = pos.getZ();
	}
	
	public Vector3f getPosition() {
		return new Vector3f(px, py, pz);
	}
	
	public void setRotation(Quaternion rot) {
		this.rw = rot.getW();
		this.rx = rot.getX();
		this.ry = rot.getY();
		this.rz = rot.getZ();
	}
	
	public Quaternion getRotation() {
		return new Quaternion(rx, ry, rz, rw);
	}
	
	void buildingComplete() {
		ready = true;
	}

	public void addMaterial(int id, int cnt) {
		int req = items.get(id);
		req -= cnt;
		if (req<=0) items.remove(id);
		else items.put(id, req);
		
		if (checkIfBuillt())
			buildingComplete();
	}
	
	private boolean checkIfBuillt() {
		return false;
	}
	
}