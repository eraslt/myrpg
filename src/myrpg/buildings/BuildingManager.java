package myrpg.buildings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import myrpg.objects.ObjectControl;
import myrpg.objects.events.BuildingAction;
import myrpg.player.Player;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.thoughtworks.xstream.XStream;


public class BuildingManager extends AbstractAppState {
	
	private Map<Integer, Building> buildings = new HashMap<>();
    
    private static BuildingManager instance = null;
    private int last_id = 0;
    
    private int uid() {
//    	while(buildings.containsKey(++last_id));
//    	return ++last_id;
    	return ++last_id;
    }
    
    Application app;
    public BuildingManager(Application app) {
    	this.app = app;
    	instance = this;
    }
    
    public static BuildingManager getInstance() {
		return instance;
	}
    
    public int addBuilding(int building_type_id, Vector3f position, Quaternion rotation) {
    	int new_id = uid();
    	Building b = getInfo(building_type_id);
    	b.setPosition(position);
    	b.setRotation(rotation);
    	buildings.put(new_id, b);
    	return new_id;
    }
    
    public void removeBuilding(int id, String node_name) {
    	//TODO: remove from object control
    	if (buildings.containsKey(id)) {
    		buildings.remove(id);
    		Spatial s = ((SimpleApplication)app).getRootNode().getChild(node_name);
    		if (null != s) {
    			app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().remove(s);
    			s.removeFromParent();
    		}
    	}
    }
    
    public void setup() {
    	
    	loadBuildings();

		Set<Integer> keys = buildings.keySet();
		Building b; 
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			b = buildings.get(i.next());
			/*if (b.ready) {
				n = new Node("vienoks buildingas");
			} else {
				n = new Node("kitoks builginas");
			}*/
			
			Geometry geom = new Geometry("box", new Box(1,1,1));
//			Spatial geom = app.getAssetManager().loadModel("Models/fireplace1/fireplace1.j3o");
//			geom.scale(5);
			geom.setShadowMode(ShadowMode.CastAndReceive);
			geom.setLocalTranslation(b.getPosition());
			geom.setLocalRotation(b.getRotation());
			
			
			Material mat1 = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
			mat1.setColor("Color", ColorRGBA.Brown);
			geom.setMaterial(mat1);
			geom.addControl(new RigidBodyControl(new BoxCollisionShape(new Vector3f(1, 1, 1)), 0f));
			
			app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().add(geom);
			int id = BuildingManager.getInstance().addBuilding(1, geom.getLocalTranslation(), geom.getLocalRotation());
			ObjectControl.add(geom, new BuildingAction(id));
			
			((SimpleApplication)app).getRootNode().attachChild(geom);
		}
    }

    @Override
    public void update(float tpf) {
    	if (!isEnabled()) return;
    	
    }
    
    private final static String DATA_FILE = "buildings.xml";
    
    public void saveBuildings() {
    	try {
			File file = new File(System.getProperty("user.home") + "/" + System.getProperty("AppName"));
			file.mkdirs();
			
			XStream xstream = new XStream();
			
			FileOutputStream fos = new FileOutputStream(
					System.getProperty("user.home") + "/" + System.getProperty("AppName") + "/" + DATA_FILE
					);
			xstream.toXML(buildings, fos);
		} catch (IOException ex) {
		    Logger.getLogger(BuildingManager.class.getName()).log(Level.SEVERE, null, ex);
		}
    }
    
    @SuppressWarnings("unchecked")
	private void loadBuildings() {
		File f = new File(System.getProperty("user.home") + "/" + System.getProperty("AppName") + "/" + DATA_FILE);
		 
    	if(f.exists()) {
    		buildings = new HashMap<>();
			XStream xstream = new XStream();
			try {
				buildings = (HashMap<Integer, Building>) xstream.fromXML(f);
			} catch(/*CannotResolveClassException*/ Exception e) {
			} finally {
			}
    	}
    }

    public Building getBuildingInfo(int building_id) {
    	return buildings.get(building_id);
    }
    
	private Building getInfo(int building_id) {
		Building b = new Building();
		b.items.put(19, 20);
		b.items.put(20, 20);
		return b;
	}

	public void build(int selected_building_id, String selected_node_name) {
		if (!buildings.containsKey(selected_building_id)) return;
		
		Building b = buildings.get(selected_building_id);
		
		Set<Integer> keys = b.items.keySet();
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			int item_id = i.next();
			int count = b.items.get(item_id);
			if (Player.getInstance().getInventory().hasItem(item_id)) {
				if (Player.getInstance().getInventory().hasItem(item_id, count)) {
					Player.getInstance().getInventory().removeItem(item_id, count);
				} else {
					count = Player.getInstance().getInventory().getItemCount(item_id);
					Player.getInstance().getInventory().removeItem(item_id, count);
				}
				b.addMaterial(item_id, count);
				return;
			}
		}
		System.out.println("neuztenka resursu!!");
	}
}
