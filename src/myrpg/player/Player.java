package myrpg.player;

import myrpg.gui.ProgressBar;
import myrpg.gui.ProgressBar.ProgressBarEvent;
import myrpg.gui.Stats;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.math.Vector3f;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import info.mindes.DataProviders.DataProvider;
import info.mindes.DataProviders.RecipesData.Recipe;

public class Player implements ProgressBarEvent {
	
	private static Player instance = null;
	
	private int health = 100;
	private int maxHealth = 100;
	private int water = 100;
	private int maxWater = 100;
	private int food = 100;
	private int maxFood = 100;
	private float x = 0;
	private float y = 0;
	
	
	private Inventory inventory = new Inventory();
	
	@XStreamOmitField
	private Stats gui = null;
	
	private Map<Integer, Integer> skills = new HashMap<>();
	
	public static Player getInstance() {
		if(instance == null) {
			instance = new Player();
		}
		return instance;
	}
	
	public Map<Integer, Integer> getSkills() {
		return skills;
	}
	
	public void setStats(Stats stats) {
		this.gui = stats;
		gui.setMaxHealth(maxHealth);
		gui.setHealth(health);
		gui.setMaxFood(maxFood);
		gui.setFood(food);
		gui.setMaxWater(maxWater);
		gui.setWater(water);
	}
	
	public void save() {
		try {
			inventory.setGui(null);
			gui = null;
			recipe = null;
			
			File file = new File(System.getProperty("user.home") + "/" + System.getProperty("AppName"));
			file.mkdirs();
			
			XStream xstream = new XStream();
			String xml = xstream.toXML(Player.getInstance());
			Files.write(Paths.get(System.getProperty("user.home") + "/" + System.getProperty("AppName") + "/player"), xml.getBytes());
		} catch (IOException ex) {
		    Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void load() {
		File file = new File(System.getProperty("user.home") + "/" + System.getProperty("AppName") + "/player");
		if (file.exists()) {
			XStream xstream = new XStream();
			try {
				Player.instance = (Player) xstream.fromXML(file);
			} catch(/*CannotResolveClassException*/ Exception e) {
			} finally {
				setDefaultPlayer();
			}
		} else {
			setDefaultPlayer();
		}
	}
	
	
	private void setDefaultPlayer(){
		health = 100;
		water = 100;
		food = 100;
		maxHealth = 100;
		maxWater = 100;
		maxFood = 100;
		x = 0;
		y = 0;
		skills = new HashMap<>();
		skills.put(1, 2);
	}


	public Inventory getInventory() {
		return inventory;
	}


	public int getActiveItem() {
		return 0;
	}
	
	private float time = 0;
	public void update(float tpf, Vector3f pos) {
		x = pos.getX();
		y = pos.getZ();
		if (health <= 0 || food <=0 || water <= 0) {
			System.out.println("u r dead!!1");
			setDefaultPlayer(); // should do something
		}
		if (time > 10 ) { // update every 10 seconds, not every frame
			time -= 10;
			update_stats();
		}
		time += tpf;
	}
	
	private void update_stats() {
		//if (false) {
		if (food>50 && water>50 && health<maxHealth) { //healing
			gui.setHealth(++health);
		}
		food--;
		gui.setFood(food);
		water -= 3;
		gui.setWater(water);
	}
	
	public Vector3f getLastPos() {
		return new Vector3f(x, 10, y); 
	}
	
	@XStreamOmitField
	private Recipe recipe = null;
	public void craft(int id) {
		recipe = DataProvider.recipes.get(id);
		Set<Integer> keys = recipe.req.keySet();
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			int key = i.next();
			if (!inventory.hasItem(key, recipe.req.get(key))) {
				System.out.println("not enought of items");
				return;
			}
		}
		
		ProgressBar.getInstance().startEvent(this, recipe.delay);
	}

	@Override
	public void complete() {
		if (null==recipe) return;
		
		Set<Integer> keys = recipe.req.keySet();
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			int key = i.next();
			inventory.removeItem(key, recipe.req.get(key));
		}
		
		keys = recipe.res.keySet();
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			int key = i.next();
			if (recipe.res.get(key)>0)
				inventory.addItem(key, recipe.res.get(key));
		}
		recipe = null;
	}

	@Override
	public void cancel() {
		System.out.println("craft cancesl");
		recipe = null;
	}
	
}
