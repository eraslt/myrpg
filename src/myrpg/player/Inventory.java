package myrpg.player;

import myrpg.gui.Inventory.onDragEndEvent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class Inventory {

	private Map<Integer, Integer> items = new HashMap<>();
	
	@XStreamOmitField
	private myrpg.gui.Inventory gui = null;
	
	public void setGui(myrpg.gui.Inventory gui) {
		this.gui = gui;
		if (null == gui) return;
		this.gui.setEvent(new onDragEndEvent() {
			
			@Override
			public void fire(int item_id) {
				removeItem(item_id, 1);
			}
		});
		refreshGui();
	}
	
	public void refreshGui() {
		if (null == gui) return;
		//gui.getWindow().removeAllChildren();
		for (Iterator<Integer> i = items.keySet().iterator(); i.hasNext();) {
			int key = i.next();
			gui.updateOnWindow(key, items.get(key));
		}
	}
	
	public void addItem(int id, int cnt) {
		if (items.containsKey(id))
			cnt += items.get(id);
		items.put(id, cnt);
		if (null != gui)
			gui.updateOnWindow(id, cnt);
	}
	
    public void removeItem(int id, int cnt) {
    	int tot = 0;
    	if (items.containsKey(id))
			tot = items.get(id);
    	if (tot>cnt) {
    		items.put(id, tot-cnt);
    		if (null != gui)
    			gui.updateOnWindow(id, tot-cnt);
    	} else {
    		items.remove(id);
    		if (null != gui)
    			gui.removeFromWindow(id);
    	}
    }
    
    public int getItemCount(int id){
		return  (items.get(id) != null)?(items.get(id)):(0);
	}
	
	public boolean hasItem(int id){
		if (items.get(id) != null)
			return true;
		return false;
	}
	
	public boolean hasItem(int id, int count){
		if (items.get(id) != null)
			if (items.get(id) >= count)
				return true;
		return false;
	}
	
	public void dropItem(int id,int cnt) {
		/*Vector3f pos = Game.getRoot().getCamera().getLocation().clone();
		Vector3f dir = Game.getRoot().getCamera().getDirection();
		pos.addLocal(dir.mult(10));
		
		CollisionResults results = new CollisionResults();
		pos.y = 400;
		Ray ray = new Ray(pos, new Vector3f(0, -1, 0));
		TerrainProvider.getInstance().getTerrain().collideWith(ray, results);
			
		if(results.size() > 0){
			Vector3f cpos = results.getClosestCollision().getContactPoint();
			GridLoader.getInstance().addItem(id, cnt, cpos);
		}
		
		removeItem(id, 1);*/
	}
}
