package networking.messages;

import java.util.ArrayList;
import java.util.Map;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

@SuppressWarnings("rawtypes")
public class EntityMessages {
	
	
	private static ArrayList<Class> messages = new ArrayList<>();

    static {
    	messages.add(EntityState.class);
    	messages.add(IdentifyEntityMessage.class);
    	messages.add(UpdateEntityMessage.class);
    	messages.add(MoveEntityMessage.class);
    	messages.add(AddEntityMessage.class);
    	messages.add(removeEntityMessage.class);
    }
    
    public static ArrayList<Class> GetMessages() {
    	return EntityMessages.messages;
    }
    
    public static class EntityState {
    	public Vector3f pos;
    	public Quaternion rot;
    	
    	public EntityState() {
		}
    	
    	public EntityState(Vector3f pos, Quaternion rot) {
    		this.pos = pos;
    		this.rot = rot;
    	}
    }
    
    /* server 2 client */
    public static class IdentifyEntityMessage {
    	public int id;
    	
    	public IdentifyEntityMessage() {
		}
    	
    	public IdentifyEntityMessage(int id) {
    		this.id = id;
		}
    }
    
    /* client 2 server */
    public static class UpdateEntityMessage {
    	public EntityState state;
    	
    	public UpdateEntityMessage() {
		}
    	
    	public UpdateEntityMessage(EntityState state) {
    		this.state = state;
		}
    }
    
    /* server 2 client */
    public static class MoveEntityMessage {
    	public Map<Integer, EntityState> entities;
    	
    	public MoveEntityMessage() {
		}
    	
    	public MoveEntityMessage(Map<Integer, EntityState> entities) {
    		this.entities = entities;
		}
    }
    
    public static class AddEntityMessage {

        public AddEntityMessage() {
        }
    }
    
    public static class removeEntityMessage {
    	public int id;
    	
        public removeEntityMessage() {
        }
        
        public removeEntityMessage(int id) {
        	this.id = id;
        }
    }
}
