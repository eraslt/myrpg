package networking.messages;

import java.util.ArrayList;

/**
 * Created by Eras on 2014.02.26
 */
@SuppressWarnings("rawtypes")
public class GuiMessages {
	
	private static ArrayList<Class> messages = new ArrayList<>();

    static {
    	messages.add(ChatMessage.class);
    }
    
    public static ArrayList<Class> GetMessages() {
    	return GuiMessages.messages;
    }
    
    public static class ChatMessage {
        public int type;
        public String msg;

        public ChatMessage() {
        }
        
        public ChatMessage(String msg) {
            this.msg = msg;
            this.type = 0;
        }
        
        public ChatMessage(String msg, int type) {
            this.msg = msg;
            this.type = type;
        }
        
        @Override
        public String toString() {
        	return "ChatMessage{" +
                    "msg='" + msg + '\'' +
                    "type='" + type + '\'' +
                    '}';
        }
    }
}
