package networking.messages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

@SuppressWarnings("rawtypes")
public class CommonMessages {
	
	
	private static ArrayList<Class> messages = new ArrayList<>();

    static {
    	messages.add(Map.class);
    	messages.add(HashMap.class);
    	messages.add(ConcurrentHashMap.class);
    	messages.add(Vector3f.class);
    	messages.add(Quaternion.class);
    	messages.add(ErrorMessage.class);
    	messages.add(NPCData.class);
    	messages.add(MobTest.class);
    }
    
    public static ArrayList<Class> GetMessages() {
    	return CommonMessages.messages;
    }
    
    public static class ErrorMessage {
        public String error;
        public boolean important = false;

        public ErrorMessage() {
        }

        public ErrorMessage(String error) {
            this.error = error;
        }
        
        public ErrorMessage(String error, boolean important) {
            this.error = error;
            this.important = important;
        }

        @Override
        public String toString() {
            return "ErrorMessage{" +
                    "error='" + error + '\'' +
                    ", important='" + important + '\'' +
                    '}';
        }
    }
    
    public static class NPCData {
    	public Vector3f position;
    	public Vector3f target;
    	public Integer action = 0;
    	public float speed = 1;
    	
    	public NPCData(){
    	}
    	
    	public NPCData(Vector3f position, Vector3f target){
    		this.position = position;
    		this.target = target;
    	}
    	
    	public NPCData(Vector3f position, Vector3f target, float speed){
    		this.position = position;
    		this.target = target;
    		this.speed = speed;
    	}
    	
    	public NPCData(Vector3f position, Vector3f target, int action){
    		this.position = position;
    		this.target = target;
    		this.action = action;
    	}
    	
    	public NPCData(Vector3f position, Vector3f target, int action, float speed){
    		this.position = position;
    		this.target = target;
    		this.action = action;
    		this.speed = speed;
    	}
    }
    
    public static class MobTest {
    	public Map<Integer, NPCData> data;

        public MobTest() {
        }

        public MobTest(Map<Integer, NPCData> data) {
            this.data = data;
        }
    }
}
