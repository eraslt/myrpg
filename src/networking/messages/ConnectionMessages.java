package networking.messages;

import java.util.ArrayList;

/**
 * Created by Eras on 2014.02.26
 */
@SuppressWarnings("rawtypes")
public class ConnectionMessages {
	
	private static ArrayList<Class> messages = new ArrayList<>();

    static {
    	messages.add(LoginMessage.class);
    	messages.add(AuthorizedMessage.class);
    	messages.add(BadCredetnialsMessage.class);
    	messages.add(DisconnectedMessage.class);
    }
    
    public static ArrayList<Class> GetMessages() {
    	return ConnectionMessages.messages;
    }
    
    public static class LoginMessage {
        public String username;
        public String password;

        public LoginMessage() {
        }
        
        public LoginMessage(String u, String p) {
            this.username = u;
            this.password = p;
        }
        
        @Override
        public String toString() {
        	return "LoginMessage{" +
                    "username='" + username + '\'' +
                    "password='" + password + '\'' +
                    '}';
        }
    }
    
    public static class AuthorizedMessage {
    	public int id;
        public int conn;
    	
    	public AuthorizedMessage() {
        }
    	
    	public AuthorizedMessage(int id, int conn) {
    		this.id = id;
    		this.conn = conn;
        }
        
        @Override
        public String toString() {
        	return "AuthorizedMessage{}";
        }
    }
    
    public static class BadCredetnialsMessage {
        
    	public BadCredetnialsMessage() {
        }
        
        @Override
        public String toString() {
        	return "BadCredetnialsMessage{}";
        }
    }
    
    public static class DisconnectedMessage {
    	
    	public String reason;
        
    	public DisconnectedMessage() {
        }
        
    	public DisconnectedMessage(String reason) {
    		this.reason = reason;
        }
    	
        @Override
        public String toString() {
        	return "DisconnectedMessage{reason='" + reason + "'}";
        }
    }
}
