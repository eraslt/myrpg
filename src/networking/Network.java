package networking;

import java.util.ArrayList;

@SuppressWarnings("rawtypes")
public abstract class Network {
    ArrayList<NetworkListener> networkListeners;

    protected Network() {
        networkListeners = new ArrayList<>();
    }

    public void addListener(NetworkListener networkListener) {
        networkListeners.add(networkListener);
    }
    
    public NetworkListener getListener(Class listener) {
    	for(NetworkListener item: networkListeners){
    		if (item.getClass().equals(listener)) 
    			return item;
    	}
        return null;
    }

    public void removeListener(Class listener) {
    	for(NetworkListener item: networkListeners){
    		if (item.getClass().equals(listener))
    			networkListeners.remove(item);
    	}
    }

    public abstract void registerMessage(Class message);

    public void registerMessages(ArrayList<Class> messages) {
        for (Class message : messages) {
            registerMessage(message);
        }
    }

    public void onConnect(int id) {
        for (NetworkListener listener : networkListeners) {
            if (listener.onConnect(id)) {
                return;
            }
        }
    }

    public void onDisconnect(int id) {
        for (NetworkListener listener : networkListeners) {
            if (listener.onDisconnect(id)) {
                return;
            }
        }
    }

    public void onMessageReceived(int id, Object message) {
        for (NetworkListener listener : networkListeners) {
            if (listener.onMessageReceived(id, message)) {
                return;
            }
        }
    }

}
