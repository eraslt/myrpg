package networking.client;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;

public class NetworkControlled implements Control {

	class State {
	    public Vector3f position = Vector3f.ZERO;
	    public Quaternion rotation = Quaternion.IDENTITY;
	    
	    public State(Vector3f l, Quaternion r) {
	    	position = l;
			rotation = r;
		}
	}
	
	private List<State> intStateList = new LinkedList<>();
	
	private Vector3f currentTranslation = Vector3f.ZERO;
	private Vector3f targetTranslation = Vector3f.ZERO;
	private Vector3f startTranslation = Vector3f.ZERO;
	 
	private Quaternion currentRotation = Quaternion.IDENTITY;
	private Quaternion targetRotation = Quaternion.IDENTITY;
	private Quaternion startRotation = Quaternion.IDENTITY;
    
    private float currentScale = 1.0f;
    public float interpFactor = 10.0f;
	
	private Spatial spatial;
	
	private boolean remove = false;
    
    public NetworkControlled() {
    }

    public void remove() {
    	remove = true;
    }
    
    public void addState(Vector3f l, Quaternion r) {
        intStateList.add(new State(l, r));
    }  
    
    @Override
    public void setSpatial(Spatial spatial) {
        this.spatial = spatial;
        if (spatial == null) {
            return;
        }
    }

    @Override
    public void update(float tpf) {
    	if (remove) {
    		spatial.removeFromParent();
    		spatial.removeControl(NetworkControlled.class);
    		return;
    	}
    	if (/*currentScale > 0.95f &&*/ intStateList.size() > 0) {
    		State nextTargetState = intStateList.remove(0);
    		// Set new target position
    		startTranslation = currentTranslation;
    		targetTranslation = nextTargetState.position;
    		// Set new target rotation
    		startRotation = currentRotation;
    		targetRotation = nextTargetState.rotation;
    		// reset the scale
    		currentScale = 0.0f;
    	}

    	// Update the scale
    	currentScale += tpf * interpFactor;
    	// Interpolate start position to target position
    	currentTranslation = FastMath.interpolateLinear(
    			currentScale,
    			startTranslation,
    			targetTranslation);
    		 
    	// Interpolate start rotation to target rotation
    	currentRotation.slerp(startRotation, targetRotation, currentScale);
    	spatial.setLocalTranslation(currentTranslation);
    	spatial.setLocalRotation(currentRotation);
    }
    
	@Override
	public Control cloneForSpatial(Spatial arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void render(RenderManager arg0, ViewPort arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void read(JmeImporter arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void write(JmeExporter arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
}