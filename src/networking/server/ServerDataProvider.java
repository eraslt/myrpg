package networking.server;

import info.mindes.DataProviders.*;
import info.mindes.DataProviders.mysql.*;

public class ServerDataProvider {

    public static ItemData item = new MysqlItemData();
    public static SkillsData skills = new MysqlSkillsData();
    public static PlayerData player = new MysqlPlayerData();
    public static ObjectData object = new MysqlObjectData();
    public static MysqlUserData user = new MysqlUserData();
    
}
