package networking.server;

import java.util.Map;

import networking.kryonet.KryoNetworkServer;
import networking.messages.ConnectionMessages;
import networking.messages.EntityMessages;
import networking.messages.GuiMessages;
import networking.messages.CommonMessages;
import networking.server.listeners.AuthListener;
import networking.server.listeners.ChatListener;
import networking.server.listeners.EntityListener;
//import server.listeners.InventoryListener;
import networking.server.listeners.Mobs;


/*
 * TODO:
 * add messages to taskpool
 * run processors as separate threads
 * http://www.javacodegeeks.com/2013/01/java-thread-pool-example-using-executors-and-threadpoolexecutor.html
 * http://math.hws.edu/javanotes/c12/s4.html
 */
public class Server {

	static final String host = "localhost";
	static final int tcp_port = 1114;
	static final int udp_port = 1115;

	KryoNetworkServer networkServer;

	public static void main(String[] args) {
		Server s = new Server();
		s.startServer();
	}

	 public Server() {
	 }

	 private void startServer() {
        networkServer = new KryoNetworkServer();
        networkServer.listen(Server.tcp_port, Server.udp_port);

        networkServer.registerMessages(CommonMessages.GetMessages());
        networkServer.registerMessages(ConnectionMessages.GetMessages());
        networkServer.registerMessages(GuiMessages.GetMessages());
        networkServer.registerMessages(EntityMessages.GetMessages());

        networkServer.addListener(new ChatListener(this));
        networkServer.addListener(new AuthListener(this));
        networkServer.addListener(new EntityListener(this));
        
//        new Mobs(this);//cia mobu testavimui, gal veliau sita nes labai neasku viskas

//        networkServer.addListener(inv = new InventoryListener(this));
    }

	public void clientAuthorized(int id, int player_id) {
		Map<String, String> player_data = ServerDataProvider.player.getPlayerData(player_id);
		((ChatListener)networkServer.getListener(ChatListener.class)).addChater(id, player_data.get("name"));
//		((InventoryListener)networkServer.getListener(InventoryListener.class)).add(id, player_id);
		((EntityListener)networkServer.getListener(EntityListener.class)).add(id, player_id);
	}

	public void sendUDP(int id, Object udpMsg) {
		networkServer.sendMessage(id, udpMsg, false);
	}

	public void sendTCP(int id, Object tcpMsg) {
		networkServer.sendMessage(id, tcpMsg, true);
	}
	
	public KryoNetworkServer getServer() {
		return networkServer;
	}
}
