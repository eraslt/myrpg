package networking.server.listeners;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;


import networking.server.Server;
import networking.server.ServerDataProvider;

import networking.NetworkListener;
import networking.messages.EntityMessages.*;

public class EntityListener implements NetworkListener {
	
	class Updater extends TimerTask {

        @Override
        public void run() {
        	server.getServer().Broadcast(new MoveEntityMessage(entities), false);
        }
    }
	
    final Map<Integer, EntityState> entities = new ConcurrentHashMap<>();
    Server server;
    
    public EntityListener(Server server) {
    	this.server = server;
    	new Timer().schedule(new Updater(), 0, 1000 / 25);
    }
    
    public void add(int conn, int player_id) {
    	entities.put(conn, new EntityState(Vector3f.ZERO, Quaternion.ZERO));
    }
    
    public Map<Integer, EntityState> getEntities() {
    	return entities;
    }
    
	@Override
	public boolean onConnect(int id) {
		return false;
	}
	
	@Override
	public boolean onDisconnect(int id) {
		if (entities.containsKey(id)) {
			Vector3f pos = entities.get(id).pos;
			entities.remove(id);
			ServerDataProvider.player.savePosition(pos.x, pos.y, pos.y);
		}
		server.getServer().Broadcast(new removeEntityMessage(id), true);
		return false;
	}
	
	@Override
	public boolean onMessageReceived(int id, Object message) {
		if (message instanceof UpdateEntityMessage) {
			entities.put(id, ((UpdateEntityMessage)message).state);
		}
		return false;
	}
}
