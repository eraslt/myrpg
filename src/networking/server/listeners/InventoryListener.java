package networking.server.listeners;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import info.mindes.DataProviders.PlayerData.Item;
//import game.gui.messages.InventoryMessage;
import networking.NetworkListener;
//import networking.messages.CommonMessages.ErrorMessage;
import networking.server.Server;
import networking.server.ServerDataProvider;

public class InventoryListener implements NetworkListener  {

	Server server;

	class Inventory {
		int player_id;
		Map<Integer, Item> items;	
	}
	
	Map<Integer, Inventory> mapOfInv = new ConcurrentHashMap<>();

	public InventoryListener(Server server) {
		this.server = server;
	}
	
	public void add(int conn_id, int player_id) {
		Inventory inv = new Inventory();
		inv.player_id = player_id;
		inv.items = ServerDataProvider.player.getInventory(player_id);
		mapOfInv.put(conn_id, inv);
	}
	
	public void remove(int conn_id) {
		if (mapOfInv.containsKey(conn_id)) {
			Inventory inv = mapOfInv.get(conn_id);
			ServerDataProvider.player.saveItems(inv.player_id, inv.items);
			mapOfInv.remove(conn_id);
		}
	}
	
	@Override
	public boolean onConnect(int conn) {
		return false;
	}

	@Override
	public boolean onDisconnect(int conn) {
		remove(conn);
		return false;
	}

	@Override
	public boolean onMessageReceived(int conn, Object message) {
		/*if (!(message instanceof InventoryMessage)) return false;
		if (!mapOfInv.containsKey(client.getId())) {
			server.sendTCP(client, new ErrorMessage("Inventory not found!!"));
			return false;
		}
		Inventory inv = mapOfInv.get(conn); 
		InventoryMessage msg = ((InventoryMessage)message);
		
		int slot, cnt, item_id, new_slot;
		
		switch (msg.getMsgAction()) {
		case Drop:
			slot = msg.getSlotOriginal();
			cnt = msg.getItemQuantity();
			if (!inv.items.containsKey(slot)) {
				System.out.println("send inv message couse drop error");
				break;
			}
			if (inv.items.get(slot).cnt<cnt) {
				Item item = inv.items.get(slot);
				item.cnt = item.cnt-cnt;
				inv.items.put(slot, item);
			} else {
				inv.items.remove(slot);
			}
			break;
		case Consume:
			slot = msg.getSlotOriginal();
			if (!inv.items.containsKey(slot)) {
				System.out.println("send inv message couse consume error");
				break;
			}
//			inv.items.put(item_id, inv.items.get(item_id)-1);
			System.out.println("consume unimplemented");
			break;
        case AddToSlot:
        	slot = msg.getSlotMovedTo();
			cnt = msg.getItemQuantity();
			item_id = msg.getItemId();
        	inv.items.put(slot, new Item(item_id, cnt));
            break;
        case MoveToSlot:
        	slot = msg.getSlotOriginal();
        	if (!inv.items.containsKey(slot)) {
				System.out.println("send inv message couse move error");
				break;
			}
        	new_slot = msg.getSlotMovedTo();
        	cnt = msg.getItemQuantity();
        	Item item = inv.items.get(slot);
        	
        	if (inv.items.containsKey(new_slot)) {
        		if(item.cnt<cnt) {
        			System.out.println("send inv message couse move error: cannot split to cell where there is item");
        		} else {
        			inv.items.put(slot, inv.items.get(new_slot));
        			inv.items.put(new_slot, item);
        		}
        	} else {
        		if (item.cnt<cnt) {
        			inv.items.remove(slot);
        			inv.items.put(new_slot, item);
        		} else {
        			inv.items.put(slot, new Item(item.id, item.cnt - cnt));
        			inv.items.put(new_slot, new Item(item.id, cnt));
        		}
        	}
        	break;
		case Activate:
			slot = msg.getSlotOriginal();
			if (!inv.items.containsKey(slot)) {
				System.out.println("send inv message couse move error");
				break;
			}
			System.out.println("Activate unimplemented");
			break;
		default:
			break;
		}*/
		return false;
	}
}
