package networking.server.listeners;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import networking.messages.CommonMessages.MobTest;
import networking.messages.CommonMessages.NPCData;
import networking.server.Server;
import tests.fsm.Mob;
import tests.fsm.RunAwayMob;


public class Mobs {

	class Updater extends TimerTask {
        @Override
        public void run() {
        	Map<Integer, NPCData> data = new HashMap<>();
        	
        	Set<Integer> keys = mobs.keySet();
        	int cnt = 0;
        	for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
        		int id = i.next();
        		Mob m = mobs.get(id);
        			m.update(0);
//        			System.out.println("state mobpos:"+m.position.toString());
        		if (m.changed) {
        			
        			data.put(id, new NPCData(m.getPosition(), m.getTarget(), m.getAction(), m.getSpeed()));
        			m.changed = false;
        			cnt++;
        		}
        		if (cnt>=10) {
        			server.getServer().Broadcast(new MobTest(data), false);
        			data.clear();
        			cnt = 0;
        		}
        	}
        	if (!data.isEmpty()) {
        		server.getServer().Broadcast(new MobTest(data), false);
        	}
        }
    }
	
	class Synchronizer extends TimerTask {
        @Override
        public void run() {
        	Map<Integer, NPCData> data = new HashMap<>();
        	
        	Set<Integer> keys = mobs.keySet();
        	int cnt = 0;
        	for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
        		int id = i.next();
        		Mob m = mobs.get(id);
//        		System.out.println("sinch mobpos:"+m.position.toString());
        		data.put(id, new NPCData(m.getPosition(), m.getTarget(), m.getAction(), m.getSpeed()));
        		cnt++;
        		if (cnt>=10) {
        			server.getServer().Broadcast(new MobTest(data), false);
        			data.clear();
        			cnt = 0;
        		}
        	}
        	if (!data.isEmpty()) {
        		server.getServer().Broadcast(new MobTest(data), false);
        	}
        }
    }
	
    final Map<Integer, Mob> mobs = new ConcurrentHashMap<>();
    Server server;
    
    public static long updater_time = 1000 / 20;
    public static float move_offset = 0.05f;
    
    public Mobs(Server server) {
    	this.server = server;
    	new Timer().schedule(new Updater(), 0, Mobs.updater_time);
    	new Timer().schedule(new Synchronizer(), 0, 10000);
    	/*for (int i = 0; i<3; i++ ) {
    		mobs.put(i, new Mob());
    	}
    	
    	for (int i = 3; i<6; i++ ) {
    		mobs.put(i, new RunAwayMob(((EntityListener)server.getServer().getListener(EntityListener.class)).getEntities()));
    	}*/
//    	mobs.put(0, new RunAwayMob(((EntityListener)server.getServer().getListener(EntityListener.class)).getEntities()));
    }
}
