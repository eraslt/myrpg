package networking.server.listeners;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import networking.NetworkListener;
import networking.messages.GuiMessages.ChatMessage;
import networking.server.Server;

public class ChatListener implements NetworkListener {

	Server server;

	private Map<Integer, String> conn2name = new ConcurrentHashMap<>(); 

	public ChatListener(Server server) {
		this.server = server;
	}
	
	public void addChater(int conn_id, String name) {
		conn2name.put(conn_id, name);
	}
	
	public void removeChater(int conn_id) {
		if (conn2name.containsKey(conn_id)) {
			conn2name.remove(conn_id);
		}
	}
	
	@Override
	public boolean onConnect(int conn) {
		return false;
	}

	@Override
	public boolean onDisconnect(int conn) {
		removeChater(conn);
		return false;
	}

	@Override
	public boolean onMessageReceived(int conn, Object message) {
		if (!(message instanceof ChatMessage)) return false;
		String msg = ((ChatMessage)message).msg;
		String name = conn2name.get(conn);
		((ChatMessage)message).msg = name + ": " + msg;
		broadcast(message);
		return false;
	}
	
	private void broadcast(Object msg) {
        Set<Integer> keys = conn2name.keySet();
        for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
            server.sendTCP(i.next(), msg);
        }
    }

}
