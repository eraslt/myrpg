package networking.server.listeners;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import networking.NetworkListener;
import networking.messages.CommonMessages.*;
import networking.messages.ConnectionMessages.*;
import networking.server.Server;
import networking.server.ServerDataProvider;

public class AuthListener implements NetworkListener  {

	Server server;
	
	private Map<Integer, Integer> sessions = new ConcurrentHashMap<>();
	
	public AuthListener(Server server) {
		this.server = server;
	}
	
	@Override
	public boolean onConnect(int id) {
		return false;
	}

	@Override
	public boolean onDisconnect(int id) {
   		sessions.remove(id);
		return false;
	}

	@Override
	public boolean onMessageReceived(int id, Object message) {
		if (message instanceof LoginMessage) {
        	proccessAuth(id, (LoginMessage) message);
        }
		return false;
	}

	
	private void proccessAuth(int conn, LoginMessage message) {
    	int user_id = ServerDataProvider.user.getUserID(message.username, message.password);
    	if (user_id<=0) {
    		server.sendTCP(conn, new BadCredetnialsMessage());
    	} else {
    		if (sessions.containsValue(user_id)) {
    			/*
    			 * TODO: do something
    			 */
    			server.sendTCP(conn, new ErrorMessage("User already loged in, what to do?"));
    		} else {
    			int player_id = ServerDataProvider.user.getActivePlayer(user_id);
    			if (player_id>0) {
    				
    				sessions.put(conn, user_id);
        			
	    			server.sendTCP(conn, new AuthorizedMessage(player_id, conn));
	    			server.clientAuthorized(conn, player_id);
    			} else {
    				server.sendTCP(conn, new ErrorMessage("u dont have any active players :("));
    			}
    		}
    	}
    }
	
}
