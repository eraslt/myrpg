package networking.kryonet;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import networking.Network;

import java.io.IOException;

@SuppressWarnings("rawtypes")
public class KryoNetworkClient extends Network {
    Client client;

    public KryoNetworkClient() {
        client = new Client();
        client.start();
        client.addListener(new Listener() {
            @Override
            public void connected(Connection connection) {
                super.connected(connection);
                onConnect(connection.getID());
            }

            @Override
            public void disconnected(Connection connection) {
                super.disconnected(connection);
                onDisconnect(connection.getID());
            }

            @Override
            public void received(Connection connection, Object o) {
                super.received(connection, o);
                onMessageReceived(connection.getID(), o);
            }

            @Override
            public void idle(Connection connection) {
                super.idle(connection);
            }
        });
    }

    public void connect(String host, int tcp_port, int udp_port) {
        try {
            client.connect(5000, host, tcp_port, udp_port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Object message, boolean reliable) {
        if (reliable) {
        	client.sendTCP(message);
        } else {
        	client.sendUDP(message);
        }
    }

	@Override
    public void registerMessage(Class message) {
        client.getKryo().register(message);
    }
}
