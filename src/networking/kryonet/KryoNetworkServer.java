package networking.kryonet;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import java.io.IOException;

import networking.Network;

@SuppressWarnings("rawtypes")
public class KryoNetworkServer extends Network {
    Server server;

	@Override
    public void registerMessage(Class message) {
        server.getKryo().register(message);
    }

    public void listen(int tcp_port, int udp_port) {
        server = new Server();
        server.start();
        try {
            server.bind(tcp_port, udp_port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.addListener(new Listener() {
            @Override
            public void connected(Connection connection) {
                super.connected(connection);
                onConnect(connection.getID());
            }

            @Override
            public void disconnected(Connection connection) {
                super.disconnected(connection);
                onDisconnect(connection.getID());
            }

            @Override
            public void received(Connection connection, Object o) {
                super.received(connection, o);
                onMessageReceived(connection.getID(), o);
            }

            @Override
            public void idle(Connection connection) {
                super.idle(connection);
            }
        });
    }

    public void sendMessage(int id, Object message, boolean reliable) {
    	if (reliable) {
    		server.sendToTCP(id, message);
    	} else {
    		server.sendToUDP(id, message);
    	}
    }
    
    public void killMe() {
        if (server != null) {
            server.stop();
        }
    }

	public void Broadcast(Object message, boolean reliable) {
		if (reliable) {
    		server.sendToAllTCP(message);
    	} else {
    		server.sendToAllUDP(message);
    	}
	}

	public void BroadcastExcept(int id, Object message, boolean reliable) {
		if (reliable) {
    		server.sendToAllExceptTCP(id, message);
    	} else {
    		server.sendToAllExceptUDP(id, message);
    	}		
	}
}
