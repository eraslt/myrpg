package networking;

public interface NetworkListener {
    public boolean onConnect(int id);

    public boolean onDisconnect(int id);

    public boolean onMessageReceived(int id, Object message);
}
