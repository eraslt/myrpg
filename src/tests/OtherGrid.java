package tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import info.mindes.SimpleGrid.EventHandler;
import info.mindes.SimpleGrid.Position;
import info.mindes.SimpleGrid.SimpleGrid;
import info.mindes.SimpleGrid.SimpleGridUtils;

@SuppressWarnings("rawtypes")
public class OtherGrid extends SimpleGrid {

    public static void main(String[] args) {

//        OtherGrid grid = new OtherGrid(10, 1, 3);
        OtherGrid grid = new OtherGrid(1024, 64, 3);
        grid.setCaching(false);
        grid.addEvent(new EventHandler() {

            @Override
            public void load(Position position) {
                System.out.println("load:"+position.toString());
            }

            @Override
            public void update(Position position) {
                System.out.println("update:"+position.toString());
            }

            @Override
            public void unload(Position position) {
                System.out.println("unload:"+position.toString());
            }
        });

        System.out.println("update 0, 0");
        System.out.println("*******************************************");
        grid.update(0, 0);
        System.out.println("*******************************************");
        System.out.println();
        System.out.println("update 500, 500");
        System.out.println("*******************************************");
        grid.update(500, 500);
        System.out.println("*******************************************");
        System.out.println();
        System.out.println("update 800, 500");
        System.out.println("*******************************************");
        grid.update(800, 500);
        System.out.println("*******************************************");
    }
    
    
    private Position current = new Position(0, 0);
    private boolean valid = false;
    private int size = 1000;
    private int unit_size = 10;
    private int units = 10;
    private int unload_dist = 3;

    private boolean caching = true;
    private int cache_delay = 5000;

    private Map<Position, Long> cached = new HashMap<>();

    private Position[][] map;

    private List<EventHandler> eventHandlers = new ArrayList<>();

    public OtherGrid(int unload_dist) {
        super(unload_dist);
        init(1024, 64, unload_dist);
    }

    public OtherGrid(int size, int unit_size, int unload_dist) {
        super(unload_dist);
        init(size, unit_size, unload_dist);
    }
    
    public void init(int size, int unit_size, int unload_dist) {
        this.size = size;
        this.unit_size = unit_size;
        this.unload_dist = unload_dist;
        this.units = (int)Math.ceil(this.size/this.unit_size);
        this.map = new Position[this.units][this.units];
    }

    public void print() {
        for (int y = 0; y < this.units; y++) {
            for (int x = 0; x < this.units; x++) {
                System.out.println(map[y][x]);
            }
        }
    }

    public void addEvent(EventHandler eventHandler) {
        eventHandlers.add(eventHandler);
    }

    public void addEvents(EventHandler... events) {
        for (int i = 0; i < events.length; i++) {
            eventHandlers.add(events[i]);
        }
    }

    public void removeEvent(EventHandler eventHandler) {
        if (eventHandlers.contains(eventHandler)) {
            eventHandlers.remove(eventHandler);
        }
    }

    public Position getPosition() {
        return current;
    }

    public void setPosition(double x, double y) {
        setPosition(new Position((int)x, (int)y));
    }

    public void setPosition(Position position) {
        this.current = position;
        valid = false;
    }


    public void setCacheDelay(int cache_delay) {
        this.cache_delay = cache_delay;
    }

    public void setCaching(boolean caching) {
        this.caching = caching;
    }

    public void update(double x, double y) {
        Position p = SimpleGridUtils.getTileByPos(x, y, unit_size, 0, 0);
        update(p);
    }

    public void update(Position position) {
        if (valid && position.equals(current))
            return; // not moved
        current = position;

        // updating distance
        for (int y = 0; y < units; y++) {
            for (int x = 0; x < units; x++) {
//                cia x ne positicija o cele
                int dist = Math.max(Math.abs(x - current.x), Math.abs(y - current.y));

                if (null==map[y][x]) {
                    if (dist<unload_dist) {
                        Position p = new Position(x, y, dist);
                        map[y][x] = p;
                        // remove from delete list if we are tryinng to load it again
                        if (caching && cached.containsKey(p)) {
                            cached.remove(p);
                        } else {
                            for (EventHandler eh : eventHandlers) {
                                eh.load(p);
                            }
                        }
                    }
                    continue;
                }

                Position p = map[y][x];
                p.dist = dist;

                if (dist >= unload_dist) {
                    if (caching && !cached.containsKey(p)) {
                        cached.put(p, System.currentTimeMillis());
                    } else {
                        for (EventHandler eh : eventHandlers) {
                            eh.unload(p);
                        }
                    }
                    map[y][x] = null;
                    continue;
                }

                for (EventHandler eh : eventHandlers) {
                    eh.update(p);
                }
            }
        }

        // unload expired cached
        if (caching) {
            Iterator it = cached.entrySet().iterator();
            long now = System.currentTimeMillis();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                if ((((long) pair.getValue()) + cache_delay) < now) {
                    Position p = (Position) pair.getKey();
                    for (EventHandler eh : eventHandlers) {
                        eh.unload(p);
                    }
                    map[p.y][p.x] = null;
                    it.remove(); // avoids a ConcurrentModificationException
                }
            }
        }

        valid = true;
    }
}
/*

As the player moves through the world, new instances are added, and instances no longer in range are removed.
Artists author grass density through the landscape material, by outputting density to a Landscape Grass Output node. This allows the grass placement to match the terrain texturing!
Grass instances are procedurally generated by a background task according to the grass density map.
Grass is efficiently rendered using a hierarchical instancing system that implements culling while keeping draw calls to a minimum. LOD transitions are per-pixel and fairly seamless due to Temporal AA.
See the "Landscape Grass Tools" section below for information about how to set this up!


//http://stackoverflow.com/questions/6388887/programming-a-2d-grid-in-java
//    TODO CHECK THIS !!!
//    http://hub.jmonkeyengine.org/t/challenge-nr-1-vegetation-aka-one-million-bushes-challenge/2849/25
//    super
//    http://hub.jmonkeyengine.org/t/threadsafe-quadtree-without-locking/8035


some ideas for you

- use instancing

- use a modulo grid roughly centered on the camera (to load/unload instance data from gpu)

- use a combination of manual and procedural positioning and variation

- use fixed seeds with Random for consistency

- use height map and normals for placing procedural objects

- quadtree/octree - look at grid registration (simpler, faster)


once it is working you will need to use imposters
for really large quantities you can use a hierarchy of imposters
(pixel perfect > approximate > aggregate > surround)

a good imposter trick is to render a normal map also - so the imposters
respond to lighting without being rerendered

imposters are very challenging

*/