package tests;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;

public class MaterialUtils {

	public static void prepareModel2(Node model, AssetManager assetManager) {
		prepareModel2(model, assetManager, 0, 0);
	}
	
	public static void prepareModel2(Node model, AssetManager assetManager, float fadeEnd, float fadeRange) {

    	// Set proper materials
        for (int i = 0; i < model.getChildren().size(); i++) {
            Geometry geom = (Geometry) model.getChild(i);
            Material oldMat = geom.getMaterial();
            if (!"Phong Lighting".equals(oldMat.getMaterialDef().getName())) {
                throw new RuntimeException("Model : " + model.getName() + " does not "
                        + "use the Lighting.j3md material definition.");
            }
            Material newMat;
            if (geom.<Boolean>getUserData("Foliage") != null) {
                newMat = new MaterialSP(assetManager, "biomonkey/matdefs/FoliageBase.j3md");
//                newMat = new Material(assetManager, "biomonkey/matdefs/FoliageBase.j3md");

                // DiffuseMap
                if (oldMat.getTextureParam("DiffuseMap") != null) {
                    newMat.setTexture("DiffuseMap", oldMat.getTextureParam("DiffuseMap").getTextureValue());
                }
                // Normal map
                if (oldMat.getTextureParam("NormalMap") != null) {
                    newMat.setTexture("NormalMap", oldMat.getTextureParam("NormalMap").getTextureValue());
                    if (oldMat.getParam("LATC") != null) {
                        newMat.setBoolean("LATC", (Boolean) oldMat.getParam("LATC").getValue());
                    }
                }
                // SpecularMap
                if (oldMat.getTextureParam("SpecularMap") != null) {
                    newMat.setTexture("SpecularMap", oldMat.getTextureParam("SpecularMap").getTextureValue());
                }
                // AlphaMap
//                if (oldMat.getTextureParam("AlphaMap") != null) {
//                    newMat.setTexture("AlphaMap", oldMat.getTextureParam("AlphaMap").getTextureValue());
//                }
                // LightMap
//                if (oldMat.getTextureParam("LightMap") != null) {
//                    newMat.setTexture("LightMap", oldMat.getTextureParam("LightMap").getTextureValue());
//                    if (oldMat.getParam("SeparateTexCoord") != null) {
//                        newMat.setBoolean("SeparateTexCoord", (Boolean) oldMat.getParam("SeparateTexCoord").getValue());
//                    }
//                }

                // Material colors
                if (oldMat.getParam("UseMaterialColors") != null) {
                    if ((Boolean) oldMat.getParam("UseMaterialColors").getValue() == true) {
                        newMat.setBoolean("UseMaterialColors", true);
                        if (oldMat.getParam("Ambient") != null) {
                            newMat.setColor("Ambient", (ColorRGBA) oldMat.getParam("Ambient").getValue());
                        }
                        if (oldMat.getParam("Diffuse") != null) {
                            newMat.setColor("Diffuse", (ColorRGBA) oldMat.getParam("Diffuse").getValue());
                        }
                        if (oldMat.getParam("Specular") != null) {
                            newMat.setColor("Specular", (ColorRGBA) oldMat.getParam("Specular").getValue());
                        }
                    }
                }
                // Shininess
                if (oldMat.getParam("Shininess") != null) {
                    newMat.setFloat("Shininess", (Float) oldMat.getParam("Shininess").getValue());
                }
                // Vertex lighting.
                if (oldMat.getParam("VertexLighting") != null) {
                    newMat.setBoolean("VertexLighting", (Boolean) oldMat.getParam("VertexLighting").getValue());
                }

                // Alpha cut-off.
                newMat.setFloat("AlphaDiscardThreshold", (Float) oldMat.getParam("AlphaDiscardThreshold").getValue());

            } else {
//                newMat = new MaterialSP(assetManager, "biomonkey/matdefs/TreeBase.j3md");
                newMat = new Material(assetManager, "biomonkey/matdefs/TreeBase.j3md");

                // DiffuseMap
                if (oldMat.getTextureParam("DiffuseMap") != null) {
                    newMat.setTexture("DiffuseMap", oldMat.getTextureParam("DiffuseMap").getTextureValue());
                }
                // Normal map
                if (oldMat.getTextureParam("NormalMap") != null) {
                    newMat.setTexture("NormalMap", oldMat.getTextureParam("NormalMap").getTextureValue());
                    if (oldMat.getParam("VTangent") != null) {
                        newMat.setBoolean("LATC", (Boolean) oldMat.getParam("LATC").getValue());
                    }
                    if (oldMat.getParam("LATC") != null) {
                        newMat.setBoolean("LATC", (Boolean) oldMat.getParam("LATC").getValue());
                    }
                    if (oldMat.getParam("PackedNormalParallax") != null) {
                        newMat.setBoolean("PackedNormalParallax", (Boolean) oldMat.getParam("PackedNormalParallax").getValue());
                    }
                }
                // SpecularMap
//                if (oldMat.getTextureParam("SpecularMap") != null) {
//                    newMat.setTexture("SpecularMap", oldMat.getTextureParam("SpecularMap").getTextureValue());
//                }
                // LightMap
                if (oldMat.getTextureParam("LightMap") != null) {
                    newMat.setTexture("LightMap", oldMat.getTextureParam("LightMap").getTextureValue());
                    if (oldMat.getParam("SeparateTexCoord") != null) {
                        newMat.setBoolean("SeparateTexCoord", (Boolean) oldMat.getParam("SeparateTexCoord").getValue());
                    }
                }

                // ParallaxMap
                if (oldMat.getTextureParam("ParallaxMap") != null) {
                    newMat.setTexture("ParallaxMap", oldMat.getTextureParam("ParallaxMap").getTextureValue());
                    if (oldMat.getParam("ParallaxHeight") != null) {
                        newMat.setFloat("ParallaxHeight", (Float) oldMat.getParam("ParallaxHeight").getValue());
                    }
                }

                // Material colors
                if (oldMat.getParam("UseMaterialColors") != null) {
                    if ((Boolean) oldMat.getParam("UseMaterialColors").getValue() == true) {
                        newMat.setBoolean("UseMaterialColors", true);
                        if (oldMat.getParam("Ambient") != null) {
                            newMat.setColor("Ambient", (ColorRGBA) oldMat.getParam("Ambient").getValue());
                        }
                        if (oldMat.getParam("Diffuse") != null) {
                            newMat.setColor("Diffuse", (ColorRGBA) oldMat.getParam("Diffuse").getValue());
                        }
                        if (oldMat.getParam("Specular") != null) {
                            newMat.setColor("Specular", (ColorRGBA) oldMat.getParam("Specular").getValue());
                        }
                    }
                }
                // Shininess
                if (oldMat.getParam("Shininess") != null) {
                    newMat.setFloat("Shininess", (Float) oldMat.getParam("Shininess").getValue());
                }
                // Vertex lighting.
                if (oldMat.getParam("VertexLighting") != null) {
                    newMat.setBoolean("VertexLighting", (Boolean) oldMat.getParam("VertexLighting").getValue());
                }
            }
            
            BlendMode bMode = oldMat.getAdditionalRenderState().getBlendMode();
            FaceCullMode fcMode = oldMat.getAdditionalRenderState().getFaceCullMode();
            if (bMode != null) {
                newMat.getAdditionalRenderState().setBlendMode(bMode);
            }
            if (fcMode != null) {
                newMat.getAdditionalRenderState().setFaceCullMode(fcMode);
            }

            geom.setMaterial(newMat);
            if (fadeEnd>0 || fadeRange>0) {
	            newMat.setFloat("FadeEnd", fadeEnd);
	            newMat.setFloat("FadeRange", fadeRange);
            }
        }
    }
	
}
