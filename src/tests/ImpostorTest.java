package tests;

import myrpg.core.Impostor;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;

public class ImpostorTest extends SimpleApplication {

    public static void main(String[] args) {
        ImpostorTest app = new ImpostorTest();
        app.start();
    }
    
    public ImpostorTest() {
        super();
    }

    DirectionalLight sun;

    @Override
    public void simpleInitApp() {
        setPauseOnLostFocus(false);
        assetManager.registerLocator("assets/", FileLocator.class);
        getViewPort().setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setEnabled(true);
        flyCam.setMoveSpeed(10);
        
        // You must add a light to make the model visible
        sun = new DirectionalLight();
        sun.setDirection(new Vector3f(0, 0, 1));
        rootNode.addLight(sun);

        for (int i = 1; i < 6; i++) {
        
	        Geometry impostor = new Geometry("crossquad", new Impostor());
	        impostor.scale(1.9f);
	        impostor.setLocalTranslation(4*i, 0, 0);
	
	        Material impostorMaterial = new Material(assetManager, "biomonkey/matdefs/TreeImpostorBase.j3md");
	        impostorMaterial.setTexture("ImpostorTexture", assetManager.loadTexture("Models/lowpoly/tree"+i+".png"));
	        impostor.setMaterial(impostorMaterial);
	        rootNode.attachChild(impostor);
	
	        Spatial tree = assetManager.loadModel("Models/lowpoly/tree"+i+".j3o");
	        tree.setLocalTranslation(4*i, 0, 0);
	        rootNode.attachChild(tree);
        }
    }

    @Override
    public void simpleUpdate(float tpf) {
    	sun.setDirection(cam.getDirection());
    }

    @Override
    public void stop() {
        super.stop(); // continue quitting the game
    }
}