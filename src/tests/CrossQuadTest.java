package tests;

import myrpg.core.Crossquad;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.system.AppSettings;

public class CrossQuadTest extends SimpleApplication {

    public BulletAppState bulletAppState;

    public Node collidable;
    
    public static void main(String[] args) {
        
        System.setProperty("AppName", "RRPG");
        
        CrossQuadTest app = new CrossQuadTest();
        
        AppSettings appSettings = new AppSettings(false);
        appSettings.setResolution(800, 600);
        appSettings.setFullscreen(false);
        appSettings.setVSync(false);
        appSettings.setTitle("SurCraft");
        appSettings.setUseInput(true);
        appSettings.setFrameRate(30);
        appSettings.setSamples(0);
        appSettings.setRenderer(AppSettings.LWJGL_OPENGL_ANY);
        appSettings.setAudioRenderer(AppSettings.LWJGL_OPENAL);
        app.setSettings(appSettings);
        app.setShowSettings(false);
        app.start();
    }
    
    public CrossQuadTest() {
        super();
    }

    Node holder;
    Geometry blue;
    Spatial red;

    @Override
    public void simpleInitApp() {
        setPauseOnLostFocus(false);
        assetManager.registerLocator("assets/", FileLocator.class);
        getViewPort().setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setEnabled(true);
        flyCam.setMoveSpeed(10);
        
        // You must add a light to make the model visible
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
        rootNode.addLight(sun);
        
        holder = new Node("holder");
        rootNode.attachChild(holder);

        blue = new Geometry("crossquad", new Crossquad(2.5f, 9.4f)); 
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//        mat.setTexture("ColorMap", assetManager.loadTexture("Models/Fir1/fir1_billboard.png"));
        mat.setTexture("ColorMap", assetManager.loadTexture("Models/Fir1/test1.png"));
        mat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        mat.setFloat("AlphaDiscardThreshold", .3f);
        blue.setMaterial(mat);
        blue.setLocalTranslation(-1.25f, -5, -4);
        holder.attachChild(blue);

        red = assetManager.loadModel("Models/Fir1/fir1_androlo.j3o");
        red.setShadowMode(ShadowMode.CastAndReceive);
        red.setCullHint(CullHint.Always);
        red.setLocalTranslation(0, -5, -4);
        holder.attachChild(red);
        
        initKeys();
    }

    private void initKeys() {
        inputManager.addMapping("change",  new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addListener(actionListener, "change");
    }

    private boolean model_on = true;
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("change") && !keyPressed) {
                if (model_on) {
                    red.setCullHint(CullHint.Never);
                    blue.setCullHint(CullHint.Always);
                } else {
                    blue.setCullHint(CullHint.Never);
                    red.setCullHint(CullHint.Always);
                }
                model_on = !model_on;
            }
        }
    };

    @Override
    public void simpleUpdate(float tpf) {
    }
    
    
    @Override
    public void stop() {
        super.stop(); // continue quitting the game
    }
}
