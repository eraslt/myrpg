package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class SimpleTreeTest extends SimpleApplication {

    public static void main(String[] args) {
        SimpleTreeTest app = new SimpleTreeTest();
        app.start();
    }
    
    public SimpleTreeTest() {
        super();
    }

    @Override
    public void simpleInitApp() {
        setPauseOnLostFocus(false);
        assetManager.registerLocator("assets/", FileLocator.class);
        getViewPort().setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setEnabled(true);
        flyCam.setMoveSpeed(10);
        
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
        rootNode.addLight(sun);

        Spatial model = new SimpleTree(assetManager);//assetManager.loadModel("Models/asd/female-parts.j3o");
        rootNode.attachChild(model);
    }

    @Override
    public void simpleUpdate(float tpf) {
    }

    @Override
    public void stop() {
        super.stop(); // continue quitting the game
    }
}