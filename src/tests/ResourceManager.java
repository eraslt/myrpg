package tests;

public class ResourceManager {

    private int[] resources = new int[2];

    static int WOOD = 1;
    static int STONE = 2;

    static class Producer {
        //should be protected atleas
        int resource_id = 0;
        int producer_type = 0;
        //todo from db or something
        //can be many resources
        int lvl = 1;
        int lvl_inc_storage = 5;
        int lvl_inc_prod = 1;
        int storage = 25;
        int produces = 1;
        int produced = 0;

        float time;
        int delay;

        public Producer(int resource_id) {
            this.resource_id = resource_id ;
        }

        public void produce() {//check time
            int level = this.lvl - 1;
            if (produced<(storage+level*lvl_inc_prod)) {
                produced += produces+level*lvl_inc_prod;
                if (produced>(storage+level*lvl_inc_prod)) {
                    produced = storage+level*lvl_inc_prod;
                }
            }
        }

        public void reset() {
            produced = 0;   
        }
    }

    public static void main(String[] args) {

        Producer wood = new Producer(1);
        wood.lvl = 2;
        wood.produce();

        System.out.println("wood:"+wood.produced);
    }
}
