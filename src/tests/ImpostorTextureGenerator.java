package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.bounding.BoundingBox;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.FrameBuffer;
import com.jme3.texture.Image;
import com.jme3.texture.Image.Format;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.util.BufferUtils;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;

/**
 * This class is used for rendering impostor textures.
 * 
 * @author Andreas
 */
public class ImpostorTextureGenerator extends SimpleApplication  {

    protected static int TEXTURE_RES = 512;
    protected static int numAngles = 8;
    
    protected Camera RTTCam;
    protected ViewPort offView;
    protected RenderManager manager;
    protected FrameBuffer offBuffer;
    protected ByteBuffer IMGBuf;
    
    protected Material dispMat;
    protected Material colorMat;


    public ImpostorTextureGenerator() {
        super();
    }
    
    public Texture generateImpostors(Node model, int distance, String name) {
        manager = new RenderManager(getRenderer());
        
        dispMat = new Material(assetManager,"biomonkey/matdefs/DispNormBase.j3md");
        dispMat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
        
        colorMat = new Material(assetManager,"biomonkey/matdefs/ColorBase.j3md");
        colorMat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);


        IMGBuf = BufferUtils.createByteBuffer(4 * TEXTURE_RES * TEXTURE_RES * 16);

        BoundingBox sphere = (BoundingBox) model.getWorldBound();
        float rad = 2.0f;

        dispMat.setFloat("BSRadius", rad);

        model.setQueueBucket(Bucket.Transparent);

        /*
         * Preparing the Render-to-texture camera. Setting up proper camera
         * frustums for rendering the model. The distance variable is the distance
         * from the camera to where impostors are faded in/out. It ensures that proper
         * perspective is used at that point (to reduce artifacts during the
         * transitioning process).
         */
        RTTCam = new Camera(TEXTURE_RES, TEXTURE_RES);

//        RTTCam.setLocation(sphere.getCenter().add(new Vector3f(0, 0, distance)));
        RTTCam.setLocation(Vector3f.ZERO.add(new Vector3f(0, 0, distance)));
        RTTCam.lookAt(sphere.getCenter(), Vector3f.UNIT_Y);

        //Frustum angle in degrees.
        float frustumAngle = 2 * FastMath.asin(rad / (float)distance) * 180f / FastMath.PI;
        
//        frustumAngle = 45f;
        System.out.println("frustumAngle:"+frustumAngle);
        
        // This is mainly for debugging purposes.
        if (frustumAngle > 45f) {
            throw new RuntimeException("The RTT-camera is too close to the model."
                    + " The y-FoV should be < 45 degrees (it is now: " + frustumAngle + "degrees).");
        }
        
        RTTCam.setFrustumPerspective(frustumAngle, 1.0f, distance - rad, distance + rad);
        
        // Setting up the viewport.
        offView = manager.createMainView("RTTView", RTTCam);
        offView.setClearFlags(true, true, true);
        offView.setBackgroundColor(new ColorRGBA(0.5f, 0.5f, 0.5f, 0f));

        // Creating the offscreen framebuffer.
        offBuffer = new FrameBuffer(TEXTURE_RES, TEXTURE_RES, 1);

        offBuffer.setDepthBuffer(Format.Depth);
        offBuffer.setColorBuffer(Format.RGBA8);
        // Render to the offscreen framebuffer.
        offView.setOutputFrameBuffer(offBuffer);
        
        /*
         * Rendering the model. This takes place in several steps.
         */
        float baseAngle = FastMath.TWO_PI / (float) numAngles;

        int counter = 0;

        // Creating an array to store pixel values in.
        byte[] IMGArray = new byte[IMGBuf.capacity()];

        offView.attachScene(model);
        model.setLocalRotation(Quaternion.IDENTITY);
        model.setLocalTranslation(Vector3f.ZERO);
        model.updateGeometricState();
        
        while (counter < numAngles) {

            int offsetX = counter % 4;
            int offsetY = (counter + 8) / 4;

            float currAng = baseAngle * counter;

            Quaternion rot = new Quaternion().fromAngleNormalAxis(currAng, Vector3f.UNIT_Y);
            
            model.setLocalRotation(rot);
            model.updateGeometricState();
            
            // Render the model and capture the framebuffer.
            
            ByteBuffer outBuf = BufferUtils.createByteBuffer(4 * TEXTURE_RES * TEXTURE_RES);
            
            manager.render(0, true);
            getRenderer().readFrameBuffer(offBuffer, outBuf);
//            renderer.readFrameBuffer(offBuffer, outBuf);

            // We need to structure the buffers a bit differently to make
            // the final texture into a proper grid of "sub-textures".
            outBuf.clear();
            byte[] bb = new byte[outBuf.capacity()];
            outBuf.get(bb);
            
            for (int i = 0; i < TEXTURE_RES; i++) {
                int NDPos = 4 * TEXTURE_RES * (offsetX + i * 4 + offsetY * 4 * TEXTURE_RES);
                System.arraycopy(bb, i * 4 * TEXTURE_RES, IMGArray, NDPos, 4 * TEXTURE_RES);
            }
            counter++;
        }

        counter = 0;

        while (counter < numAngles) {

            int offsetX = counter % 4;
            int offsetY = counter / 4;

            float currAng = baseAngle * counter;
            
            Quaternion rot = new Quaternion().fromAngleNormalAxis(currAng, Vector3f.UNIT_Y);
            
            model.setLocalRotation(rot);
            model.updateGeometricState();

            ByteBuffer outBuf = BufferUtils.createByteBuffer(4 * TEXTURE_RES * TEXTURE_RES);

            manager.render(0, true);
            getRenderer().readFrameBuffer(offBuffer, outBuf);

            outBuf.clear();
            byte[] bb = new byte[outBuf.capacity()];
            outBuf.get(bb);

            for (int i = 0; i < TEXTURE_RES; i++) {
                System.arraycopy(bb, i * 4 * TEXTURE_RES, IMGArray, 4 * TEXTURE_RES * (offsetX + i * 4 + offsetY * 4 * TEXTURE_RES), 4 * TEXTURE_RES);
            }
            counter++;
        }

        IMGBuf.put(IMGArray);

        printImage(IMGBuf, name);
        Image img = new Image(Format.ABGR8,TEXTURE_RES*4, TEXTURE_RES*4, IMGBuf);
        
        Texture tex = new Texture2D(img);
        tex.setMinFilter(Texture.MinFilter.BilinearNoMipMaps);
        tex.setMagFilter(Texture.MagFilter.Bilinear);
        
        return tex;
        
    }

    /**
     * Get a vector of magnitude 1.
     * 
     * @param angle The angle.
     * @return The vector.
     */
    protected Vector3f getUCVec(float angle) {
        // Measuring the angle from the z-axis so it needs to be
        // displaced by pi/2
        return new Vector3f(FastMath.cos(-angle + FastMath.HALF_PI), 0, FastMath.sin(-angle + FastMath.HALF_PI));
    }

    protected void printImage(ByteBuffer imgBuf, String name) {
        BufferedImage awtImage = new BufferedImage(TEXTURE_RES*4, TEXTURE_RES*4, BufferedImage.TYPE_4BYTE_ABGR);
        WritableRaster wr = awtImage.getRaster();
        DataBufferByte db = (DataBufferByte) wr.getDataBuffer();

        byte[] cpuArray = db.getData();

        // Copy native memory to java memory
        imgBuf.clear();
        imgBuf.get(cpuArray);
        imgBuf.clear();

        int width = wr.getWidth();
        int height = wr.getHeight();

        // Flip the components the way AWT likes them
        for (int y = 0; y < height / 2; y++) {
            for (int x = 0; x < width; x++) {
                int inPtr = (y * width + x) * 4;
                int outPtr = ((height - y - 1) * width + x) * 4;

                byte b1 = cpuArray[inPtr + 0];
                byte g1 = cpuArray[inPtr + 1];
                byte r1 = cpuArray[inPtr + 2];
                byte a1 = cpuArray[inPtr + 3];

                byte b2 = cpuArray[outPtr + 0];
                byte g2 = cpuArray[outPtr + 1];
                byte r2 = cpuArray[outPtr + 2];
                byte a2 = cpuArray[outPtr + 3];

                cpuArray[outPtr + 0] = a1;
                cpuArray[outPtr + 1] = b1;
                cpuArray[outPtr + 2] = g1;
                cpuArray[outPtr + 3] = r1;

                cpuArray[inPtr + 0] = a2;
                cpuArray[inPtr + 1] = b2;
                cpuArray[inPtr + 2] = g2;
                cpuArray[inPtr + 3] = r2;
            }
        }
        
        try {
            File file = new File(name);
            file.mkdirs();
            ImageIO.write(awtImage, "png", file);
        } catch (IOException ex) {
        }
    }

    public static void main(String[] args) {
        ImpostorTextureGenerator app = new ImpostorTextureGenerator();
        app.start();
    }

    Spatial s;
    @Override
    public void simpleInitApp() {
//        assetManager.registerLocator("assets/", FileLocator.class);
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.5f, -1.0f));
        rootNode.addLight(sun);
    }
    
    private int cnt = 0;
    @Override
    public void simpleUpdate(float tpf) {
        cnt ++;
        if (cnt==5) {
        	for (int i = 1; i<6; i++) {
        		s = assetManager.loadModel("Models/lowpoly/tree"+i+".j3o");
                rootNode.attachChild(s);
                generateImpostors((Node)s, 100, "tree"+i+".png");
                rootNode.detachChild(s);
        	}
            stop();
        }
    }
}
