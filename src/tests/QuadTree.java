package tests;


import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import com.jme3.bounding.BoundingVolume;

/**
 * A node in the quadtree.
 *
 * @author jochen
 *
 */
public class QuadTree {

   /**
    * Bounding volume of this node.
    */
   private BoundingVolume volume;

   /**
    * Contains all the children of this node.
    */
   private Set<QuadLeaf> children;

   /**
    * Contains all the child nodes of this node.
    */
   private Set<QuadTree> childNodes;

   /**
    * Is this node a leaf?
    */
   private boolean leaf;

   /**
    * Minimum size of a node.
    */
   private Float minExtend;

   /**
    * Atomic boolean for cas operations.
    */
   private AtomicBoolean lockBool = new AtomicBoolean(false);

   /**
    * Constructor.
    *
    * @param volume
    *            boundingvolume of this node.
    */
   public QuadTree(BoundingVolume volume, Float minExtend) {

      this.volume = volume;

      this.minExtend = minExtend;

      if (minExtend.equals(volume.getExtend())) {

         leaf = true;

         children = new HashSet<QuadLeaf>();

         return;

      }

      leaf = false;

      childNodes = new HashSet<QuadTree>();

   }

   /**
    * Check if the node is a leaf.
    *
    * @return
    */
   public boolean isLeaf() {

      return leaf;

   }

   /**
    * Check if the given leaf intersects the node.
    * @param child
    * @return
    */
   public boolean intersects(QuadLeaf child) {

      return volume.intersects(child.getBoundingVolume());

   }

   /**
    * Add a new child to the node.
    * @param child
    */
   public void addChild(QuadLeaf child) {

      Set<QuadTree> collectedNodes=new HashSet<QuadTree>();

      if (leaf) {

         children.add(child);

         return;

      }

      if (childNodes.size() == 0) {

         Float _extend = volume.getExtend() / 2;

         QuadTree newNode;

         // left top

         newNode = new Node(new BoundingVolume(volume.getX() - _extend,

               volume.getY() - _extend, _extend), minExtend);

         newNode.addChild(child);

         if (newNode.intersects(child)) {

            newNode.lock();

            collectedNodes.add(newNode);

         }

         childNodes.add(newNode);

         // right top

         newNode = new QuadTree(new BoundingVolume(volume.getX() + _extend,

               volume.getY() - _extend, _extend), minExtend);

         newNode.addChild(child);

         if (newNode.intersects(child)) {

            newNode.lock();

            collectedNodes.add(newNode);

         }

         childNodes.add(newNode);

         // left bottom

         newNode = new Node(new BoundingVolume(volume.getX() - _extend,

               volume.getY() + _extend, _extend), minExtend);

         newNode.addChild(child);

         if (newNode.intersects(child)) {

            newNode.lock();

            collectedNodes.add(newNode);

         }

         childNodes.add(newNode);

         // right bottom

         newNode = new QuadTree(new BoundingVolume(volume.getX() + _extend,

               volume.getY() + _extend, _extend), minExtend);

         newNode.addChild(child);

         if (newNode.intersects(child)) {

            newNode.lock();

            collectedNodes.add(newNode);

         }

         childNodes.add(newNode);

      }

      if (collectedNodes.size() == 0) {

         //check what nodes the child intersects

         for (QuadTree node : childNodes) {

            if (node.intersects(child)) {

               collectedNodes.add(node);

               do{

                  //spinlock!!! :)

               }while(!node.lock());

            }

         }

      }

      //we are done processing, unlock

      unlock();

      for (QuadTree node : collectedNodes) {

         node.addChild(child);

      }

   }

   /**
    * Used to perform a cas for locking this node.
    *
    * @return false if the lock failed.
    */
   public boolean lock() {
      return lockBool.compareAndSet(false, true);
   }

   /**
    * Used to perform a cas for unlocking this node.
    *
    * @return false if the unlock failed.
    */
   public boolean unlock() {
      return lockBool.compareAndSet(true, false);
   }
}
