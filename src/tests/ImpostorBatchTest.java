package tests;

import myrpg.core.Impostor;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.SimpleBatchNode;

public class ImpostorBatchTest extends SimpleApplication {

    public static void main(String[] args) {
        ImpostorBatchTest app = new ImpostorBatchTest();
        app.start();
    }
    
    public ImpostorBatchTest() {
        super();
    }

    float fadeEnd = 20;
    float fadeRange = 10;
    @Override
    public void simpleInitApp() {
        setPauseOnLostFocus(false);
        assetManager.registerLocator("assets/", FileLocator.class);
        getViewPort().setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setEnabled(true);
        flyCam.setMoveSpeed(10);

        // You must add a light to make the model visible
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
        rootNode.addLight(sun);

        Geometry impostor = new Geometry("crossquad", new Impostor(1));
        impostor.scale(5f);

        Material impostorMaterial = new Material(assetManager, "biomonkey/matdefs/TreeImpostorBase.j3md");
        impostorMaterial.setTexture("ImpostorTexture", assetManager.loadTexture("Models/Fir1/fir1_androlo300.png"));
        impostor.setMaterial(impostorMaterial);
        rootNode.attachChild(impostor);

        Geometry impostorBatched = new Geometry("crossquad", new Impostor(5));
        impostorBatched.setLocalTranslation(5, 0, 0);
        impostorBatched.setMaterial(impostorMaterial);
        SimpleBatchNode batch = new SimpleBatchNode();
        batch.attachChild(impostorBatched);
        batch.batch();
        rootNode.attachChild(batch);
    }


    @Override
    public void simpleUpdate(float tpf) {
    }

    @Override
    public void stop() {
        super.stop(); // continue quitting the game
    }
}