package tests;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class ModelsTest extends SimpleApplication {

    public static void main(String[] args) {
        ModelsTest app = new ModelsTest();
        app.start();
    }
    
    public ModelsTest() {
        super();
    }

    float fadeEnd = 20;
    float fadeRange = 10;
    float SpritefadeEnd = 50;
    float fadeEndTree = 30;
    float fadeRangeTree = 10;
    boolean impostor_fade_in = false;
    boolean impostor_fade_out = false;

    @Override
    public void simpleInitApp() {
        setPauseOnLostFocus(false);
        assetManager.registerLocator("assets/", FileLocator.class);
        getViewPort().setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setEnabled(true);
        flyCam.setMoveSpeed(10);
        
        // You must add a light to make the model visible
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
        rootNode.addLight(sun);

        Spatial model = assetManager.loadModel("Models/asd/female-parts.j3o");
//        Spatial model = assetManager.loadModel("Models/asd/male-parts-no-bones.j3o");
        
//        Spatial model = assetManager.loadModel("Models/lowpoly/Wolf.blend");
//        Spatial model = assetManager.loadModel("Models/lowpoly/Character1.blend");
        rootNode.attachChild(model);
    }

    @Override
    public void simpleUpdate(float tpf) {
    }

    @Override
    public void stop() {
        super.stop(); // continue quitting the game
    }
}