package tests;

import info.mindes.SimpleGrid.Position;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import myrpg.world.forest.TreePage;

public class SerializeTest {

    public ConcurrentHashMap<Integer, TreePage> trees = new ConcurrentHashMap<>();
    private final static String DATA_FILE = System.getProperty("user.home") + "/RRPG/test_serialize";

    public static void main(String[] args) {
        SerializeTest s = new SerializeTest();
        s.getTreeData(new Position(0, 0));
        s.getTreeData(new Position(1, 0));
        s.getTreeData(new Position(0, 1));
        System.out.println(s.trees.size());
        s.saveForest();
        s.loadForest();
        System.out.println(s.trees.size());
    }
    
    public void saveForest() {
        try (
                OutputStream file = new FileOutputStream(DATA_FILE);
                OutputStream buffer = new BufferedOutputStream(file);
                ObjectOutput output = new ObjectOutputStream(buffer);
            ){
                output.writeObject(trees);
        }
        catch(IOException ex){
            Logger.getLogger(SerializeTest.class.getName()).log(Level.SEVERE, "Cannot perform output.", ex);
        }
    }

    @SuppressWarnings("unchecked")
    private void loadForest() {
        try(
                InputStream file = new FileInputStream(DATA_FILE);
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input = new ObjectInputStream (buffer);
            ){
            trees = (ConcurrentHashMap<Integer, TreePage>) input.readObject();
        } catch(ClassNotFoundException ex) {
            Logger.getLogger(SerializeTest.class.getName()).log(Level.SEVERE, "Cannot perform input. Class not found.", ex);
        } catch(IOException ex) {
            Logger.getLogger(SerializeTest.class.getName()).log(Level.SEVERE, "Cannot perform input.", ex);
        }
    }

    public TreePage getTreeData(Position position) {
        int tile = position.hashCode();
        if (!trees.contains(tile)) {
            trees.put(tile, new TreePage());
        }
        return trees.get(tile);
    }
    
}
