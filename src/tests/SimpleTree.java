package tests;

//import java.util.Random;


import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Sphere;

public class SimpleTree extends Node {

    static int tree_id = 0;
    
    public SimpleTree(AssetManager assetManager) {
        setName("SimpleTree-"+tree_id++);
        float trunk_radius = 1f;
        float trunk_height = 5f;
        float leaves_radius = 3f;
        float leaves_offset = 4.7f;
        attachChild(createTrunk(assetManager, trunk_radius, trunk_height));
        attachChild(createLeaves(assetManager, leaves_radius, leaves_offset));
        
        
        
//        Random rnd = new Random();
//        int leaves  = rnd.nextInt(10);
//        https://en.wikipedia.org/wiki/Spherical_coordinate_system
        /*for (int j = 0; j < 100; j++) {
            float size = rnd.nextFloat()+0.3f;
            int x = rnd.nextInt(Math.round(leaves_radius));
            int y = rnd.nextInt(Math.round(leaves_radius));
            int z = rnd.nextInt(Math.round(leaves_radius));
            System.out.println("size:"+size+" x:"+x+" y:"+y+" z:"+z);
            Geometry g = createLeaves(assetManager, size, leaves_offset);
            
            double r = Math.sqrt(x*x+y*y+z*z);
            double i = Math.acos(r>0?z/r:0);
            double a = Math.atan(x>0?y/x:0);

//            g.move(x, y, z);
            g.move((float)r, (float)i, (float)a);
            attachChild(g);
        }*/
        
//        attachChild(createLeaves(assetManager, 3, 4.7f));
        
        
        
//        Quad c = new Quad(10, 10);
//        Geometry g = new Geometry("plane-", c);
//        Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//        mat1.setColor("Color", ColorRGBA.Blue);
//        g.setMaterial(mat1);
//        g.rotate(0, 90*FastMath.DEG_TO_RAD, 0);
//        attachChild(g);
    }
    
    private Geometry createTrunk(AssetManager assetManager, float radius, float height) {
        Cylinder c = new Cylinder(6, 30, radius, height);
        Geometry trunk = new Geometry("trunk-"+tree_id, c);
        Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat1.setColor("Color", ColorRGBA.Brown);
        trunk.setMaterial(mat1);
        trunk.rotate(90*FastMath.DEG_TO_RAD, 0, 0);

//        CylinderCollisionShape cs = new CylinderCollisionShape(new Vector3f(1f,0,9f), 2);
//        RigidBodyControl ph = new RigidBodyControl(cs, 0f);
//        trunk.addControl(ph);
//        root.bulletAppState.getPhysicsSpace().add(ph);

        trunk.move(0, height/2, 0);

        return trunk;
    }

    private Geometry createLeaves(AssetManager assetManager, float radius, float offset) {
        Sphere s = new Sphere(30, 30, radius);
        Geometry leaves = new Geometry("leaves-"+tree_id, s);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.randomColor());
        leaves.setMaterial(mat);
        leaves.move(0, offset+radius, 0);
        return leaves;
    }
}
