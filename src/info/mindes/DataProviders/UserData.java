package info.mindes.DataProviders;

import java.util.Map;


public interface UserData {
	
	public int getUserID(String username, String password);
	
	public Map<Integer, String> getPlayers(int user_id);
	
	public int getActivePlayer(int user_id);

}
