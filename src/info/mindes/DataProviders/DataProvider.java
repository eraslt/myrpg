package info.mindes.DataProviders;

import info.mindes.DataProviders.foobar.FoobarSkillsData;
import info.mindes.DataProviders.jsonRemote.JsonItemData;
import info.mindes.DataProviders.jsonRemote.JsonRecipesData;
import info.mindes.DataProviders.mysql.MysqlObjectData;

public class DataProvider {

    public static ItemData item = new JsonItemData();
    public static RecipesData recipes = new JsonRecipesData();
    public static SkillsData skills = new FoobarSkillsData();
    public static ObjectData object = new MysqlObjectData();
    
}
