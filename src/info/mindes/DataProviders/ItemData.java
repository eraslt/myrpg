package info.mindes.DataProviders;

import java.util.Map;

public interface ItemData {
	
	class Attribute {
		public String attribute;
		public String value;
		
		public Attribute(String attribute, String value) {
			this.attribute = attribute;
			this.value = value;
		}
	}
	
	/**
     * Getter for item attribute.
     *
     * @param Integer item_id represents item  
     * @param String attribute name.
     * 
     * @return String attribute defined for that item, or default if none defined, or null if attribute/item doesnt exist
     */
	public String getItemAttribute(int item_id, String attribute);
	
	/**
     * Getter for item attribute.
     *
     * @param Integer item_id represents item  
     * @param Integer attribute id.
     * 
     * @return String attribute defined for that item, or default if none defined, or null if attribute/item doesnt exist
     */
	public String getItemAttribute(int item_id, int attribute_id);
	
	/**
     * Return map of all atributes
     * 
     * @return all atributes in form Map<attribute_id, attribute_name>
     */
	public Map<Integer, String> getAllAttributes();
	
	/**
     * Getter for all item attributes.
     *
     * @param Integer item_id represents item  
     * 
     * @return all item atributes in form Map<attribute_id, value>, or null if item doesnt exist
     */
	public Map<Integer, String> getAllItemAttributes(int item_id);
	
}
