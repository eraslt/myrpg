package info.mindes.DataProviders;

import java.util.Map;

public interface SkillsData {

	public String getSkill(int skill_id);
	
	public String getDescription(int skill_id);
	
	public Map<Integer, String> getAll();
	
}
