package info.mindes.DataProviders;

import java.util.Map;

public interface RecipesData {
	
	class Recipe {
		public int id;
		public String name;
		public int delay;
		public Map<Integer, Integer> req;
		public Map<Integer, Integer> res;
	}
	
	public Recipe get(int id);
	
	public Recipe getRecipe(int id);
	
	public int getDelay(int id);
	
	public String getName(int id);
	
	public Map<Integer, Recipe> getAll();
	
	public Map<Integer, Integer> getRequiredItems(int id);
	
	public Map<Integer, Integer> getResultItems(int id);
	
	public boolean isRecipe(Map<Integer, Integer> items);
	
	public int getRecipeId(Map<Integer, Integer> items);
}
