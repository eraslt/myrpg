package info.mindes.DataProviders;

import java.util.Map;

public interface PlayerData {

	public class Item {
		public int id;
		public int cnt;
		
		public Item(int id, int cnt) {
			this.id = id;
			this.cnt = cnt;
		}
	}
	
	public Map<Integer, Item> getInventory(int player_id);
	
	public Map<String, String> getPlayerData(int player_id);
	
	public Map<Integer, Integer> getAllItems(int player_id);
	
	public Map<Integer, Integer> getAllSkills(int player_id);
	
	public int getPlayerSkill(int player_id, int skill_id);

	public void saveItems(int player_id, Map<Integer, Item> inventory);
	
	public void savePosition(float x, float y, float z);
	
}
