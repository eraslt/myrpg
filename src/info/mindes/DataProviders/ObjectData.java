package info.mindes.DataProviders;

import java.util.HashMap;
import java.util.Map;


public interface ObjectData {
	
	public class Action {
		
		public class ActionItem {
			public int id;
			public int pos;
			public int min;
			public int max;
			
			public ActionItem(int id, int pos, int min, int max) {
				this.id = id;
				this.pos = pos;
				this.min = min;
				this.max = max;
			}
		}
		
		public int id = 0;
		public String name;
		public float delay = 0;
		public int req_att = 0;
		public int req_val = 0;
		
		public Map<Integer, ActionItem> items = new HashMap<>();
		
		public Action(int id, String name, int delay) {
			this.id = id;
			this.name = name;
			this.delay = delay;
		}
		
		public Action(int id, String name, int delay, int req_att, int req_val) {
			this.id = id;
			this.name = name;
			this.delay = delay;
			this.req_att = req_att;
			this.req_val = req_val;
		}
		
		public void addActionItem(int id, int pos, int min, int max) {
			items.put(id, new ActionItem(id, pos, min, max));
		}
	}
	
	public String getName(int id);

	public String getModel(int id);
	
	public Map<Integer, Action> getActions(int id);
	
	public Action getDefaultAction(int id);
	
	public Action getItemAction(int object_id, int item_id);

}
