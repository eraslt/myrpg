package info.mindes.DataProviders.foobar;

import info.mindes.DataProviders.SkillsData;

import java.util.HashMap;
import java.util.Map;

public class FoobarSkillsData implements SkillsData {

	@SuppressWarnings("serial")
	private Map<Integer, String> skills = new HashMap<Integer, String>(){{
		put(1, "Woodcuting");
		put(2, "Mining");
		put(3, "Survival");
	}};
	
	@Override
	public String getSkill(int skill_id) {
		return (null==skills.get(skill_id))?"none":skills.get(skill_id);
	}

	@Override
	public String getDescription(int skill_id) {
		return (null==skills.get(skill_id))?"none":skills.get(skill_id);
	}

	@Override
	public Map<Integer, String> getAll() {
		return skills;
	}

}
