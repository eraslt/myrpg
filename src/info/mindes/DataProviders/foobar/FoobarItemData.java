package info.mindes.DataProviders.foobar;

import info.mindes.DataProviders.ItemData;

import java.util.HashMap;
import java.util.Map;


public class FoobarItemData implements ItemData {

	private Map<Integer, String> attributes = null;
	private Map<Integer, String> foo = null;
	
	public FoobarItemData() {
		attributes = new HashMap<Integer, String>();
		attributes.put(1, "Name");
		attributes.put(2, "Description");
		attributes.put(3, "Image");
		attributes.put(4, "Model");
		foo = new HashMap<Integer, String>();
		foo.put(1, "Item name");
		foo.put(2, "some descriptiopn");
		foo.put(3, "None.png");
		foo.put(4, "Teapot/Teapot.j3o");
	}
	
	@Override
	public String getItemAttribute(int item_id, String attribute) {
		return "None.png";
	}

	@Override
	public String getItemAttribute(int item_id, int attribute_id) {
		return (null==foo.get(attribute_id))?"None.png":foo.get(item_id);
	}

	@Override
	public Map<Integer, String> getAllAttributes() {
		return attributes;
	}

	@Override
	public Map<Integer, String> getAllItemAttributes(int item_id) {
		return foo;
	}
}
