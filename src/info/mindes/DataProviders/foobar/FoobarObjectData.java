package info.mindes.DataProviders.foobar;

import info.mindes.DataProviders.ObjectData;

import java.util.HashMap;
import java.util.Map;

public class FoobarObjectData implements ObjectData {

    @Override
    public String getName(int id) {
        return "name";
    }

    @Override
    public String getModel(int id) {
        return "Tree/Tree.mesh.j3o";
    }

    @Override
    public Map<Integer, Action> getActions(int id) {
        Map<Integer, Action> ret = new HashMap<>();
        return ret;
    }

    @Override
    public Action getDefaultAction(int id) {
    	Action a = new Action(
                1,
                "somtest",
                2);

        a.addActionItem(1, //item_id
                75, //possibility
                1, //min
                2 //max
                );
        return a;
    }

    @Override
    public Action getItemAction(int object_id, int item_id) {
        Action a = new Action(
                2,
                "somtest",
                1);
        a.addActionItem(1, //item_id
                100, //possibility
                1, //min
                5 //max
                );
        a.addActionItem(2, //item_id
                75, //possibility
                1, //min
                2 //max
                );
        return a;
    }
}
