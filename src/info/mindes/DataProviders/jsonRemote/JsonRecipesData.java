package info.mindes.DataProviders.jsonRemote;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;

import info.mindes.DataProviders.RecipesData;

public class JsonRecipesData implements RecipesData {
	
	private Map <Integer, Recipe> recipes;
	
	public JsonRecipesData() {
		fetchData();
	}
	
	@Override
	public Recipe getRecipe(int id) {
		return recipes.get(id);
	}

	public String getName(int id) {
		if (!recipes.containsKey(id)) return null;
		return recipes.get(id).name;
	}
	
	@Override
	public int getDelay(int id) {
		if (!recipes.containsKey(id)) return 0;
		return recipes.get(id).delay;
	}

	@Override
	public Map<Integer, Integer> getRequiredItems(int id) {
		if (!recipes.containsKey(id)) return null;
		return recipes.get(id).req;
	}

	@Override
	public Map<Integer, Integer> getResultItems(int id) {
		if (!recipes.containsKey(id)) return null;
		return recipes.get(id).res;
	}

	@Override
	public boolean isRecipe(Map<Integer, Integer> items) {
		return getRecipeId(items)>0;
	}

	/*
	 * TODO: should return all recipes that match criteria 
	 */
	@Override
	public int getRecipeId(Map<Integer, Integer> items) {
		int rid = 0;
		Set<Integer> keys = recipes.keySet();
        for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
        	
        	Recipe recipe = recipes.get(i.next());
        	
        	Set<Integer> ids = recipe.req.keySet();
        	for (Iterator<Integer> j = ids.iterator(); j.hasNext();) {
            	int item_id = j.next();
            	int item_cnt = recipe.req.get(item_id);
            	
            	System.out.println("item:"+item_id+" cnt:"+item_cnt);
            	
            	//check if has items
            	if (!items.containsKey(item_id)) {
            		rid = 0;
            		break;
            	}
            	//check if has right amount
            	if (items.get(item_id)<item_cnt) {
            		rid = 0;
            		break;
            	}
            	rid = recipe.id;
            }
        	if (rid>0) break;//stop searching on first found item
        }
		return rid;
	}

	private void fetchData() {
		recipes = new HashMap<>();
		/*
		 * Get all recipes
		 */
		URL url = null;
		try {
			JsonRemotes.Auth();
			url = new URL(JsonRemotes.recipes);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		
		try (InputStream is = url.openStream(); JsonReader rdr = Json.createReader(is)) {
			JsonArray recipes = rdr.readArray();
			for (JsonArray recipe : recipes.getValuesAs(JsonArray.class)) {
				Recipe r = new Recipe();
				r.id = recipe.getInt(0);
				r.name = recipe.getString(1);
				r.delay = recipe.getInt(2);
				r.req = new HashMap<>();
				r.res = new HashMap<>();
				JsonArray required = recipe.getJsonArray(3);
				for (JsonArray req : required.getValuesAs(JsonArray.class)) {
					r.req.put(req.getInt(0), req.getInt(1));
				}
				JsonArray resluts = recipe.getJsonArray(4);
				for (JsonArray res : resluts.getValuesAs(JsonArray.class)) {
					r.res.put(res.getInt(0), res.getInt(1));
				}
				this.recipes.put(r.id, r);
			}
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*tests*/
	public static void main(String[] args) {
		JsonRecipesData test = new JsonRecipesData();
		System.out.println(test.getName(2) + ":" + test.getDelay(2));
		
		System.out.println(test.getRequiredItems(2).toString());
		System.out.println(test.getResultItems(2).toString());
		
		HashMap<Integer, Integer> t = new HashMap<Integer, Integer>();
		t.put(1,5);
		System.out.println(test.isRecipe(t));
		t.put(15,5);
		System.out.println(test.isRecipe(t));
		
	}

	@Override
	public Map<Integer, Recipe> getAll() {
		return recipes;
	}

	@Override
	public Recipe get(int id) {
		return recipes.get(id);
	}
}
