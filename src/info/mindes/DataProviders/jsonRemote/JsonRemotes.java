package info.mindes.DataProviders.jsonRemote;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class JsonRemotes {

	private static boolean auth_required = false;
	
	static void Auth() {
		if (auth_required)
			Authenticator.setDefault (new Authenticator() {
			    protected PasswordAuthentication getPasswordAuthentication() {
			        return new PasswordAuthentication ("ochlocracy", "supersave!".toCharArray());
			    }
			});
	}
	public static String url = "http://localhost/rAdmin/";
	
	public static final String items = url + "?data/exportItems";
	public static final String attributes = url + "?data/exportAttributes";
	public static final String actions = url + "?data/exportActions";
	public static final String recipes = url + "?data/exportRecipes";
	
}
