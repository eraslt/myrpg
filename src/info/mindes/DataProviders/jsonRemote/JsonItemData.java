package info.mindes.DataProviders.jsonRemote;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;

import info.mindes.DataProviders.ItemData;

public class JsonItemData implements ItemData {
	
	Map<Integer, Attribute> attributes = null;
	//string to id link to decrease iterations
	Map<String, Integer> att_values = null;
	Map<Integer, String> rev_att_values = null;
	Map<Integer, Map<Integer, String>> items = null;

	public JsonItemData() {
		fetchData();
	}
	
	@Override
	public String getItemAttribute(int item_id, String attribute) {
		if (!att_values.containsKey(attribute)) return null;
		int attr_id = att_values.get(attribute);
		
		if (!items.containsKey(item_id)) return null;
		String attr = items.get(item_id).get(attr_id);
		if (null==attr) attr = attributes.get(attr_id).value;// set default if null;
		return attr;
	}

	@Override
	public String getItemAttribute(int item_id, int attr_id) {
		if (!att_values.containsValue(attr_id)) return null;
		
		if (!items.containsKey(item_id)) return null;
		String attr = items.get(item_id).get(attr_id);
		if (null==attr) attr = attributes.get(attr_id).value;// set default if null;
		return attr;
	}

	@Override
	public Map<Integer, String> getAllItemAttributes(int item_id) {
		if (!items.containsKey(item_id)) return null;
		return items.get(item_id);
	}

	private void fetchData() {
		attributes = new HashMap<>();
		att_values = new HashMap<>();
		rev_att_values = new HashMap<>();
		items = new HashMap<>();

		/*
		 * Get all atributes
		 */
		URL url = null;
		try {
			JsonRemotes.Auth();
			url = new URL(JsonRemotes.attributes);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		
		try (InputStream is = url.openStream(); JsonReader rdr = Json.createReader(is)) {
			JsonArray results = rdr.readArray();
			for (JsonArray result : results.getValuesAs(JsonArray.class)) {
				attributes.put(result.getInt(0), new Attribute(result.getString(1), result.getString(2)));
				att_values.put(result.getString(1), result.getInt(0));
				rev_att_values.put(result.getInt(0), result.getString(1));
			}
			
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		/*
		 * Get all item stuff
		 */
		try {
			url = new URL(JsonRemotes.items);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		
		try (InputStream is = url.openStream(); JsonReader rdr = Json.createReader(is)) {
			JsonArray results = rdr.readArray();
			for (JsonArray result : results.getValuesAs(JsonArray.class)) {
				if (!items.containsKey(result.getInt(0)))
					items.put(result.getInt(0), new HashMap<Integer, String>());
				items.get(result.getInt(0)).put(result.getInt(1), result.getString(2));
			}
			
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<Integer, String> getAllAttributes() {
		return rev_att_values;
	}
	
	/*tests*/
	public static void main(String[] args) {
		JsonItemData test = new JsonItemData();
		System.out.println(test.getItemAttribute(1,1));
		System.out.println(test.getItemAttribute(1,"name"));
		System.out.println(test.getAllItemAttributes(20).toString());
	}
}
