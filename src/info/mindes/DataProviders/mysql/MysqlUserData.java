package info.mindes.DataProviders.mysql;

import java.util.HashMap;
import java.util.Map;

import info.mindes.DataProviders.UserData;

import simplemysql.SimpleMySQL;
import simplemysql.SimpleMySQLResult;

/*
 * TODO: username and password qstr to prevent injections
 */

public class MysqlUserData implements UserData {

	@Override
	public int getUserID(String username, String password) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT id FROM `rrpg_users` "+
				"WHERE email='" + username + "' "+
				"AND password=MD5('" + password + "')");
		
		if (result.getNumRows()<=0) return -1;
		
		int id = Integer.parseInt(result.FetchAssoc().get("id"));
		result.close();
		mysql.close();
		return id;
	}

	@Override
	public Map<Integer, String> getPlayers(int user_id) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT id, name FROM `rrpg_player` WHERE user_id="+user_id);
		
		Map<Integer, String> ret = new HashMap<Integer, String>(); 
		Map<String, String> myRow;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			ret.put(Integer.parseInt(myRow.get("id")), myRow.get("name"));
		}
		result.close();
		mysql.close();
		return ret;
	}

	@Override
	public int getActivePlayer(int user_id) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT id FROM `rrpg_players` WHERE user_id='" + user_id + "' "+
				"LIMIT 1;");
		
		if (result.getNumRows()<=0) return -1;
		
		int id = Integer.parseInt(result.FetchAssoc().get("id"));
		result.close();
		mysql.close();
		return id;
	}
}
