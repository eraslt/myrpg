package info.mindes.DataProviders.mysql;

import java.util.HashMap;
import java.util.Map;

import info.mindes.DataProviders.ObjectData;

import simplemysql.SimpleMySQL;
import simplemysql.SimpleMySQLResult;

public class MysqlObjectData implements ObjectData {

	@Override
	public String getName(int id) {
		return Mysql.getOne("SELECT name FROM `rrpg_objects` WHERE id='"+id+"'");
	}

	@Override
	public String getModel(int id) {
		return Mysql.getOne("SELECT model FROM `rrpg_objects` WHERE id='"+id+"'");
	}

	@Override
	public Map<Integer, Action> getActions(int id) {
		Map<Integer, Action> ret = new HashMap<Integer, Action>();
		
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT * FROM rrpg_object_actions AS oa " +
				"INNER JOIN rrpg_actions AS a ON (a.id=oa.action_id) " +
				"INNER JOIN rrpg_action_items AS ai ON (a.id=ai.action_id) " +
				"WHERE oa.object_id='" + id + "'");
		Map<String, String> myRow;
		Action a;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			int action_id = Integer.parseInt(myRow.get("id"));
			
			if (!ret.containsKey(action_id)) {
				if (myRow.get("attribute").equals("0"))
					a = new Action(action_id,
						myRow.get("action"),
						Integer.parseInt(myRow.get("delay"))
					);
				else
					a = new Action(action_id,
						myRow.get("action"),
						Integer.parseInt(myRow.get("delay")),
						Integer.parseInt(myRow.get("attribute")),
						Integer.parseInt(myRow.get("value"))
					);
				ret.put(action_id, a);	
			} else
				a = ret.get(action_id);
			
			a.addActionItem(Integer.parseInt(myRow.get("item_id")),
				Integer.parseInt(myRow.get("possibility")),
				Integer.parseInt(myRow.get("min")),
				Integer.parseInt(myRow.get("max")));
			
		}
		result.close();
		mysql.close();
		
		return ret;
	}

	@Override
	public Action getDefaultAction(int id) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT * FROM rrpg_object_actions AS oa " +
				"INNER JOIN rrpg_actions AS a ON (a.attribute=0 AND a.id=oa.action_id) " +
				"INNER JOIN rrpg_action_items AS ai ON (a.id=ai.action_id) " +
				"WHERE oa.object_id='" + id + "'");
		Map<String, String> myRow;
		Action a = null;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			if (null==a) {
				a = new Action(
					Integer.parseInt(myRow.get("id")),
					myRow.get("action"),
					Integer.parseInt(myRow.get("delay"))
				);
			}
			a.addActionItem(Integer.parseInt(myRow.get("item_id")),
				Integer.parseInt(myRow.get("possibility")),
				Integer.parseInt(myRow.get("min")),
				Integer.parseInt(myRow.get("max")));
		}
		return a;
	}

	@Override
	public Action getItemAction(int object_id, int item_id) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT a.*, ai.* FROM rrpg_object_actions AS oa " +
				"INNER JOIN rrpg_actions AS a ON ( a.id = oa.action_id ) " +  
				"INNER JOIN rrpg_item_attributes AS ia ON ( ia.item_id ='" + item_id + "'" +
				"AND ia.attribute_id = a.attribute AND ia.value = a.value ) " + 
				"INNER JOIN rrpg_action_items AS ai ON ( a.id = ai.action_id ) " +
				"WHERE oa.object_id ='" + object_id + "'");
		Map<String, String> myRow;
		Action a = null;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			if (null==a) {
				a = new Action(
					Integer.parseInt(myRow.get("id")),
					myRow.get("action"),
					Integer.parseInt(myRow.get("delay"))
				);
			}
			a.addActionItem(Integer.parseInt(myRow.get("item_id")),
				Integer.parseInt(myRow.get("possibility")),
				Integer.parseInt(myRow.get("min")),
				Integer.parseInt(myRow.get("max")));
		}
		return a;
	}
	

}
