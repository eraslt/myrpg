package info.mindes.DataProviders.mysql;

import java.util.HashMap;
import java.util.Map;

import info.mindes.DataProviders.ItemData;


import simplemysql.SimpleMySQL;
import simplemysql.SimpleMySQLResult;

public class MysqlItemData implements ItemData {
	
	static Map<Integer, Attribute> attributes = null;
	static Map<Integer, String> att_values = null;

	public MysqlItemData() {
		readAttributes();
	}
	
	@Override
	public String getItemAttribute(int item_id, String attribute) {
		return Mysql.getOne("SELECT IFNULL( ia.value, a.default ) " + 
				"FROM `rrpg_attributes` AS a " +
				"LEFT JOIN `rrpg_item_attributes` AS ia ON ( a.id = ia.attribute_id " +
				"AND ia.item_id="+item_id+") " +
				"WHERE a.attribute='"+attribute+"'");
	}

	@Override
	public String getItemAttribute(int item_id, int attribute_id) {
		return Mysql.getOne("SELECT IFNULL( ia.value, a.default )" + 
				"FROM `rrpg_attributes` AS a " +  
				"LEFT JOIN `rrpg_item_attributes` AS ia ON ( a.id = ia.attribute_id " +
				"AND ia.item_id="+item_id+") " +
				"WHERE a.id='"+attribute_id+"'");
	}

	@Override
	public Map<Integer, String> getAllItemAttributes(int item_id) {
		return null;
	}

	private void readAttributes() {
		attributes = new HashMap<Integer, Attribute>();
		att_values = new HashMap<Integer, String>();
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT * FROM `rrpg_attributes`");
		
		Map<String, String> myRow;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			attributes.put(Integer.parseInt(myRow.get("id")), new Attribute(myRow.get("attribute"),myRow.get("default")));
			att_values.put(Integer.parseInt(myRow.get("id")), myRow.get("attribute"));
		}
		result.close();
		mysql.close();
	}

	@Override
	public Map<Integer, String> getAllAttributes() {
		return att_values;
	}
}
