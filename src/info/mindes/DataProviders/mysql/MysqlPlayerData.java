package info.mindes.DataProviders.mysql;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import simplemysql.SimpleMySQL;
import simplemysql.SimpleMySQLResult;

import info.mindes.DataProviders.PlayerData;

public class MysqlPlayerData implements PlayerData {

	@Override
	public Map<Integer, Integer> getAllItems(int player_id) {
		
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT item_id, quantity FROM `rrpg_player_items` WHERE player_id="+player_id);
		
		Map<Integer, Integer> ret = new HashMap<>(); 
		Map<String, String> myRow;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			ret.put(Integer.parseInt(myRow.get("item_id")), Integer.parseInt(myRow.get("quantity")));
		}
		result.close();
		mysql.close();
		return ret;
	}
	
	@Override
	public Map<Integer, Integer> getAllSkills(int player_id) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT item_id, quantity FROM `rrpg_player_items` WHERE player_id="+player_id);
		
		Map<Integer, Integer> ret = new HashMap<>(); 
		Map<String, String> myRow;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			ret.put(Integer.parseInt(myRow.get("item_id")), Integer.parseInt(myRow.get("quantity")));
		}
		result.close();
		mysql.close();
		return ret;
	}

	@Override
	public void saveItems(int player_id, Map<Integer, Item> inv) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		
		mysql.Query ("DELETE FROM rrpg_player_inventory WHERE player_id=" + player_id);
		Set<Integer> keys = inv.keySet();
		for (Iterator<Integer> i = keys.iterator(); i.hasNext();) {
			int pos = i.next();
			Item item = inv.get(pos);
			mysql.Query ("INSERT INTO rrpg_player_inventory (player_id, slot, item_id, cnt) " +
					"VALUES (" + player_id + ",  " + pos + ",  " + item.id + ",  " + item.cnt + ");");
		}
		mysql.close();
	}

	@Override
	public int getPlayerSkill(int player_id, int skill_id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, String> getPlayerData(int player_id) {
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT * FROM `rrpg_players` WHERE id=" + player_id);
		
		Map<String, String> ret = result.FetchAssoc();
		
		result.close();
		mysql.close();
		return ret;
	}

	@Override
	public Map<Integer, Item> getInventory(int player_id) {
		Map<Integer, Item> inv = new HashMap<>();
		
		SimpleMySQL mysql = new SimpleMySQL();
		mysql.connect(Mysql.server, Mysql.username, Mysql.password, Mysql.database);
		SimpleMySQLResult result = mysql.Query ("SELECT slot, item_id, cnt FROM `rrpg_player_inventory` WHERE player_id="+player_id);
		
		Map<String, String> myRow;
		while (!(myRow = result.FetchAssoc()).isEmpty()){
			int item_id = Integer.parseInt(myRow.get("item_id"));
			int cnt = Integer.parseInt(myRow.get("cnt"));
			int pos = Integer.parseInt(myRow.get("slot"));
			inv.put(pos, new Item(item_id, cnt));
		}
		result.close();
		mysql.close();
		return inv;
	}

	@Override
	public void savePosition(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}
}
